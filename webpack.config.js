const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const EncodingPlugin = require('webpack-encoding-plugin');
require('babel-polyfill');
const VENDER_LIBS = [
    "axios",
    "bootstrap",
    // "font-awesome",
    "jquery",
    "jquery-mousewheel",
    "popper.js",
    "react",
    "react-dom",
    "react-redux",
    "react-router-dom",
    "redux",
    "redux-thunk",
    "react-router",
    "firebase",
    "proptypes",
    "warning"
    // "webpack-cli"
];

const devServer={
    port:4000,
    // contentBase: './dist',
    
    historyApiFallback: true,
    hot: true,
  }


module.exports={
    entry:  ["babel-polyfill", "./src/index.js"],
    //  {
    //     bundle: './src/index.js',
    //     vendor: VENDER_LIBS
    // }, 
    output: {
        path: path.join(__dirname, '/dist'),
        filename: '[name].[hash].js',
        publicPath: '/',
    }, 
    resolve: {
        extensions: ['.js','.scss']
        
      },
    module: {

        rules:[
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: '/node_modules/'
            },
            {
                use: 'style-loader',
                test: /\.css$/,
            },
            {
                use: 'css-loader',
                test: /\.css$/,
            },
            {
                loader: 'file-loader',
                test: /\.png$|\.gif$|\.jpe?g$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$|\.wav$|\.mp3$|\.mp4$|\.ico$/,
            }
        ]
    },
    plugins:[
        new webpack.ProvidePlugin({
            $:'jquery',
            jQuery:'jquery',
            'window.$':'jquery',
            'window.jQuery':'jquery',
        })
        ,
        new webpack.optimize.CommonsChunkPlugin({
            names:['vendor','mainfest']
        }),
        new webpack.HotModuleReplacementPlugin(),
         new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            template:'./index.html'
        })
        ,
        // new EncodingPlugin({
        //     encoding: 'Unicode'
        //   })
        
    ],
    devtool: 'cheap-eval-source-map',
    devServer
}


