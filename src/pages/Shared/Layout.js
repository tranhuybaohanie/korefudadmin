import React, { Component } from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

// import './App.css';

import Header from '../Shared/Header';
import Footer from '../Shared/Footer';

class Layout extends Component {

  constructor(props) {
    super(props);

  }
  setColor = (param) => {
    this.setState({
      color: param
    });
  }

  render() {
    
    return (
      <div>
      <div className="container-fluid">
        <div className="row">
      
       <Header></Header>


<div className="content container h-100 mt-2 ">
         {this.props.children}
          </div>

           </div>
          </div> 
         <Footer></Footer>
      
        </div>

    );
  }
}

export default Layout;
