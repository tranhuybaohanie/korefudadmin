import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

class Footer extends Component {


    render() {
        var d = new Date();
        var getdate = d.getFullYear();
    
        return (
            <footer className="container-fluid mt-5">

                <div className="subcribe-form col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-4 mb-4 ">

                    <form className="d-flex flex-row w-100 justify-content-center" action="" method="POST" role="form" >

                        <div className="p-2"> <label >Subcribe to have new things</label></div>
                        <div className="p-2"> <input type="text" className="form-control" id="" placeholder="bao.tranhuyvn@gmail.com" /></div>
                        <div className="p-2"> <button type="submit" className="btn btn-primary p-2">Submit</button></div>




                    </form>

                </div>
                <hr />
                <div className="row ">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-2">

                        <ul className="list-group">
                            <li className="list-group-item">Support </li>
                            <li className="list-group-item">Terms of use Legal Information </li>
                            <li className="list-group-item">Privacy Policy </li>
                        </ul>

                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-2">
                        <ul className="list-group">
                            <li className="list-group-item">Sitemap </li>
                            <li className="list-group-item">Frequently Asked Questions</li>
                            <li className="list-group-item">Mobile App</li>
                        </ul>
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 mt-2">

                        <div className="language-form">
                            <img src="/img/flag/us.png" className="img-fluid" alt="Image" />
                            <select name="" className="form-control" >
                                <option value="">English</option>
                                <option value="">Vietnammese</option>
                            </select>
                        </div>
                    </div>


                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-4  text-center">
                    <h2>Lavian Hotel</h2>
                    <a>@Tran Huy Bao, right ({getdate})</a>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-4">
                    <ul className="list-group d-flex flex-row justify-content-center">
                        <li className="list-group-item p-2">
                            <i className="fab fa-facebook-square"></i>
                        </li>
                        <li className="list-group-item p-2">
                            <i className="fab fa-twitter-square"></i>
                        </li>
                        <li className="list-group-item p-2">
                            <i className="fab fa-instagram"></i>
                        </li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer;
