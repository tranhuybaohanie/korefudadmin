import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import '../../Assets/css/Shared/Header.css';
import logo from './../../Assets/img/logo.png';
// import renderPath from '../../RenderCustom/Image';

// import logo from './logo.svg';
//import './App.css';
const renderPath =({path})=>{
  return require(path);
}

const MenuLink = ({ label, to }) => {
 
    // <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => {
      //var active = activeOnlyWhenExact ? ct : "";
      return (
        // <li className="nav-item ">
          <NavLink  activeClassName="active" exact  className="nav-link"  to={to}> {label}<span className="sr-only">{label}</span></NavLink>
        // </li>
      )
    // }}
    // ></Route>
  
}


class Header extends Component {

  
  render() {
   
    var imgPath =require('./../../Assets/img/logo.png');
    return (
      <header className="w-100">

        <div className="w-100">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="navbar-brand logo " href="#"><img  src={imgPath}  className="img-fluid" alt="logo" /> </div>
            <div className="col-sm"><h5>Lavian Hotel</h5></div>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto menu-top">
                <li className="nav-item ">
                  <MenuLink   to="/" label="Home"/>
                </li>
                <li className="nav-item ">
                  <MenuLink   to="/Admin" label="Admin"/>
                </li>
                <li className="nav-item">
                <MenuLink   to="/Explore" label="Explore"/>
                </li>
                <li className="nav-item">
                <MenuLink    to="/Apartments" label="Apartments"/>
                </li>
                <li className="nav-item">
                <MenuLink    to="/Contact" label="Contact"/>
                </li>

                 <li className="nav-item">
                <MenuLink    to="/About" label="About"/>
                </li>
                <li className="nav-item">
                <MenuLink    to="/Blogs" label="Blogs"/>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
