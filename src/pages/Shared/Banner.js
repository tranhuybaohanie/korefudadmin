import React, { Component } from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
// import logo from './logo.svg';


class Header extends Component {

	
  render() {

    return (
      <header>
      <div className="banner">
        <img src={require("./../../Assets/img/hotel-banner.png")} className="img-fluid rounded-top|rounded-right|rounded-bottom|rounded-left|rounded-circle|" alt="banner hotel" />
      </div>
     
    </header>
    );
  }
}

export default Header;
