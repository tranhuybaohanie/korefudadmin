import React, { Component } from 'react';
import HotelItem from './HotelItem';
import {connect} from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
// import logo from './logo.svg';
// import './App.css';

class HotelList extends Component {


    constructor(props){
        super(props);
        this.state = {
            Hotels:[]
        };
    }
    componentDidMount(){
       this.props.fetchAllHotels();
    }

    showHotelList=(Hotels)=>{
        return Hotels.map((hotel,id)=>{
           
            return ( <HotelItem key={id} Hotels={hotel}></HotelItem>
            );

        });
    }
    render() {
        console.log(this.props);
    var {Hotels} = this.props;

    // var {Hotels} = this.state;

        return (

 <div className="listroom  mt-2">
 {this.showHotelList(Hotels)}

</div>
        );
    }
}


const mapStateToProps = state =>{
    return {
        Hotels : state.Hotel
    }
}
const mapDispatchToProps = (dispatch,props)=>{
    return { fetchAllHotels : ()=>{
    dispatch(actFetchHotelsRequest());
    }}
}
export default connect(mapStateToProps,mapDispatchToProps)(HotelList);
