import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';

class FilterForm extends Component {


    render() {
    
        return (
            <div className="orderform  ">
            <div className=" card ">

              <div className="card-body">
                <div className="row">

                  <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">


                    <div className="form-group">
                      <label >Checkin</label>
                      <input type="date" className="form-control" id="" placeholder="Input field" />
                    </div>


                  </div>
                  <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div className="form-group">
                      <label >Checkout</label>
                      <input type="date" className="form-control" id="" placeholder="Input field" />
                    </div>
                  </div>
                  <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <div className="form-group">
                      <label >Adults</label>
                      <input type="number" className="form-control" id="" placeholder="0" />
                    </div>
                  </div>
                  <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <div className="form-group">
                      <label >Kids</label>
                      <input type="number" className="form-control" id="" placeholder="0" />
                    </div>
                  </div>
                  <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label ></label>
                    <button type="button" className="btn btn-large btn-block btn-success btn-default">Search</button>

                  </div>
                </div>
                <div className="row">

                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">


                      <div className="form-group">
                        <label >Price</label>
                        <div className="form-group">
                            <label >Example Range input</label>
                            <input type="range" className="form-control-range" id="formControlRange"/>
                          </div>
                      </div>


                    </div>
                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                      <div className="form-group">
                       
                        <div className="dropdown">
                            <label className="mr-2">Hotel class</label>
                            <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                             All
                            </button>
                        <div className="dropdown-menu">
                          
                          <div className="checkbox">
                            <label>
                              <input type="checkbox" value="5"/>
                              5 star hotel
                            </label>
                          </div>
                          
                         
                          </div>
                        </div>
                   
                       
                      </div>
                    </div>
                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                      <div className="form-group">
                          <div className="dropdown">
                              <label className="mr-2">Guest rating</label>
                              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                               All
                              </button>
                          <div className="dropdown-menu">
                            
                            <div className="checkbox">
                              <label>
                                <input type="checkbox" value="5"/>
                                5 star hotel
                              </label>
                            </div>
                            
                           
                            </div>
                          </div>
                      </div>
                    </div>
                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <div className="form-group">
                            <div className="dropdown">
                                <label className="mr-2">Location</label>
                                <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                 All
                                </button>
                            <div className="dropdown-menu">
                              
                              <div className="checkbox">
                                <label>
                                  <input type="checkbox" value="5"/>
                                  5 star hotel
                                </label>
                              </div>
                              
                             
                              </div>
                            </div>
                        </div>
                      </div>
                      <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                          <div className="form-group">
                              <div className="dropdown">
                                  <label className="mr-2">More Filter</label>
                                  <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                   All
                                  </button>
                              <div className="dropdown-menu">
                                
                                <div className="checkbox">
                                  <label>
                                    <input type="checkbox" value="5"/>
                                    5 star hotel
                                  </label>
                                </div>
                                
                               
                                </div>
                              </div>
                          </div>
                        </div>
                  </div>
              </div>
            </div>
          </div>
        );
    }
}

export default FilterForm;
