import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import logo from './logo.svg';
// import './App.css';

class Footer extends Component {

descriptionLitmitWord =(descriptionText)=>{
  return(
    descriptionText.slice(0, 120)+""+((descriptionText.length>120) ? " ...":"")
   // descriptionText.lenght>30?descriptionText.slice(7, 13):descriptionText
  );
}

    render() {
    var {name,description,address,moneyRatesMax,moneyRatesMin,currency,noOfRating} =this.props.Hotels;
   
        return (
 <div className="card d-flex flex-row mb-2">
 <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
   <img src="http://imgio.trivago.com/itemimages/92/68/9268576_v2_isq.jpeg" className="img-fluid img-thumbnail mt-2 mb-2" alt="room" />
 </div>
 <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
   <div className="card-body listroom-content h-100">
     <h3 className="card-title">{name}</h3>
     <p className="card-text">{address}</p>
     <p className="cart-text">{this.descriptionLitmitWord(description)}</p>
     <span className="appriciate">
       
       <i className="text-danger"><FontAwesomeIcon icon="star" /></i>
       <i className="text-danger"><FontAwesomeIcon icon="star" /></i>
       <i className="text-danger"><FontAwesomeIcon icon="star" /></i>
       <i className="text-danger"><FontAwesomeIcon icon="star" /></i>
     </span>
     <a>({noOfRating} reviews)</a>
     <div>
     </div>
   </div>
 </div>
 <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 d-flex flex-column">
   <div className="p-2 mt-auto">
     <h5 className=" text-success text-center ">{`${moneyRatesMax}-${moneyRatesMin}`}

</h5>
     <h5 className="price text-center ">{currency.name}</h5>
   </div>
   <div className="mt-auto p-2">
     <button type="button" className="btn btn-default w-100">Explore room</button>
   </div>
 </div>
</div>
        );
    }
}

export default Footer;
