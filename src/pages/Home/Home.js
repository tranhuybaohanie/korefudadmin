import React, { Component } from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

// import './App.css';

import Header from '../Shared/Header';
import Footer from '../Shared/Footer';
import Banner from '../Shared/Banner';
import FilterForm from './FilterForm';
import HotelList from './HotelList';
import './../../Assets/css/Shared/layout.css';
class Home extends Component {

  constructor(props) {
    super(props);

  }
  setColor = (param) => {
    this.setState({
      color: param
    });
  }

  render() {

    
    return (
      <div>
      
      <div className="container-fluid">
        <div className="row">
        <Banner></Banner>
       <Header></Header>



          <div className="content container h-100 mt-2 ">
           <FilterForm></FilterForm>

           <HotelList></HotelList>
          </div>
        </div>
      </div>

          

         <Footer></Footer>
      
        </div>

    );
  }
}

export default Home;
