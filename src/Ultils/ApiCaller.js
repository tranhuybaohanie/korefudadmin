import axios from 'axios';
import * as Config from './../Constants/Config';



export default function callApi(endpoint, method ='GET',bodyData){
    return axios({
        method:method,
        url:`${Config.API_URL}/${endpoint}`,
        data: bodyData

    }).catch((e)=>{
        console.log('APICALLER: '+e+' '+e.status);
    });
}