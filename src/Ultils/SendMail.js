import axios from 'axios';
import * as Config from './../Constants/Config';

export default function SendMail(email,subject,content){
  var  serialize = obj => {
        let str = [];
        for (let p in obj)
          if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
        return str.join("&");
      };
    return axios({
        method:'POST',
        headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
          },
        // url:'https://korefudmailer.herokuapp.com/api/mail/tranhuybaohanie@gmail.com',
        url:`${Config.domainnodeemal}/${email}`,
        data: serialize({
            subject,content
         })

    }).catch((e)=>{
        console.log('APICALLER: '+e+' '+e.status);
    });
}