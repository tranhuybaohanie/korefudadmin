import axios from 'axios';
import * as Config from './../Constants/Config';

export default function SendFile(file_name,file_upload){
    let fd = new FormData();
    fd.append('file_name',file_name)
    fd.append('file_upload',file_upload)
    return axios({
        method:'POST',
        headers: {
            Accept: "application/json",
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'multipart/form-data'
          },
        // url:'https://korefudmailer.herokuapp.com/api/mail/tranhuybaohanie@gmail.com',
        url:`${Config.domainnodeuploadfile}`,
        data:fd
        //  serialize({
        //     file_name,file_upload
        //  })

    }).catch((e)=>{
        console.log('APICALLER: '+e+' '+e.status);
    });
}