import axios from 'axios';
import * as Config from './../Constants/Config';

export default function DeleteFile(file_name,cb){
    return axios({
        method:'DELETE',
        headers: {
            Accept: "application/json",
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'multipart/form-data'
          },
        // url:'https://korefudmailer.herokuapp.com/api/mail/tranhuybaohanie@gmail.com',
        url:`${Config.domainnodeuploadfile+"/"+file_name}`,

    }).catch((e)=>{
        cb(false)
        console.log('APICALLER: '+e+' '+e.status);
    }).then(()=>{  cb(true)});
}