import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

import logo from './logo.svg';
import './App.css';
import './Assets/css/animation.css';
import Routes from './Routes';
import { browserHistory } from 'react-router';
import warning from "warning";
import PropTypes from "prop-types";
import { createBrowserHistory as createHistory } from "history";
import Home from './pages/Home/Home';
import Explore from './pages/Explore/Explore';
import NotFound from './pages/NotFound/NotFound';
import AdminHome from './AdminPage/Home/Index';
import { firebaseApp } from './Ultils/firebaseConfig';
import * as actionRedux from './Storage/Actions/Index';
class App extends Component {

  constructor(props) {
    super(props);
    if (localStorage.getItem("lang") == null) {
      localStorage.setItem("lang", "vi");
    }

    this.state = {
      position: ""
    }
  }
  // static propTypes = {
  //   basename: PropTypes.string,
  //   forceRefresh: PropTypes.bool,
  //   getUserConfirmation: PropTypes.func,
  //   keyLength: PropTypes.number,
  //   children: PropTypes.node
  // };

  componentWillMount() {
    var me = this;
    firebaseApp.auth().onAuthStateChanged(function (user) {
      if (user) {
        actionRedux.getUserAdminByID(user.uid, result => {
          me.setState({ position: result.position })
        })
      }
    })

    warning(
      !this.props.history,
      "<Router> ignores the history prop. To use a custom history, " +
      "use `import { Router }` instead of `import { BrowserRouter as Router }`."
    );
  }
  // history = createHistory(this.props);
  setColor = (param) => {
    this.setState({
      color: param
    });
  }
  showMenu = (routes) => {
    return routes(this.state.position).map((route, id) => {
      return <Route key={id} path={route.path} exact={route.exact} component={route.component} />
    });
  }
  render() {
    localStorage.removeItem("LoginState");
    
  
    if (Routes(this.state.position.length != 0)) {
    return (
      <Router>
        <div style={{ height: "100%" }}>
          <Switch>
            {this.showMenu(Routes)}
          </Switch>

        </div>
      </Router>)
    }
    else{    return (
      <div className="container-fluid" style={{ height: "100%", backgroundColor: "white" }}>
        <div className="row" style={{ height: "100%", backgroundColor: "white" }}>


        </div>
        <img src={require("./Assets/img/loadsquare.gif")} style={{ position: "absolute", left: "48%", top: "40%" }} width="5%"></img>
      </div>)
  }

      //   <Router history={browserHistory}>
      //   <Route path={"/home"} component={Home}/>
      //   <Route path={"/"} component={Home}/>>

      // </Router>

  
  }
}


//   constructor(props){
//     super(props);
//    this.state ={

//     color:'red'
//    } 
// }
// setColor=(param)=>{
// this.setState({
//   color: param
// });
// }

//   render() {

//     return (
//       <div className="container">
//       <div className="row">
//      <Colorpanels color={this.state.color} appReceiveColor={this.setColor}/>
//      <Letter color={this.state.color} />

//       </div>
//       </div>


//       );
//   }
// }

export default App;
