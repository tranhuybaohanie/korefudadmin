import React, { Component } from 'react';

import Home from './pages/Home/Home';
import Explore from './pages/Explore/Explore';
import NotFound from './pages/NotFound/NotFound';
import AdminHome from './AdminPage/Home/Index' ;
import AdminItem from './AdminPage/Item/Index' ;
import AdminAddNewItem from './AdminPage/Item/AddNewForm' ;
import AdminAddNewBigCategory from './AdminPage/BigCategory/AddNewForm' ;
import AdminAddNewProduct from './AdminPage/Product/AddNewForm';
import AdminModifiedProduct from './AdminPage/Product/Modified';
import AdminProduct from './AdminPage/Product/Index';
import AdminAddNewProductCategory from './AdminPage/ProductCategory/AddNewForm';
import AdminModifiedProductCategory from './AdminPage/ProductCategory/Modified';
import ProductCategory from './AdminPage/ProductCategory/Index';
import Menu from './AdminPage/Menu/Index';
import ModifiedMenu from './AdminPage/Menu/Modified';
import AddNewMenu from './AdminPage/Menu/AddNewForm';
import BigCategory from './AdminPage/BigCategory/Index';
import Order from './AdminPage/Order/Index'
import AdminImageManagerment from './AdminPage/ImageManagerment/Index';
import AdminLogin from './AdminPage/Home/Login';
import UserCustomer from './AdminPage/UserCustomer/Index';
import OrderAnaLysisYear from './AdminPage/Analysis/OrderYear'
import OrderAnaLysisDayly from './AdminPage/Analysis/OrderDay'
import UserStaff from './AdminPage/UserStaff/Index';

import UserStaffAddNewForm from './AdminPage/UserStaff/AddNewForm';
import UserStaffModified from './AdminPage/UserStaff/Modified';
import * as actionRedux from './Storage/Actions/Index';

const auth=(position)=>{
 return Routes.filter(x=>x.auth.includes(all)||x.auth.includes(position))
}

const all="all";
const admin="admin";
const content="content";
const order="order";
const superman ="super";

const Routes = [
    {
        path     : '/',
        component: ({match})=><Home  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[all]
    },
    {
        path     : '/Explore',
        component: ()=><Explore match={match} history={history} location={location}/>,
        exact    : false,
        auth:[all]
    },
    {
        path     : '/admin',
        component: ({match,history})=><AdminHome match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman,order]
    },
    {
        path     : '/admin/login/:urlReturn',
        component: ({match,history,location})=><AdminLogin  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[all]
    },
    {
        path     : '/admin/login/',
        component: ({match,history,location})=><AdminLogin  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[all]
    },
    {
        path     : '/admin/product',
        component: ({match,history})=><AdminProduct  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
   {
        path     : '/admin/add-new-product',
        component: ({match,history,location})=><AdminAddNewProduct   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/modified-product/:id',
        component: ({match,history,location})=><AdminModifiedProduct   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/image-managerment',
        component: ({match,history,location})=><AdminImageManagerment   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/add-new-big-category',
        component: ({match,history,location})=><AdminAddNewBigCategory   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/add-new-product-category',
        component: ({match,history,location})=><AdminAddNewProductCategory   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/order-analysis-dayly',
        component: ({match,history,location})=><OrderAnaLysisDayly   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/order-analysis-year',
        component: ({match,history,location})=><OrderAnaLysisYear   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/modified-product-category/:id',
        component: ({match,history,location})=><AdminModifiedProductCategory   match={match} history={history} location={location}/>,
        exact    : false,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/big-category',
        component: ({match,history,location})=><BigCategory  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/product-category',
        component: ({match,history,location})=><ProductCategory match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/menu',
        component: ({match,history,location})=><Menu match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/add-new-menu',
        component: ({match,history,location})=><AddNewMenu  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/modified-menu/:id',
        component: ({match,history,location})=><ModifiedMenu  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,content,superman]
    },
    {
        path     : '/admin/order',
        component: ({match,history,location})=><Order  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,order,superman]
    },
    {
        path     : '/admin/user-customer',
        component: ({match,history,location})=><UserCustomer  match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin,superman]
    },
    {
        path     : '/admin/user-staff',
        component: ({match,history,location})=><UserStaff match={match}   match={match} history={history} location={location}/>,
        auth:[admin]
    },
    {
        path     : '/admin/user-staff-add-new',
        component: ({match,history,location})=><UserStaffAddNewForm match={match}   match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin]
    },
    {
        path     : '/admin/user-staff-edit-new',
        component: ({match,history,location})=><UserStaffModified match={match}   match={match} history={history} location={location}/>,
        exact    : true,
        auth:[admin]
    },
    {
        path     : '',
        component: ({match})=><NotFound/>,
        exact    : false,
        auth:[admin,content,superman,order]
    }
]

export default auth;