import * as Types from './../Constants/ActionTypes';
import callApi from '../../Ultils/ApiCaller';
import {
    firebaseApp
} from '../../Ultils/firebaseConfig';
import {
    db
} from './../../Ultils/firebaseConfig';
import {
    storageRef
} from './../../Ultils/firebaseConfig';
import SendMail from './../../Ultils/SendMail';
import DeleteFile from './../../Ultils/DeleteFile'
export const getBigCategory = function (cb) {

    var finalResult = [];
    db.collection('big-category').onSnapshot(snapshot => {
        var lang = localStorage.getItem("lang") || "en";
        var result = []
        snapshot.forEach(doc => {
            const {
                name_en,
                name_vi,
                description_en,
                description_vi,
                status
            } = doc.data();

            result.push({
                id: doc.id,
                name: eval("name_" + lang),
                description: eval("description_" + lang),
                status
            })
            cb(result);

        });

    });


}

export const getBigCategoryById = function (id, cb) {
    var finalResult = {};
    db.collection('big-category').doc(id + "").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}

export const adBigCategory = (bigcategory) => {
    console.log(bigcategory)
    db.collection("big-category").add({
        name_en: bigcategory.name_en,
        description_en: bigcategory.description_en,
        name_vi: bigcategory.name_vi,
        description_vi: bigcategory.description_vi,
        status: bigcategory.status
    })
        .then(function () {
            alert("Document successfully written!");
        })
        .catch(function (error) {
            alert("Error writing document: ", error);
        });
}



export const delBigCategory = (bigcategoryid) => {

    db.collection("big-category").doc(bigcategoryid).delete().then(function () {
        alert("Document successfully deleted!");
    }).catch(function (error) {
        alert("Error removing document: ", error);
    });

}

export const getImange = function (cb) {
    var finalResult = [];
    db.collection('image-library').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                name,
                status,
                img_size,
                img_url,
                create_date,
                content_type
            } = doc.data();
            result.push({
                id: doc.id,
                name,
                status,
                img_size,
                img_url,
                create_date,
                content_type
            })
            result = result.sort(function (a, b) {
                return (new Date(b.create_date) - new Date(a.create_date)) * 1
            })
            cb(result);

        });

    });


}
export const adImage = (image) => {
    db.collection("image-library").add({
        name: image.name,
        img_url: image.img_url,
        img_size: image.img_size,
        create_date: image.create_date,
        content_type: image.content_type,
        status: true
    })
        .then(function () {

        })
        .catch(function (error) {
            console.log("Error writing document: ", error);
        });
}

export const adNotification = (email, content_vi, content_en, img_url, navigate) => {
    var today = new Date().toLocaleString("vi");
    db.collection("customer-notification").add({
        img_url,
        navigate,
        email, content_en, content_vi,
        create_date: today,
        readed: false,
        status: true
    })
        .then(function () {

        })
        .catch(function (error) {
            console.log("Error writing document: ", error);
        });
}

export const delImgageById = (id, cb) => {
    db.collection("image-library").doc(id).get().then(result => {
        var { name, content_type } = result.data();

        console.log(result.data())
        DeleteFile(name + content_type, final => {
            db.collection("image-library").doc(id).delete().then(function () {
                cb(true);
            }).catch(function (error) {
                cb(error);
            });
        })
        var desertRef = storageRef.ref(`images/${id}`)
        // Delete the file
        desertRef.delete().then(function () {
            // File deleted successfully
        }).catch(function (error) {
            // Uh-oh, an error occurred!
        });


    })
}

export const addMenu = (menu) => {
    console.log(menu)
    var today = new Date().toLocaleString("vi");
    return db.collection("menu").add({
        name: menu.name,
        itemsMenu: menu.itemsMenu,
        slider: menu.slider,
        caption_drink_en: menu.caption_drink_en,
        caption_drink_vi: menu.caption_drink_vi,
        caption_food_en: menu.caption_food_en,
        caption_food_vi: menu.caption_food_vi,
        title_drink_en: menu.title_drink_en,
        title_drink_vi: menu.title_drink_vi,
        title_food_en: menu.title_food_en,
        title_food_vi: menu.title_food_vi,
        create_date: today,
        modified_by: "",
        modified_date: "",
        author: localStorage.getItem("uid"),
        view: 0,
        status: true

    })

}
export const updateMenu = (id, menu) => {
    console.log(menu)
    var today = new Date().toLocaleString("vi");
    return db.collection("menu").doc(id + "").update({
        name: menu.name,
        itemsMenu: menu.itemsMenu,
        slider: menu.slider,
        caption_drink_en: menu.caption_drink_en,
        caption_drink_vi: menu.caption_drink_vi,
        caption_food_en: menu.caption_food_en,
        caption_food_vi: menu.caption_food_vi,
        title_drink_en: menu.title_drink_en,
        title_drink_vi: menu.title_drink_vi,
        title_food_en: menu.title_food_en,
        title_food_vi: menu.title_food_vi,
        create_date: today,
        modified_by: localStorage.getItem("uid"),
        modified_date: today,
        status: true

    })

}
export const duplicateMenu = (id, cb) => {
    getMenuByID(id, (result) => {
        if (result != false) {
            var menu = {
                name: result.name + "(Duplicated)",
                itemsMenu: result.itemsMenu,
                slider: result.slider,
                caption_drink_en: result.caption_drink_en,
                caption_drink_vi: result.caption_drink_vi,
                caption_food_en: result.caption_food_en,
                caption_food_vi: result.caption_food_vi,
                title_drink_en: result.title_drink_en,
                title_drink_vi: result.title_drink_vi,
                title_food_en: result.title_food_en,
                title_food_vi: result.title_food_vi,
            }
            addMenu(menu).then(function () {
                cb(true)
            })
                .catch(function (error) {
                    cb(false)
                })

        } else {
            cb(false)
        }
    })


}


export const getMenu = function (cb) {


    db.collection('menu').onSnapshot(snapshot => {
        // var lang   = localStorage.getItem("lang") || "en";
        var result = []

        snapshot.forEach(async doc => {
            var itemMenuDetail = [];
            const {
                name,
                itemsMenu,
                slider,
                caption_drink_en,
                caption_drink_vi,
                caption_food_en,
                caption_food_vi,
                title_drink_en,
                title_drink_vi,
                title_food_en,
                title_food_vi,
                create_date,
                modified_by,
                modified_date,
                author,
                view,
                status
            } = doc.data();
            console.log("-----------------")
            var promise1 = new Promise(function (resolve, reject) {
                if (itemsMenu.length == 0) {
                    resolve()
                }
                var length = 0;
                itemsMenu.forEach(async (item) => {
                    await getProductByID(item, (re) => {
                        length++;
                        itemMenuDetail.push(re)
                        if (itemsMenu.length == length) {
                            resolve()
                        }

                    })
                })

            })
            promise1.then(() => {

                result.push({
                    menu_id: doc.id,
                    name,
                    itemsMenu: itemMenuDetail,
                    slider,
                    caption_drink_en,
                    caption_drink_vi,
                    caption_food_en,
                    caption_food_vi,
                    title_drink_en,
                    title_drink_vi,
                    title_food_en,
                    title_food_vi,
                    create_date,
                    modified_by,
                    modified_date,
                    author,
                    view,
                    status
                })
                result = result.sort(function (a, b) {
                    return (new Date(b.create_date) - new Date(a.create_date)) * 1
                })
                return cb(result);
            })
        });

    });


}

export const getOrder = function (cb) {


    db.collection('Order').onSnapshot(snapshot => {
        // var lang   = localStorage.getItem("lang") || "en";
        var result = []

        snapshot.forEach(async doc => {
            var itemMenuDetail = [];
            const {
                Cart,
                CartComponentPrice,
                address,
                phone,
                fullname,
                received_state,
                status,
                totalItem,
                totalMoney,
                usermail,
                create_date,
                cancel_state,
                content_state,
                delivered_state,
                finished_state,
                Acceptance,
                totalItemAcceptance, totalMoneyAcceptance
            } = doc.data();

            result.push({
                id: doc.id,
                Cart,
                CartComponentPrice,
                address,
                phone,
                fullname,
                received_state,
                status,
                totalItem,
                totalMoney,
                usermail,
                create_date,
                cancel_state,
                content_state,
                delivered_state,
                finished_state,
                Acceptance,
                totalItemAcceptance, totalMoneyAcceptance
            })
            result = result.sort(function (a, b) { return (new Date(b.create_date.split(",")[1].split("/")[2], b.create_date.split(",")[1].split("/")[1], b.create_date.split(",")[1].split("/")[0], b.create_date.split(",")[0].split(":")[0], b.create_date.split(",")[0].split(":")[1], b.create_date.split(",")[0].split(":")[2]) - new Date(a.create_date.split(",")[1].split("/")[2], a.create_date.split(",")[1].split("/")[1], a.create_date.split(",")[1].split("/")[0], a.create_date.split(",")[0].split(":")[0], a.create_date.split(",")[0].split(":")[1], a.create_date.split(",")[0].split(":")[2])) });
            return cb(result);
        })
    });

}

export const searchOrder = function (lang, key, value, cb) {
    var finalResult = [];

    db.collection('Order').onSnapshot(snapshot => {
        // var lang   = localStorage.getItem("lang") || "en";
        var result = []

        snapshot.forEach(async doc => {
            var itemMenuDetail = [];
            const {
                Cart,
                CartComponentPrice,
                address,
                phone,
                fullname,
                received_state,
                status,
                totalItem,
                totalMoney,
                usermail,
                create_date,
                cancel_state,
                content_state,
                delivered_state,
                finished_state,
                Acceptance,
                totalItemAcceptance, totalMoneyAcceptance
            } = doc.data();

            // var obj[key]
            if (key == "name" || key == "description") {
                key = key + "_" + lang;
            }
            if (eval(key).toString().toLowerCase().includes(value.toLowerCase()))
                result.push({
                    id: doc.id,
                    Cart,
                    CartComponentPrice,
                    address,
                    phone,
                    fullname,
                    received_state,
                    status,
                    totalItem,
                    totalMoney,
                    usermail,
                    create_date,
                    cancel_state,
                    content_state,
                    delivered_state,
                    finished_state,
                    Acceptance,
                    totalItemAcceptance, totalMoneyAcceptance
                })
            result = result.sort(function (a, b) { return (new Date(b.create_date.split(",")[1].split("/")[2], b.create_date.split(",")[1].split("/")[1], b.create_date.split(",")[1].split("/")[0], b.create_date.split(",")[0].split(":")[0], b.create_date.split(",")[0].split(":")[1], b.create_date.split(",")[0].split(":")[2]) - new Date(a.create_date.split(",")[1].split("/")[2], a.create_date.split(",")[1].split("/")[1], a.create_date.split(",")[1].split("/")[0], a.create_date.split(",")[0].split(":")[0], a.create_date.split(",")[0].split(":")[1], a.create_date.split(",")[0].split(":")[2])) });
            return cb(result);





        });

    });


}

export const getOrderById = function (id, cb) {


    db.collection('Order').doc(id + "").onSnapshot(doc => {
        // var lang   = localStorage.getItem("lang") || "en";
        var result = []

        var itemMenuDetail = [];

        const {
            Cart,
            CartComponentPrice,
            address,
            phone,
            fullname,
            received_state,
            status,
            totalItem,
            totalMoney,
            usermail,
            create_date,
            cancel_state,
            content_cancel,
            delivered_state,
            Acceptance,
            finished_state,
            log_en,
            log_vi
        } = doc.data();

        result = {
            id: doc.id,
            Cart,
            CartComponentPrice,
            address,
            phone,
            fullname,
            received_state,
            status,
            totalItem,
            totalMoney,
            usermail,
            create_date,
            cancel_state,
            content_cancel,
            Acceptance,
            delivered_state,
            finished_state,
            log_en,
            log_vi
        }
        return cb(result);
    })


}
export const RejectOrder = (usermail, Cart, CartComponent, CartComponentPrice, orderId, content_cancel, cb) => {
    var today = new Date().toLocaleString("vi");
    db.collection("Order").doc(orderId + "").update({
        content_cancel,
        cancel_state: true,
        received_state: false,
        cancel_time: today,
        finished_state: false,
        delivered_state: false,
        status: false,
        log_vi: "Đơn hàng đã bị hủy.",
        log_en: "Order was canceled.",
    }).then(() => { cb(true) }).catch(() => { cb(false) })
    adNotification(usermail, "Đơn hàng " + orderId + " đã bị hủy.", "Order " + orderId + " was canceled.", "https://firebasestorage.googleapis.com/v0/b/korefud-d61f4.appspot.com/o/images%2Fg13694.png?alt=media&token=3f2b3bdd-1bc0-4dc4-843e-7c341dd63476", "order")
    SendMail(usermail, "Đơn hàng " + orderId + " đã bị hủy.",
        "<H4>Chào bạn!</h4>" +

        "<h5>Đơn hàng <b>" + orderId + "</b> đã bị hũy, lý do: " + content_cancel + "</h5>"
        +
        "  <table> <tr><th>Tên</th><th>Giá</th><th>Đặt</th><th >Duyệt</th><th >Tổng</th></tr>" +
        CartComponent.map((item, idd) => {
            var { id, name, productCategoryName, authorName, currency, price, promotion_price, description, status, bigCategoryName, author, create_date } = item;
            const quantity = Cart[Cart.findIndex(x => x.id === id)] ? Cart[Cart.findIndex(x => x.id === id)]["quantity"] : 0;
            const unitPrice = CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)] ? CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]["price"] : 0
            if (idd < Cart.length) {
                return ("<tr key={id}>"
                    + "<td>" + name + "</td>"
                    + "<td>" + unitPrice + "</td>"
                    + "<td>" + quantity + "</td>"
                    + "<td>" + unitPrice * quantity + "</td>"

                    + "</tr>"
                )
            }
        })
        + "</table>"
        + "<h5>(Korefud Vietnam, " + today + ")</h5>"
        + "<p>---------------English version-------------------</p>" +
        "<H4>Dear you!</h4>" +
        "<h5>Order <b>" + orderId + "</b> was canceled" +
        "  <table> <tr><th>Name</th><th>Price</th><th>Order</th><th >Acceptance</th><th >Total</th></tr>" +
        CartComponent.map((item, idd) => {
            var { id, name, productCategoryName, authorName, currency, price, promotion_price, description, status, bigCategoryName, author, create_date } = item;
            const quantity = Cart[Cart.findIndex(x => x.id === id)] ? Cart[Cart.findIndex(x => x.id === id)]["quantity"] : 0;
            const unitPrice = CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)] ? CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]["price"] : 0
            if (idd < Cart.length) {
                return ("<tr key={id}>"
                    + "<td>" + name + "</td>"
                    + "<td>" + unitPrice + "</td>"
                    + "<td>" + quantity + "</td>"
                    + "<td>" + unitPrice * quantity + "</td>"

                    + "</tr>"
                )
            }
        })
        + "</table>" +
        "<h5>(Korefud Vietnam, " + today + ")</h5>"

    )

}





export const AcceptanceOrder = (usermail, Cart, CartComponent, CartComponentPrice, orderId, Acceptance, totalItemAcceptance, totalMoneyAcceptance, cb) => {
    // sendMail()
    var today = new Date().toLocaleString("vi");
    db.collection("Order").doc(orderId + "").update({
        totalItemAcceptance,
        totalMoneyAcceptance,
        Acceptance,
        AcceptanceTime: today,
        cancel_state: false,
        status: true,
        finished_state: false,
        delivered_state: false,
        received_state: true,

        log_vi: "Đơn hàng đã được chấp nhận với số lượng: " + totalItemAcceptance + ", số tiền: " + totalMoneyAcceptance + "VND",
        log_en: "Order was accepted with quantity: " + totalItemAcceptance + ", total money: " + totalMoneyAcceptance + "VND",
    }).then(() => {
        cb(true);
    }).catch((e) => { cb(false); console.log(e) })
    adNotification(usermail, "Đơn hàng " + orderId + " đã được duyệt.", "Order " + orderId + " was appepted.", "https://firebasestorage.googleapis.com/v0/b/korefud-d61f4.appspot.com/o/images%2FjFXH8IItYI1QE1ABaAgT?alt=media&token=a9204493-ecee-4917-a403-ee6723e38988", "order")
    SendMail(usermail, "Đơn hàng " + orderId + " đã được chấp nhận với số lượng: " + totalItemAcceptance + ", số tiền: " + totalMoneyAcceptance + "VND",
        "<H4>Chào bạn!</h4>" +

        "<h5>Đơn hàng <b>" + orderId + "</b> đã được chấp nhận với số lượng: " + totalItemAcceptance + ", số tiền: " + totalMoneyAcceptance + "VND" + "</h5>"
        + "<p>Bạn sẽ nhận được email khác khi đơn hàng chuẩn bị giao.</p>"
        +
        "  <table> <tr><th>Tên</th><th>Giá</th><th>Đặt</th><th >Duyệt</th><th >Tổng</th></tr>" +
        CartComponent.map((item, idd) => {
            var { id, name, productCategoryName, authorName, currency, price, promotion_price, description, status, bigCategoryName, author, create_date } = item;


            const quantity = Cart[Cart.findIndex(x => x.id === id)] ? Cart[Cart.findIndex(x => x.id === id)]["quantity"] : 0;
            const quantityAceptace = Acceptance[Acceptance.findIndex(x => x.id === id)] ? Acceptance[Acceptance.findIndex(x => x.id === id)]["quantity"] : 0;
            const unitPrice = CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)] ? CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]["price"] : 0
            if (idd < Cart.length) {
                return ("<tr key={id}>"
                    + "<td>" + name + "</td>"
                    + "<td>" + unitPrice + "</td>"
                    + "<td>" + quantity + "</td>"
                    + "<td>" + quantityAceptace + "</td>"
                    + "<td>" + unitPrice * quantityAceptace + "</td>"

                    + "</tr>"
                )
            }
        })
        + "</table>"
        + "<h5>(Korefud Vietnam, " + today + ")</h5>"
        + "<p>---------------English version-------------------</p>" +
        "<H4>Dear you!</h4>" +
        "<h5>Order <b>" + orderId + "</b> was accepted with quantity: " + totalItemAcceptance + ", total money: " + totalMoneyAcceptance + "VND</h5>"
        + "<p>You will be received another email when package delivering.</p>" +
        "  <table> <tr><th>Name</th><th>Price</th><th>Order</th><th >Acceptance</th><th >Total</th></tr>" +
        CartComponent.map((item, idd) => {
            var { id, name, productCategoryName, authorName, currency, price, promotion_price, description, status, bigCategoryName, author, create_date } = item;


            const quantity = Cart[Cart.findIndex(x => x.id === id)] ? Cart[Cart.findIndex(x => x.id === id)]["quantity"] : 0;
            const quantityAceptace = Acceptance[Acceptance.findIndex(x => x.id === id)] ? Acceptance[Acceptance.findIndex(x => x.id === id)]["quantity"] : 0;
            const unitPrice = CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)] ? CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]["price"] : 0
            if (idd < Cart.length) {
                return ("<tr key={id}>"
                    + "<td>" + name + "</td>"
                    + "<td>" + unitPrice + "</td>"
                    + "<td>" + quantity + "</td>"
                    + "<td>" + quantityAceptace + "</td>"
                    + "<td>" + unitPrice * quantityAceptace + "</td>"

                    + "</tr>"
                )
            }
        })
        + "</table>" +
        "<h5>(Korefud Vietnam, " + today + ")</h5>"

    )

}

export const checkFinished = (usermail, Cart, CartComponentPrice, orderId, Acceptance, totalItemAcceptance, totalMoneyAcceptance, cb) => {
    var today = new Date().toLocaleString("vi");
    var once = true;

    getOrderById(orderId, result => {
        if (once == true) {
            once = false;
            db.collection("Order").doc(orderId + "").update({
                FinishedTime: today,
                cancel_state: false,
                status: true,
                finished_state: true,
                delivered_state: false,
                received_state: true,
                log_vi: result.log_vi + "\n Đang giao hàng.",
                log_en: result.log_en + "\n Order is delivering",
            }).then(() => { cb(true) }).catch(() => { cb(false) })
        }
    })
    adNotification(usermail, "Đơn hàng " + orderId + " đang được giao.", "Order " + orderId + " is being delivered.", "https://firebasestorage.googleapis.com/v0/b/korefud-d61f4.appspot.com/o/images%2Fyzsrh5LqH12VFRWp6Y12?alt=media&token=9cfc350f-d845-4747-999d-b72b69a64372", "order")
    SendMail(usermail, "Đơn hàng " + orderId + " đang được giao.",
        "<H4>Chào bạn!</h4>" +
        "<h5>Đơn hàng <b>" + orderId + "</b> đang được giao với số lượng: " + totalItemAcceptance + ", số tiền: " + totalMoneyAcceptance + "VND" + "</h5>" +
        "<h5>Vui lòng chuẩn bị tiền mặt trước.</h5>"
        + "<h5>(Korefud Vietnam, " + today + ")</h5>"
        + "<p>---------------English version-------------------</p>" +
        "<H4>Dear you!</h4>" +
        "<h5>Order <b>" + orderId + "</b> is delivering with quantity: " + totalItemAcceptance + ", total money: " + totalMoneyAcceptance + "VND</h5>"
        + "<h5>Please prepare your money." +
        "<h5>(Korefud Vietnam, " + today + ")</h5>"

    )
}
export const checkDelivered = (usermail, Cart, CartComponentPrice, orderId, Acceptance, totalItemAcceptance, totalMoneyAcceptance, cb) => {
    var today = new Date().toLocaleString("vi");
    var once = true;

    getOrderById(orderId, result => {
        //    console.log(result.delivered_state)
        if (once == true && !result.delivered_state) {
            once = false;

            db.collection("Order").doc(orderId + "").update({
                FinishedTime: today,
                cancel_state: false,
                status: true,
                finished_state: true,
                delivered_state: true,
                received_state: true,
                log_vi: result.log_vi + "\n Đang giao hàng.",
                log_en: result.log_en + "\n Order is delivering",
            }).then(() => { cb(true) }).catch(() => { cb(false) })

            adNotification(usermail, "Đơn hàng " + orderId + " đã được giao.", "Order " + orderId + " was delivered.", "https://firebasestorage.googleapis.com/v0/b/korefud-d61f4.appspot.com/o/images%2FiiGY9lL685z6mma0Dw45?alt=media&token=c7d2ecd4-8431-4102-b962-1d529026212f", "order")
            SendMail(usermail, "Đơn hàng " + orderId + " đã được giao.",
                "<H4>Chào bạn!</h4>" +
                "<h5>Đơn hàng <b>" + orderId + "</b> đã được giao với số lượng: " + totalItemAcceptance + ", số tiền: " + totalMoneyAcceptance + "VND" + "</h5>"
                + "<h5>Cảm ơn bạn đã mua hàng tại Korefud</h5>" +
                "<h5>(Korefud Vietnam, " + today + ")</h5>"
                + "<p>---------------English version-------------------</p>" +
                "<H4>Dear you!</h4>" +
                "<h5>Order <b>" + orderId + "</b> is delivered with quantity: " + totalItemAcceptance + ", total money: " + totalMoneyAcceptance + "VND</h5>"
                + "<h5>Thank you for buying at Korefud</h5>"
                + "<h5>(Korefud Vietnam, " + today + ")</h5>"
            )

        }
    })
}
export const getMenuByID = function (id, cb) {
    var finalResult = {};
    db.collection('menu').doc(id + "").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {
            cb(false)
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}
export const delMenuByID = (id, cb) => {

    db.collection("menu").doc(id).delete().then(function () {
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}
export const getMenuConfig = function (cb) {
    db.collection('menu-config').doc("1").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}



export const updateMenuConfig = (menu) => {

    var today = new Date().toLocaleString("vi");
    return db.collection("menu-config").doc("1").update({
        menu_config_type: menu.menu_config_type,
        menu_repeat_default: menu.menu_repeat_default,
        menu_monday: menu.menu_monday,
        menu_tuesday: menu.menu_tuesday,
        menu_wednesday: menu.menu_wednesday,
        menu_thursday: menu.menu_thursday,
        menu_friday: menu.menu_friday,
        menu_saturday: menu.menu_saturday,
        menu_sunday: menu.menu_sunday,
        author: localStorage.getItem("uid"),
        modified_date: today
    })

}
export const adProduct = (product) => {

    var today = new Date().toLocaleString("vi");
    return db.collection("product").add({
        name_en: product.name_en,
        name_vi: product.name_vi,
        big_category_id: product.BigCategoryID,
        description_en: product.description_en,
        description_vi: product.description_vi,
        img_url: product.img_url,
        meta_title: product.meta_title,
        meta_description: product.meta_description,
        meta_keyword: product.meta_keyword,
        create_date: today,
        modified_by: "",
        modified_date: "",
        author: localStorage.getItem("uid"),
        price: product.price,
        promotion_price: product.promotion_price,
        img_more: product.img_more,
        product_category_id: product.ProductCategoryID,
        ingredient_en: product.ingredient_en,
        nutritional_value_en: product.nutritional_value_en,
        ingredient_vi: product.ingredient_vi,
        nutritional_value_vi: product.nutritional_value_vi,
        currency: product.currency,
        view: 0,
        status: product.status

    })

}

export const getProduct = function (lang, cb) {
    var finalResult = [];
    db.collection('product').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {

                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        if (bigcategoryData.status && productCategoryData.status) {
                            result.push({
                                id: doc.id,
                                bigCategoryName: eval("bigcategoryData.name_" + lang),
                                productCategoryName: eval("productCategoryData.name_" + lang),
                                authorName: userData.name,
                                name: eval("name_" + lang),
                                big_category_id,
                                description: eval("description_" + lang),
                                img_url,
                                meta_title,
                                meta_description,
                                meta_keyword,
                                create_date,
                                author,
                                price,
                                promotion_price,
                                img_more,
                                product_category_id,
                                ingredient: eval("ingredient_" + lang),
                                nutritional_value: eval("nutritional_value_" + lang),
                                currency,
                                status
                            })
                        }
                        result = result.sort(function (a, b) { return (new Date(b.create_date.split(",")[1].split("/")[2], b.create_date.split(",")[1].split("/")[1], b.create_date.split(",")[1].split("/")[0], b.create_date.split(",")[0].split(":")[0], b.create_date.split(",")[0].split(":")[1], b.create_date.split(",")[0].split(":")[2]) - new Date(a.create_date.split(",")[1].split("/")[2], a.create_date.split(",")[1].split("/")[1], a.create_date.split(",")[1].split("/")[0], a.create_date.split(",")[0].split(":")[0], a.create_date.split(",")[0].split(":")[1], a.create_date.split(",")[0].split(":")[2])) });
                        cb(result);
                    })
                })

            })



        });

    });


}

export const getProductByIDLang = function (lang, id, cb) {
    var finalResult = [];

    db.collection('product').doc(id + "").get().then(function (doc) {

        if (doc.exists) {

            var result = {};

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        var modified_ID = modified_by ? modified_by : "0";
                        getUserAdminByID(modified_ID, (userModifiedData) => {
                            if (bigcategoryData.status && productCategoryData.status) {
                                result = {
                                    id: doc.id,
                                    bigCategoryName: eval("bigcategoryData.name_" + lang),
                                    productCategoryName: eval("productCategoryData.name_" + lang),
                                    authorName: userData.name,
                                    name: eval("name_" + lang),
                                    big_category_id,
                                    description: eval("description_" + lang),
                                    img_url,
                                    meta_title,
                                    meta_description,
                                    meta_keyword,
                                    create_date,
                                    author,
                                    price,
                                    promotion_price,
                                    img_more,
                                    product_category_id,
                                    ingredient: eval("ingredient_" + lang),
                                    nutritional_value: eval("nutritional_value_" + lang),
                                    currency,
                                    modified_by,
                                    modified_by_name: userModifiedData.name,
                                    modified_date,
                                    status
                                }
                            }

                            cb(result);

                        })
                    })

                })



            });
        } else {
            cb(false);
        }
    });


}

export const getProductByID = async function (id, cb) {
    var finalResult = [];

    await db.collection('product').doc(id + "").get().then(function (doc) {

        if (doc.exists) {

            var result = {};

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        var modified_ID = modified_by ? modified_by : "0";
                        getUserAdminByID(modified_ID, (userModifiedData) => {
                            if (bigcategoryData.status && productCategoryData.status) {
                                result = {
                                    id: doc.id,
                                    bigCategoryName: bigcategoryData.name_en,
                                    productCategoryName: productCategoryData.name_en,
                                    authorName: userData.name,
                                    name_en,
                                    name_vi,
                                    big_category_id,
                                    description_en,
                                    description_vi,
                                    img_url,
                                    meta_title,
                                    meta_description,
                                    meta_keyword,
                                    create_date,
                                    author,
                                    price,
                                    promotion_price,
                                    img_more,
                                    product_category_id,
                                    ingredient_en,
                                    nutritional_value_en,
                                    modified_by_name: userModifiedData.name,
                                    ingredient_vi,
                                    nutritional_value_vi,
                                    currency,
                                    modified_by,
                                    modified_date,
                                    status
                                }
                            }

                            cb(result);

                        })
                    })

                })



            });
        } else {
            cb(false);
        }
    });


}

export const searchProduct = function (lang, key, value, cb) {
    var finalResult = [];
    db.collection('product').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            // var obj[key]
            if (key == "name" || key == "description") {
                key = key + "_" + lang;
            }
            if (eval(key).toString().toLowerCase().includes(value.toLowerCase()))

                getBigCategoryById(big_category_id, (bigcategoryData) => {
                    getProductCategoryByID(product_category_id, (productCategoryData) => {
                        getUserAdminByID(author, (userData) => {
                            var modified_ID = modified_by ? modified_by : "0";
                            getUserAdminByID(modified_ID, (userModifiedData) => {
                                if (bigcategoryData.status && productCategoryData.status) {
                                    result.push({
                                        id: doc.id,
                                        bigCategoryName: eval("bigcategoryData.name_" + lang),
                                        productCategoryName: eval("productCategoryData.name_" + lang),
                                        authorName: userData.name,
                                        name: eval("name_" + lang),
                                        big_category_id,
                                        description: eval("description_" + lang),
                                        img_url,
                                        meta_title,
                                        meta_description,
                                        meta_keyword,
                                        create_date,
                                        author,
                                        price,
                                        promotion_price,
                                        img_more,
                                        product_category_id,
                                        ingredient: eval("ingredient_" + lang),
                                        nutritional_value: eval("nutritional_value_" + lang),
                                        currency,
                                        status
                                    });
                                }

                                cb(result);

                            })
                        })

                    })



                });


        });

    });


}



export const updateProduct = (id, product) => {
    console.log(product)
    var today = new Date().toLocaleString("vi");
    return db.collection("product").doc(id).update({
        name_en: product.name_en,
        name_vi: product.name_vi,
        big_category_id: product.BigCategoryID,
        description_en: product.description_en,
        description_vi: product.description_vi,
        img_url: product.img_url,
        meta_title: product.meta_title,
        meta_description: product.meta_description,
        meta_keyword: product.meta_keyword,
        modified_date: today,
        modified_by: localStorage.getItem("uid"),
        price: product.price,
        promotion_price: product.promotion_price,
        img_more: product.img_more,
        product_category_id: product.ProductCategoryID,
        ingredient_en: product.ingredient_en,
        nutritional_value_en: product.nutritional_value_en,
        ingredient_vi: product.ingredient_vi,
        nutritional_value_vi: product.nutritional_value_vi,
        currency: product.currency,
        view: 0,
        status: product.status
    })

}

export const delProductByID = (id, cb) => {
    db.collection("product").doc(id).delete().then(function () {
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}

export const adProductCategory = (category) => {
    var today = new Date().toLocaleString("vi");
    return db.collection("product-category").add({
        name_vi: category.name_vi,
        name_en: category.name_en,
        big_category_id: category.BigCategoryID,
        description_vi: category.description_vi,
        description_en: category.description_en,
        img_url: category.img_url,
        meta_title: category.meta_title,
        meta_description: category.meta_description,
        meta_keyword: category.meta_keyword,
        create_date: today,
        author: localStorage.getItem("uid"),
        status: category.status
    })

}
export const updateProductCategory = (id, category) => {
    console.log(category)
    var today = new Date().toLocaleString("vi");
    return db.collection("product-category").doc(id).update({
        name_vi: category.name_vi,
        name_en: category.name_en,
        big_category_id: category.BigCategoryID,
        description_vi: category.description_vi,
        description_en: category.description_en,
        img_url: category.img_url,
        meta_title: category.meta_title,
        meta_description: category.meta_description,
        meta_keyword: category.meta_keyword,
        modified_date: today,
        modified_by: localStorage.getItem("uid"),
        status: category.status
    })

}

export const getProductCategoryByID = function (id, cb) {
    var finalResult = {};

    db.collection('product-category').doc(id + "").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {


            cb(false);
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });
}

export const getProductCategoryByBigCategoryID = function (lang, BigCategoryID, cb) {
    var finalResult = [];
    console.log(BigCategoryID)
    db.collection('product-category').where("big_category_id", "==", BigCategoryID + "").where("status", "==", true).onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();

            getBigCategoryById(big_category_id, (data) => {
                // console.log(data.name)
                result.push({
                    id: doc.id,
                    name: eval("name_" + lang),
                    description: eval("description_" + lang),
                    bigCategoryName: data.name,
                    status,
                    status,
                    big_category_id,
                    author,
                    create_date
                })
                cb(result);
            })


        });

    });


}
export const getProductCategory = function (lang, cb) {
    var finalResult = [];
    db.collection('product-category').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();

            getBigCategoryById(big_category_id, (data) => {
                getUserAdminByID(author, (userData) => {
                    // console.log(data.name)
                    result.push({
                        id: doc.id,
                        name: eval("name_" + lang),
                        description: eval("description_" + lang),
                        bigCategoryName: eval("data.name_" + lang),
                        status,
                        big_category_id,
                        author_id: author,
                        author: userData.name,
                        create_date
                    })
                    result = result.sort(function (a, b) {
                        return (new Date(b.create_date) - new Date(a.create_date)) * 1
                    })
                    cb(result);
                })
            })


        });

    });


}

export const searchProductCategory = function (lang, key, value, cb) {
    var finalResult = [];
    db.collection('product-category').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {

            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();
            // var obj[key]
            if (key == "name" || key == "description") {
                key = key + "_" + lang;
            }

            if (eval(key).toString().toLowerCase().includes(value.toLowerCase()))
                getBigCategoryById(big_category_id, (data) => {
                    // console.log(data.name)
                    result.push({
                        id: doc.id,
                        name: eval("name_" + lang),
                        bigCategoryName: data.name,
                        status,
                        description: eval("description_" + lang),
                        big_category_id,
                        author,
                        create_date
                    })
                    cb(result);
                })


        });

    });


}


export const delProductCategory = (id, cb) => {

    db.collection("product-category").doc(id).delete().then(function () {
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}




const createUserStaffOnDB = function (name, username, email, url_profile, position, user_id, status) {
    var today = new Date().toLocaleString("vi");
    return db.collection("UserAdmin").add({
        name,
        username,
        email,
        user_id,
        url_profile,
        address: "",
        phone: "",
        Author: localStorage.getItem("uid"),
        create_date: today,
        position,
        status
    })
}
export const adUserStaff = function (name, username, email, password, url_profile, position, status, cb) {
    firebaseApp.auth().createUserWithEmailAndPassword(email, password)
        .then(function success(userData) {

            var user_id = userData.user.uid; // The UID of recently created user on firebase

            createUserStaffOnDB(name, username, email, url_profile, position, user_id, status).then(() => {
                cb(true)
            }).catch((e) => {
                cb(e)
            })
        }).catch((error) => {
            cb(error)

        });



}


export const updateUserstaffStatus = (id, cb) => {

    var today = new Date().toLocaleString("vi");
    var once = true;

    getUserAdminByID(id, (userData, uid) => {
        if (once) {
            once = false
            db.collection("UserAdmin").doc(uid + "").update({
                status: !userData.status

            }).then(() => { cb(true) }).catch(() => { cb(false) })
        }
    })

}
export const getUserStaff = function (cb) {
    var finalResult = [];
    db.collection('UserAdmin').onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                user_id,
                username,
                url_profile,
                email,
                Author,
                create_date,
                status
            } = doc.data();
            getUserAdminByID(Author, (userData) => {
                result.push({
                    id: doc.id,
                    user_id,
                    username,
                    url_profile,
                    email,
                    Author: userData.username,
                    create_date,
                    status
                })
                result = result.sort(function (a, b) { return (new Date(b.create_date.split(",")[1].split("/")[2], b.create_date.split(",")[1].split("/")[1], b.create_date.split(",")[1].split("/")[0], b.create_date.split(",")[0].split(":")[0], b.create_date.split(",")[0].split(":")[1], b.create_date.split(",")[0].split(":")[2]) - new Date(a.create_date.split(",")[1].split("/")[2], a.create_date.split(",")[1].split("/")[1], a.create_date.split(",")[1].split("/")[0], a.create_date.split(",")[0].split(":")[0], a.create_date.split(",")[0].split(":")[1], a.create_date.split(",")[0].split(":")[2])) });
                cb(result);

            })

        });

    });


}

export const getUserCustomer = function (cb) {
    var finalResult = [];
    db.collection('userCustomer').onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {

                user_profile,
                email,
                username,
                address,
                create_date,
                status
            } = doc.data();

            result.push({
                id: doc.id,
                user_profile,
                email,
                username,
                address,
                create_date,
                status
            })
            result = result.sort(function (a, b) { return (new Date(b.create_date.split(",")[1].split("/")[2], b.create_date.split(",")[1].split("/")[1], b.create_date.split(",")[1].split("/")[0], b.create_date.split(",")[0].split(":")[0], b.create_date.split(",")[0].split(":")[1], b.create_date.split(",")[0].split(":")[2]) - new Date(a.create_date.split(",")[1].split("/")[2], a.create_date.split(",")[1].split("/")[1], a.create_date.split(",")[1].split("/")[0], a.create_date.split(",")[0].split(":")[0], a.create_date.split(",")[0].split(":")[1], a.create_date.split(",")[0].split(":")[2])) });
            cb(result);

        })


    });


}

export const checkLogin = function (username, password, cb) {
    var finalResult = [];
    var result = false;
    var userExist = false;
    db.collection('UserAdmin').where("username", "==", username).onSnapshot(snapshot => {

        snapshot.forEach(doc => {
            userExist = true;

            if (doc.exists) {

                firebaseApp.auth().signInWithEmailAndPassword(doc.data().email, password).then((user) => {
                    // firebase.auth().onAuthStateChanged(function(user) {
                    result = true;
                    cb(true);
                    //     if (user) {
                    //       // User is signed in.

                    //       console.log(user)
                    //       var displayName   = user.displayName;
                    //       var email         = user.email;
                    //       var emailVerified = user.emailVerified;
                    //       var photoURL      = user.photoURL;
                    //       var isAnonymous   = user.isAnonymous;
                    //       var uid           = user.uid;
                    //       var providerData  = user.providerData;
                    //           result        = true;
                    //     
                    //       // ...
                    //     } else {
                    //       // User is signed out.
                    //       // ...
                    //     }
                    //   });



                }).catch(function (error) {
                    // Handle Errors here.
                    cb(false)
                    //   alert( error.code+" "+ error.message);
                    // ...
                });


                //  cb(doc.id);
            } else {
                cb(false);
            }
        })


        if (!userExist) cb(false)
    })


}

export const getUserAdminByID = function (id, cb) {
    var finalResult = {};

    db.collection('UserAdmin').where("user_id", "==", id).onSnapshot(snapshot => {
        var exist = false;
        snapshot.forEach(doc => {
            exist = true;
            if (doc.exists) {
                cb(doc.data(), doc.id);
            } else {


                cb(false);
            }
        })
        if (exist == false) cb(false);
    })
}
// export const actFetchBigCategoryRequest = () => {

//    var jo=   (dispatch) => {

//             db.collection('big-category').get().then(snapshot => {
//                 var result = []
//                 snapshot.forEach(doc => {

//                     const { name, status } = doc.data();

//                     result.push({
//                         id: doc.id,
//                         name: name, status
//                     })
//                     dispatch(actFetchCategory(result));

//                 });
//             });


//     }

//     return jo(dispatch)
// }

export const updateLanguage = (Language) => {

    return {
        type: Types.FETCH_LANGUAGE,
        Language
    }
}

export const actFetchBigCategory = (BigCategory) => {

    return {
        type: Types.FETCH_BIGCATEGORY,
        BigCategory
    }
}

export const actFetchProductCategory = (ProductCategory) => {

    return {
        type: Types.FETCH_PRODUCTCATEGORY,
        ProductCategory
    }
}


export const actFetchHotelsRequest = () => {
    return (dispatch) => {
        return callApi('hotel', 'GET', null).then((res) => {

            dispatch(actFetchHotels(res.data));

        });
    }
}

export const actFetchHotels = (hotels) => {
    return {
        type: Types.FETCH_HOTELS,
        hotels
    }
}


// export const actFetchBigCategoryRequest = ()=>{
//     return (dispatch)=>{
//         return apiBigCategory('hotel','GET', null).then((res)=>{

//             dispatch(actFetchCategory(res.data));

//         });
//     }
// }

// export const actFetchCategory = (hotels)=>{
//     return {
//         type: Types.FETCH_HOTELS,
//         hotels
//     }
// }