import {combineReducers} from 'redux';
import Hotel from './Hotel';
import BigCategory from './BigCategory';
import ProductCategory from './ProductCategory';
import Language from './Language';

const appReducers = combineReducers({
    BigCategory,
    ProductCategory,
    Language
})

export default appReducers;
