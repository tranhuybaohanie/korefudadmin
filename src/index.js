import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createStore, applyMiddleware } from 'redux';
import appReducers from './Storage/Reducers/Index';
import { Provider } from 'react-redux';

import registerServiceWorker from './registerServiceWorker';
import thunk from 'redux-thunk';
import 'bootstrap/dist/css/bootstrap.min.css' ;
import './Assets/css/fontawesome.min.css';
import 'jquery';
// import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.min.js';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import {faBandAid,faUser,faArchive,faGenderless,faBolt,faCubes,faBook,faListAlt,faBell,faEnvelope, faStar, faHome,faNewspaper,faBuilding,faPencilAlt,faTimes,faCircle,faPlus,faTrash,faEye,faSearch,faSignOutAlt} from '@fortawesome/free-solid-svg-icons' 


library.add(fab,faBandAid,faUser,faUser,faArchive,faGenderless,faBolt,faCubes,faBook, faListAlt,faBell,faEnvelope,faStar,faHome,faNewspaper,faBuilding,faPencilAlt,faTimes,faCircle,faPlus,faTrash,faEye,faSearch,faSignOutAlt)

const store = createStore(
    appReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)

);

ReactDOM.render(
    
    <Provider store={store}>
        <App />
    </Provider>

    , document.getElementById('root'));
registerServiceWorker();


 // "build": "react-scripts build",