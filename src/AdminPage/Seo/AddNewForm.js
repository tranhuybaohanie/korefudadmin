import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import { Z_DEFLATED } from 'zlib';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      setClassFixed: false
    }
    //  this.myRef = React.createRef();
  }
  //componentDrops.fetchAllHotels();

  componentWillMount() {
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();

  }
  componentWillUpdate() {

  }



  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }

  render() {
    setInterval(() => {
      //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
      // this.refs.setOfSubmitBtn.clientWidth=500;
      if (window.pageYOffset > this.refs.setOfSubmitBtn.clientWidth) {
        this.setState({
          setClassFixed: true
        })

      }
      else {
        this.setState({
          setClassFixed: false
        })
      }
    }, 1000);
    var Hotels = this.props.Hotels;

    return (
     
    
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">SEO info</h4>

                    <label class="col-sm-12 form-control-label">Material Inputs</label>
                    <divs class="col-sm-12">
                      <div class="form-group-material">
                        <input type="text" name="registerUsername" required="" class="input-material" />
                        <label for="register-susername" class="label-material"> MetaTitle</label>
                      </div>
                      <div class="form-group-material">
                        <input type="email" name="registerEmail" required="" class="input-material" />
                        <label for="register-email" class="label-material active">MetaDescription</label>
                      </div>
                      <div class="form-group-material">
                        <input type="phone" name="registerPassword" required="" class="input-material" />
                        <label class="label-material">Status</label>
                      </div>
                      <div class="form-group-material">
                        <input type="phone" name="registerPassword" required="" class="input-material" />
                        <label class="label-material">Phone</label>
                      </div>
                      <div class="form-group-material">
                        <input type="text" name="activities" required="" class="input-material" />
                        <label class="label-material">Activities</label>
                      </div>
                      <div class="form-group-material">
                        <input type="text" name="service" required="" class="input-material" />
                        <label class="label-material">Service</label>
                      </div>
                      <div class="form-group-material">
                        <input type="text" name="number_of_star" required="" class="input-material" />
                        <label class="label-material">Number of Star</label>
                      </div>
                      <div class="form-group-material">
                        <input type="text" name="web_link" required="" class="input-material" />
                        <label class="label-material">Web Link</label>
                      </div>
                      <div class="form-group-material">
                        <input type="text" name="total_room" required="" class="input-material" />
                        <label class="label-material">Total Room</label>
                      </div>
                    </divs>

                  </div>
                </div>

    
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels: state.Hotel
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllHotels: () => {
      dispatch(actFetchHotelsRequest());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);