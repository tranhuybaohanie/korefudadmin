import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';

let prev = 0;
let next = 0;
let last = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ok: "oyennnn",
      // todos: ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','T','v','u','w','x','y','z'],
      // currentPage: 1,
      // todosPerPage: 3,
      Hotels: this.props.Hotels,
      example: "1",
      currentPage: 1,
      todosPerPage: 3,
      HotelsPagination:null
    };



  }

  componentDidMount() {
    this.props.fetchAllHotels();



  }
  componentWillMount() {
    // this.props.history.push('/#/admin');

  }


  showHotelList = (Hotels) => {
 
    return Hotels.map((hotel, id) => {

      var { name, createBy, createDate, email, phone, webrating, numberOfStar, status, noOfRating } = hotel;
      return (<tr key={id}>
        <th scope="row">1</th>
        <td>{name}</td>
        <td>{createBy}</td>
        <td>{createDate}</td>
        <td>{email}</td>
        <td>{phone}</td>
        <td>{webrating}</td>
        <td>{noOfRating}</td>
        <td>{numberOfStar}</td>
        <td>{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td>
          <FontAwesomeIcon icon="pencil-alt" />
          <FontAwesomeIcon icon="times" />
        </td>
      </tr>
      );

    });
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }

  onclick = () => {
    this.setState({
      ok: "dlfasldjflkasdjflkjsdklff",
      example: "lalalalal",
      Hotels: this.props.Hotels
    }, () => {
      //console.log(this.state.Hotels)
    })

  }
  componentWillReceiveProps(newprops) {
 
   
    if (this.props != newprops) {
      this.setState({
        Hotels: newprops.Hotels
      }, () => {
        
      });
     
    }}
    setStateHotelsPagination=(state)=>{
   
      if(state!=this.state.HotelsPagination){
      //   console.log(state);
        this.setState({
      
          HotelsPagination: state
        })
      }
    
     
    }
  render() {


    var Hotels = this.state.Hotels;
    var HotelsPagination = this.state.HotelsPagination;

    return (

      <Layout>

       

        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2">
         
          <div className="card">
            <div className="card-header">
              <div className="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                  <h4 >Hotel managerment </h4>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">

                  {this.shortNavlink("/admin/add-new-product", "btn btn-success float-right", "Add new", "plus")}
                </div>
              </div>

            </div>
            <div className="card-body">
              <div className="table-responsive">

                <PaginationCustom  todos={Hotels}  onReceive={this.setStateHotelsPagination}>
                  <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>create_by</th>
                        <th>date</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>webrating</th>
                        <th>no_of_rating</th>
                        <th>number_of_star</th>
                        <th>status</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>



                      {HotelsPagination!=null?this.showHotelList(HotelsPagination):""}




                    </tbody>
                  </table>
                </PaginationCustom>

              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}


const mapStateToProps = state => {
  return {
    Hotels: state.Hotel
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllHotels: () => {
      dispatch(actFetchHotelsRequest());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);