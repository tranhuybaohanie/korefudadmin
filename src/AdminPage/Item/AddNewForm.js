import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import { Z_DEFLATED } from 'zlib';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state={
      setClassFixed:false
    }
  //  this.myRef = React.createRef();
  }
  //componentDrops.fetchAllHotels();

  componentWillMount() {
    // this.props.history.push('/#/admin');
  //  this.myRef.focus();
    
  }
  componentWillUpdate(){
   
  }



  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  
  render() {
  setInterval(()=>{
   //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
  // this.refs.setOfSubmitBtn.clientWidth=500;
    if(window.pageYOffset>this.refs.setOfSubmitBtn.clientWidth){
      this.setState({
        setClassFixed:true
      })
    
    }
    else{
      this.setState({
        setClassFixed:false
      })
    }
   },1000);
    var Hotels = this.props.Hotels;

    return (
      <Layout>

        <form>
        <div class="container">

          <div className="row">
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-2">

              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Basic info</h4>

                  <label class="col-sm-12 form-control-label">Material Inputs</label>
                  <div class="col-sm-12">
                    <div class="form-group-material">
                      <input type="text" name="registerUsername" required="" class="input-material" />
                      <label for="register-username" class="label-material">Hotel Name</label>
                    </div>
                    <div class="form-group-material">
                      <input type="email" name="registerEmail" required="" class="input-material" />
                      <label for="register-email" class="label-material active">Email</label>
                    </div>
                    <div class="form-group-material">
                      <input type="phone" name="registerPassword" required="" class="input-material" />
                      <label class="label-material">Phone</label>
                    </div>
                    <div class="form-group-material">
                      <input type="phone" name="registerPassword" required="" class="input-material" />
                      <label class="label-material">Phone</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="activities" required="" class="input-material" />
                      <label class="label-material">Activities</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="service" required="" class="input-material" />
                      <label class="label-material">Service</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="number_of_star" required="" class="input-material" />
                      <label class="label-material">Number of Star</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="web_link" required="" class="input-material" />
                      <label class="label-material">Web Link</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="total_room" required="" class="input-material" />
                      <label class="label-material">Total Room</label>
                    </div>
                  </div>

                </div>
              </div>

            </div>
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5 mt-2">

              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Basic info</h4>

                  <label class="col-sm-12 form-control-label">Material Inputs</label>
                  <div class="col-sm-12">
                    <div class="form-group-material">
                      <input type="text" name="registerUsername" required="" class="input-material" />
                      <label for="register-username" class="label-material">Hotel Name</label>
                    </div>
                    <div class="form-group-material">
                      <input type="email" name="registerEmail" required="" class="input-material" />
                      <label for="register-email" class="label-material active">Email</label>
                    </div>
                    <div class="form-group-material">
                      <input type="phone" name="registerPassword" required="" class="input-material" />
                      <label class="label-material">Phone</label>
                    </div>
                    <div class="form-group-material">
                      <input type="phone" name="registerPassword" required="" class="input-material" />
                      <label class="label-material">Phone</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="activities" required="" class="input-material" />
                      <label class="label-material">Activities</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="service" required="" class="input-material" />
                      <label class="label-material">Service</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="number_of_star" required="" class="input-material" />
                      <label class="label-material">Number of Star</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="web_link" required="" class="input-material" />
                      <label class="label-material">Web Link</label>
                    </div>
                    <div class="form-group-material">
                      <input type="text" name="total_room" required="" class="input-material" />
                      <label class="label-material">Total Room</label>
                    </div>
                  </div>

                </div>
              </div>

            </div>


        
        <div class=" col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <div className="line mt-2"/>
          <div className= {` container set-submit   text-center ${this.state.setClassFixed?"fixed-set-submit":"mt-2"}`} > 

          <button type="button" class="btn btn-success" ref="setOfSubmitBtn">Submit </button>
          {this.props.history != null ? <div className="mt-2"><button className="btn btn-default" onClick={this.props.history.goBack}>Go Back</button></div> : ""}
          </div>
          <div className="line mt-2"/>
        </div>
        

          </div>


        </div>
        </form>
      </Layout >
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels: state.Hotel
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllHotels: () => {
      dispatch(actFetchHotelsRequest());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);