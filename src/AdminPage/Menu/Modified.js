import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from '../../Ultils/StranlatetionData';
import Detail from './../Product/Detail'

// import "babel-polyfill";
let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ModifiedID : this.props.match.params.id,
      Language     : localStorage.getItem("lang"),
      ConfirmCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      AlertCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      delete: {
        name: "undified",
        id  : "11111111"
      },
      BigCategory  : [],
      pageNumber   : 1,
      loading      : true,
      displayDetail: false,
      detailByID   : "",
      ProductCategory       : [],
      ProductCategoryReal   : [],
      BigCategoryIDSearching: "",
      currentPage           : 1,
      todosPerPage          : 3,
      HotelsPagination      : null,
      searchText            : "",
      searchOption          : "name",
      showBtnSubmit         :  true,
      itemsMenu             : [],
      itemsMenuComponent    : [],
      slider                : [],
      caption_drink_en      : "",
      caption_drink_vi      : "",
      caption_food_en       : "",
      caption_food_vi       : "",
      title_drink_en        : "",
      title_drink_vi        : "",
      title_food_en         : "",
      title_food_vi         :  "",
      menu_name             : "",
      validate              : {
        menu_name      : {
          id        : "menu_name",
          length_min: 0,
          length_max: 50,
          required  : false,
          msg       : ""
        },
        caption_drink_en      : {
        id        : "caption_drink_en",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      caption_drink_vi: {
        id        : "caption_drink_vi",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      caption_food_en: {
        id        : "caption_food_en",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      caption_food_vi: {
        id        : "caption_food_vi",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      title_drink_en: {
        id        : "title_drink_en",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      title_drink_vi: {
        id        : "title_drink_vi",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      title_food_en: {
        id        : "title_food_en",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      },
      title_food_vi: {
        id        : "title_food_vi",
        length_min: 0,
        length_max: 50,
        required  : false,
        msg       : ""
      }
    }
    };

    // this.showItemList = this.showItemList.bind(this);

  }


  
  addStateEvent = (e) => {
    
    const input      = e.target;
    var   length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
  }
  
  addStateEventCB = (e,cb) => {
    const input = e.target;
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      },
    ()=>{
      cb();
    })

  }
  
  addStateNameKey = (name, value) => {
    if(this.state[name]!=value){
    this.setState({
      [name]: value
    })}

  }
  addToitemsMenu=(e)=>{
    var id = this.state.itemsMenu;
    if(!this.state.itemsMenu.includes(e.target.dataset.id)){
     id.push(e.target.dataset.id);
      this.setState({
        itemsMenu: id
      })
      var menu = this.state.itemsMenuComponent;
          actionRedux.getProductByIDLang(this.state.Language,e.target.dataset.id,(result)=>{  
            menu.push(result);
          this.setState({
            itemsMenuComponent: menu
          })
        })
      }
  }

  removeToitemsMenu=(e)=>{
    var id     = this.state.itemsMenu;
        id     = this.arrayRemove(id,e.target.dataset.id)
    var slider = this.state.slider;
        if(slider.includes(e.target.dataset.id)){
          slider = this.arrayRemove(slider,e.target.dataset.id)
        }
    if(this.state.itemsMenu.includes(e.target.dataset.id)){
      var menu  = this.state.itemsMenuComponent;
      var final = [];
      menu.map((item,id)=>{
          if(item.id!=e.target.dataset.id){
            final.push(item)
          }
      })
      this.setState({
        itemsMenu         : id,
        itemsMenuComponent: final,
        slider            : slider
      })
      }
  }
  arrayRemove(arr, value) {

    return arr.filter(function(ele){
        return ele != value;
    });
 
 }
 addToSlider=(e)=>{
  var idProduct = e.target.dataset.id;
  var slider    = this.state.slider;
  if(slider.includes(idProduct)){
    slider = this.arrayRemove(slider,idProduct)
  }else{
  slider.push(idProduct)
  }
  console.log(slider)
  this.setState({slider:slider})
 }
  big_category = () => {
    actionRedux.getBigCategoryById(big_category_id, (data) => { big_category_name = data.name })
    this.setState({ big_category })
  }
  componentDidMount() {
    actionRedux.getBigCategory((result) => {
      this.setState({BigCategory:result})
    });
    actionRedux.getProduct(this.state.Language,(result) => {
     
      this.setState({ProductCategory:result})
      // this.props.fetchAllProductCategory(result);
    });

    actionRedux.getMenuByID(this.state.ModifiedID,(result) => {
      if(result==false){
        this.props.history.push('/admin/menu');
      }
     
      if(result!=false&&result){
        console.log(result)
        result.itemsMenu.map((item,id)=>{
          var menu = this.state.itemsMenuComponent;
          actionRedux.getProductByIDLang(this.state.Language,item,(pro)=>{  
            menu.push(pro);
          this.setState({
            itemsMenuComponent: menu
          })
        })
        })
        this.setState({
        itemsMenu             : result.itemsMenu,
        slider                :result.slider,
        caption_drink_en      : result.caption_drink_en,
        caption_drink_vi      :result.caption_drink_vi,
        caption_food_en       : result.caption_food_en,
        caption_food_vi       : result.caption_food_vi,
        title_drink_en        : result.title_drink_en,
        title_drink_vi        :result.title_drink_vi,
        title_food_en         : result.title_food_en,
        title_food_vi         : result.title_food_vi,
        menu_name             : result.name,
      })
      }
    });
  }
  deleteProductCategory = (e) => {
    if (e.target.id) {
      var name = e.target.name;
      var id   = e.target.id;
      this.setState({
        delete: {
          id,
          name
        }
      }, () => {
        this.setStateConfirmCustom("warning", this.stran("confirm"), this.stran("Do you realy want to delete: ") + this.state.delete.name)
      })
    }
  }
  // componentWillMount() {
  //   // this.props.history.push('/#/admin');

  // }
  showItemMenu = (item) => {
    return item.map((result, idd) => {   
     var { id, name,productCategoryName,authorName,currency, price,promotion_price, description, status, bigCategoryName, author, create_date } = result;
      return (<tr key={idd} className="animated tada">
        <th scope="row">{(idd+1)+(10*(this.state.pageNumber-1))}</th>
        <td>{name}</td>
        <td>{bigCategoryName}</td>
        <td>{productCategoryName}</td>
        <td>{authorName}</td>
        <td>{price}({currency})</td>
        <td>{promotion_price}({currency})</td>
        <td>{create_date}</td>
        <td>{this.state.slider.includes(id) ?<label data-id={id}  onClick={this.addToSlider} className="btn btn-sm btn-warning">Added</label>:<button  data-id={id} className="btn btn-sm btn-secondary" onClick={this.addToSlider}>Add</button>}</td>
        <td className="text-center">{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td className="text-center">
          <button className="btn-action text-darkcyan" id={id}  data-id={id} onClick={this.showDetail}>
             <img src={require("./../../Assets/img/icon/eye.png")} style={{ paddingBottom: "3px" }} id={id} name={name} onClick={this.showDetail} width="15px" className="img-responsive" />
          </button>
         <button  data-id={id} className="btn btn-sm btn-danger" onClick={this.removeToitemsMenu}>X</button>
      
        </td>
      </tr>
      );
  
    })
    
  }
  showItemList = (item) => {
    return item.map((me, idd) => {
      var { id, name,productCategoryName,authorName,currency, price,promotion_price, description, status, bigCategoryName, author, create_date } = me;
      
     
      return (<tr key={idd}>
        <th scope="row">{(idd+1)+(10*(this.state.pageNumber-1))}</th>
        <td >{name}</td>
        <td>{bigCategoryName}</td>
        <td>{productCategoryName}</td>
        <td>{authorName}</td>
        <td>{price}({currency})</td>
        <td>{promotion_price}({currency})</td>
        <td>{create_date}</td>
        <td className="text-center">{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td className="text-center">
          <button className="btn-action text-darkcyan" id={id}  data-id={id} onClick={this.showDetail}>
             <img src={require("./../../Assets/img/icon/eye.png")} style={{ paddingBottom: "3px" }} id={id} name={name} onClick={this.showDetail} width="15px" className="img-responsive" />
          </button>
          {this.state.itemsMenu.includes(id) ?<label className="btn btn-sm btn-secondary">Added</label>:<button  data-id={id} className="btn btn-sm btn-info" onClick={this.addToitemsMenu}>Add</button>}
         
          {/* <span className="btn-action text-darkyellow" id={id}  data-id={id}>
          {this.shortNavlink("/admin/modified-product/"+id, "text-darkyellow", "", "pencil-alt")}
          </span> 
         
          <button className="btn-action text-darkcyan" name={name} id={id} onClick={this.deleteProductCategory} data-id={id}>

            <img src={require("./../../Assets/img/icon/trash.png")} style={{ paddingBottom: "3px" }} id={id} name={name} onClick={this.deleteProductCategory} width="15px" className="img-responsive" />
        
          </button> */}
        </td>
      </tr>
      );

    });
    
  }
  showDetail=(e)=>{

    this.setState({
      displayDetail: true,
      detailByID   : e.target.id
    });
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }

  shortNavlink = (to, type, text, icon) => {
    return <li style={{    listStyleType:"none"}}><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }


  componentWillReceiveProps(newprops) {

    var previousLang = this.props.Lang[0]["lang"];
    if (this.props != newprops) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      
      }, () => {
       
        if(previousLang!=this.state.Language){
          actionRedux.getProduct(this.state.Language,(result) => {
       
            this.setState({ProductCategory:result})
            // this.props.fetchAllProductCategory(result);
          });
        }
      });

    }

  }
  BigCategoryGet = () => {
  
    return this.state.BigCategory.map((item, id) => {
      return (
        <option value={item.id} >{item.name}</option>
      );
    });
  }
  OnSelectBigcategory =(e)=>{

    actionRedux.getProductCategoryByBigCategoryID(this.state.Language,e.target.value,(result) => {
      this.setState({ProductCategoryReal:result})
    });
  }
  ProductCategoryGet = () => {
   
    return this.state.ProductCategoryReal.map((item, id) => {
      return (
        <Fragment>
       
        <option value={item.id} >{item.name}</option>
        <option selected>Choose here</option>
        </Fragment>
      );
    });
  }

  setStateHotelsPagination = (state,pageNumber) => {

    if (state != this.state.HotelsPagination) {
      setTimeout(()=>{
        this.setState({
          loading: false
        })
      },1000)
     
      this.setState({
       
        pageNumber,
        HotelsPagination: state
      })
    }


  }

  setStateConfirmCustom = (type, title, content, time) => {
    this.setState({
      ConfirmCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.ConfirmCustom.session + 1
      },
    })
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  search=(e)=>{
    // console.log(this.state.searchOption+"ok")
    this.addStateEventCB(e,()=>{
    if(this.state.searchText!=""){
      actionRedux.searchProduct(this.state.Language,this.state.searchOption,this.state.searchText,(result) => {
        if(result!=false){
          this.setState({ProductCategory:result})
        }
        
      });
    }else{
      actionRedux.getProduct(this.state.Language,(result) => {
        this.setState({ProductCategory:result})
      });
    }
  })

  }
  submit=()=>{
    var me=this;
    me.setState({showBtnSubmit:false})
    var menu={
      name:this.state.menu_name,
      itemsMenu             : this.state.itemsMenu,
      slider                : this.state.slider,
      caption_drink_en      : this.state.caption_drink_en,
      caption_drink_vi      : this.state.caption_drink_vi,
      caption_food_en       : this.state.caption_food_en,
      caption_food_vi       : this.state.caption_food_vi,
      title_drink_en        : this.state.title_drink_en,
      title_drink_vi        : this.state.title_drink_vi,
      title_food_en         : this.state.title_food_en,
      title_food_vi         : this.state.title_food_vi,
    }
    actionRedux.updateMenu(this.state.ModifiedID,menu).then(function () {
      me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You updated:") +me.state.menu_name)
      me.setState({showBtnSubmit: true})
   })
     .catch(function (error) {
      me.setState({showBtnSubmit: true})
       me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ")+ me.state.menu_name + ". error: " + error)
     })


  }
  yesConfirmDeleted = () => {
    this.state.ConfirmCustom.display  = false;
    this.state.ConfirmCustom.session += 1;
    // alert(this.state.delete.id)
    actionRedux.delProductByID(this.state.delete.id, (result) => {
      if (result == true) {
        this.setStateAlertCustom("success", this.stran("Congratulation!"), this.stran("You have deleted: ") + this.state.delete.name)

      } else {
        this.setStateAlertCustom("danger", this.stran("Fail"), this.stran("there are some thing wrong cannot delete: "))
      }
      this.setState({
        delete: {
          id  : "",
          name: ""
        }
      });
    })

  }
  render() {


    var { ProductCategory,itemsMenuComponent } = this.state;
    var HotelsPagination                       = this.state.HotelsPagination;
    var stranl                                 = this.stran;
    return (

      <Layout history={this.props.history}>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 ">

          <div className="card">
            <div className="card-header">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h4 >{stranl("Update new awesome menu")}  </h4></div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div className="card">
            <div className="card-header">
            <h5>Menu</h5>
            <div className="form-group-material d-flex flex-row">
                            <label className="">{stranl("Menu name")} ({this.state.menu_name.length}/50) </label>
                            <input type="text" name="menu_name" required="" value={this.state.menu_name} className="input-material form-control ml-2" onChange={this.addStateEvent} style={{width:"39%"}}/>
                            <button style={{width:"339px",height:"36px",display:this.state.showBtnSubmit?"block":"none"}} type="button" className="btn btn-success ml-5" ref="setOfSubmitBtn" onClick={this.submit}>Submit </button>
                  {this.props.history != null ? <div className="mt-2">
                  <button style={{    marginTop: "-10px"}} className="btn btn-default ml-2" onClick={(e)=> {e.preventDefault();this.props.history.goBack();}}>Go Back</button></div> : ""}

                          </div>

            </div>
            <div className="card-body">
            <div className="row">
            <div className="col-lg-4">
                       <div className="form-group-material">
                            <label className="">{stranl("Title food")}_en ({this.state.title_food_en.length}/50) </label>
                            <input type="text" name="title_food_en" required="" value={this.state.title_food_en} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                        <div className="form-group-material">
                            <label className="">{stranl("Caption food")}_en ({this.state.caption_food_en.length}/50) </label>
                            <input type="text" name="caption_food_en" required="" value={this.state.caption_food_en} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Title food")}_vi ({this.state.title_food_vi.length}/50) </label>
                            <input type="text" name="title_food_vi" required="" value={this.state.title_food_vi} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Caption food")}_vi ({this.state.caption_food_vi.length}/50) </label>
                            <input type="text" name="caption_food_vi" required="" value={this.state.caption_food_vi} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Title drink")}_en ({this.state.title_drink_en.length}/50) </label>
                            <input type="text" name="title_drink_en" required="" value={this.state.title_drink_en} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Caption Drink")}_en ({this.state.caption_drink_en.length}/50) </label>
                            <input type="text" name="caption_drink_en" required="" value={this.state.caption_drink_en} className="input-material form-control" onChange={this.addStateEvent} />
                            
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Title drink")}_vi ({this.state.title_drink_vi.length}/50) </label>
                            <input type="text" name="title_drink_vi" required="" value={this.state.title_drink_vi} className="input-material form-control" onChange={this.addStateEvent} />
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Caption Drink")}_vi ({this.state.caption_drink_vi.length}/50) </label>
                            <input type="text" name="caption_drink_vi" required="" value={this.state.caption_drink_vi} className="input-material form-control" onChange={this.addStateEvent} />
                            
                          </div>

              </div>
              <div className="col-lg-8 d-flex flex-column align-items-center" style={{    height: "100%"}} >
              <h6 className="text-center">{stranl("Food screen")}</h6>   <hr/>
              <div className="col-lg-9 d-flex flex-column align-items-center" >
              <img className="img-responsive animated bounce" width="82%" src={require("./../../Assets/img/iphonescreen.png")}/>
             <div className="text-center" style={{    position: "absolute",top: "120px",  left: "107px",width: "288px"}}>
              <h6 >{eval("this.state.title_food_"+this.state.Language)}</h6>
              <p>{eval("this.state.caption_food_"+this.state.Language)}</p>
              </div>
              </div>
              <hr className="text-center mt-2"/>
              <h6 className="text-center mt-5">{stranl("Drink screen")}</h6>   <hr/>
              <div className="col-lg-9 d-flex flex-column align-items-center" >
              <img className="img-responsive animated bounce" width="82%" src={require("./../../Assets/img/iphonescreen.png")}/>
              <div className="text-center" style={{    position: "absolute",top: "120px",  left: "107px",width: "288px"}}>
              <h6 >{eval("this.state.title_drink_"+this.state.Language)}</h6>
              <p>{eval("this.state.caption_drink_"+this.state.Language)}</p>
              </div>
              </div>
              </div>
              </div>
              <hr/>
              <h5 className="text-center">{stranl("Menu items")}</h5>
              <hr/>
 <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>

                        <th>{stranl("Name")}</th>
                        <th>{stranl("big category")}</th>
                        <th>{stranl("product category")}</th>
                        <th >{stranl("Author")}</th>
                        <th>{stranl("price")}</th>
                        <th>{stranl("promotion price")}</th>
                        <th>{stranl("CreateDate")}</th>
                        <th className="text-center">{stranl("Slider")}</th>
                        <th className="text-center">{stranl("Status")}</th>
                        <th className="text-center">{stranl("Action")}</th>

                      </tr>
                    </thead>
                    <tbody>
{this.showItemMenu(itemsMenuComponent)}
</tbody>
</table>

 </div>  
 <hr/>
            </div>


            </div>
              <div className="row">
              
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 animated bounceIn mt-1">
                  {/* {this.shortNavlink("/admin/add-new-product", "btn btn-success ", stranl("Add new awesome menu"), "plus")} */}
                </div>
                <hr/>
                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">

                <div className="input-group d-flex justify-content-end">
                {this.state.searchOption=="product_category_id"?  
                <div className="d-flex flex-row justify-content-between" style={{    position: "absolute",top: "0%",right:"35%",width:"651px"}}>
                            <label className="">{stranl("big category")}</label>
                            <select name="BigCategoryIDSearching" className="form-control" onChange={  this.OnSelectBigcategory} style={{width:"200px"}}>
                              <option selected disabled hidden>Choose here</option>
                              {this.BigCategoryGet()}
                            </select>
                      
                            <label className="">{stranl("product category")}</label>
                            <select name="searchText" className="form-control" value="" onChange ={this.search} style={{width:"200px"}} >
                              <option selected disabled hidden>Choose here</option>
                              {this.ProductCategoryGet()}
                            </select>
                         
                  </div>
                  :                                                                                                                                                                                this.state.searchOption=="status"?<span> <button className="btn btn-success border-right-0 border" name="searchText" value="true" type="button" onClick={this.search}>
                   Active
                </button>
                <button className="btn btn-danger border-left-0 border" name="searchText" value="false" type="button" onClick={this.search}>
                   InActive
                </button> </span>
              :                                                                                                                                                                                <input type="text" name="searchText" required="" value={this.state.searchText} className="rounded-left p-1 control-form border-right-0 border" onChange={this.search} />
                }
            <span className="input-group-append">
                  
                <select className="form-control" name="searchOption" onChange={this.addStateEvent}>
                <option  value="name">Name</option>
                  <option value="status">Status</option>
                   <option value="author">Author</option>
                   <option value="create_date">Date</option>
                   <option value="product_category_id">Category</option>

                   
                </select>
                <button className="btn btn-outline-secondary border-left-0 border" name="searchText" value="" type="button" onClick={(e)=>{this.search(e);}}>
                   X
                </button> 
               
              </span>
              </div>
                </div>
              </div>

            </div>
            <div className="card-body">
              <div className="table-responsive">

                <PaginationCustom todos={ProductCategory} onReceive={this.setStateHotelsPagination}>
                  <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>

                        <th>{stranl("Name")}</th>
                        <th>{stranl("big category")}</th>
                        <th>{stranl("product category")}</th>
                        <th >{stranl("Author")}</th>
                        <th>{stranl("price")}</th>
                        <th>{stranl("promotion price")}</th>
                        <th>{stranl("CreateDate")}</th>
                        <th className="text-center">{stranl("Status")}</th>
                        <th className="text-center">{stranl("Action")}</th>

                      </tr>
                    </thead>
                    <tbody>



                      {HotelsPagination != null ? this.showItemList(HotelsPagination) : ""}




                    </tbody>
                  </table>
                </PaginationCustom>

              </div>
            </div>
          </div>
        </div>
        <img style={{display:this.state.loading?"block":"none"}} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%"/>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session} />
        <ConfirmCustom type={this.state.ConfirmCustom.type} title={this.state.ConfirmCustom.title} content={this.state.ConfirmCustom.content} display={this.state.ConfirmCustom.display} time={this.state.ConfirmCustom.time} session={this.state.ConfirmCustom.session} yes={this.yesConfirmDeleted} />
        <Detail detailByID={this.state.detailByID} displayDetail={this.state.displayDetail} setShow={()=>{this.setState({displayDetail:false})}}></Detail>
      </Layout>
    );
  }
}



const mapStateToProps = state => {
  return {
    Lang: state.Language,
    // ProductCategory: state.ProductCategory
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllProductCategory: (result) => {

      dispatch(actionRedux.actFetchProductCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);