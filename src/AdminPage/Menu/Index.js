import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from '../../Ultils/StranlatetionData';
let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      Language   : localStorage.getItem("lang"),
      ConfirmCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      AlertCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      delete: {
        id:"",
        name:""
      },
      BigCategory: this.props.BigCategory,
      showBtnSubmit:true,
      currentPage     : 1,
      todosPerPage    : 3,
      HotelsPagination: null,
      Menu:[],
      itemsMenu:[],
      menu_config_type:"repeat_default",
      menu_repeat_default:0, 
      menu_monday:"",
      menu_tuesday:"",
      menu_wednesday:"",
      menu_thursday:"",
      menu_friday:"",
      menu_saturday:"",
      menu_sunday:"",

    };



  }
  
  addStateEvent = (e) => {
   
    const input      = e.target;
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
  
  
  btnAddNew=()=>{
    this.props.history.push('/admin/add-new-menu');
  }
  btnDuplicate=(e)=>{
    actionRedux.duplicateMenu(e.target.id,(result)=> {
if(result){
      me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You duplicated:") +e.target.name)
    
}else{
    
       me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ")+e.target.name)
      }
  })
}
  componentDidMount() {
    actionRedux.getBigCategory((result) => {
     
      this.props.fetchAllBigCategory(result);
    });

actionRedux.getMenuConfig((result)=>{
  if(result!=false){
    this.setState({
    menu_config_type:result.menu_config_type,
    menu_repeat_default:result.menu_repeat_default, 
    menu_monday:result.menu_monday,
    menu_tuesday:result.menu_tuesday,
    menu_wednesday:result.menu_wednesday,
    menu_thursday:result.menu_thursday,
    menu_friday:result.menu_friday,
    menu_saturday:result.menu_saturday,
    menu_sunday:result.menu_sunday
  },()=>{
    console.log(this.state)
  })
  }
})

actionRedux.getMenu((result)=>{
this.setState({
  Menu:result
}
,()=>{
  })

}
)

  }
  // componentWillMount() {
  //   // this.props.history.push('/#/admin');

  // }


  deleteMenu = (e) => {
    if (e.target.id) {
      var name = e.target.name;
      var id   = e.target.id;
      this.setState({
        delete: {
          id,
          name
        }
      }, () => {
        this.setStateConfirmCustom("warning", this.stran("confirm"), this.stran("Do you realy want to delete: ") +name)
      })
    }
  }
  showItemMenu= (itemsMenu)=>{


    return  itemsMenu.map((itemsm,ik)=>{
  

var {name_vi,name_en}=itemsm;
   return (<p className="text-center" style={{height:"16px"}}>
{eval("name_"+this.state.Language)}
    </p>)
    })
 
  }
  showListMenu=(arr)=>{
    return arr.map((item, idd) => {

      var {menu_id, name, itemsMenu,slider ,caption_drink_en ,caption_drink_vi , caption_food_en , caption_food_vi,title_drink_en,title_drink_vi,title_food_en,title_food_vi,create_date,modified_by,modified_date,author,view, status         } = item;

      return(
        <div className="ml-4 mt-5 mb-5 col-lg-2  d-flex flex-column  align-items-center">
        
        <div className=" col-lg-12 d-flex flex-column  align-items-center" style={styles.ItemMenu}>
       {this.state.menu_config_type=="repeat_default"?
        <button name="menu_repeat_default" value={menu_id} className={this.state.menu_repeat_default==menu_id?"btn btn-info":"btn btn-dark "} style={{position: "absolute",top: "-14%",width: "67%",height: "31px",lineHeight: "1"}} onClick={this.addStateEvent}>{this.state.menu_repeat_default==menu_id?"Default":"InActive"}</button>
        :<div className="d-flex justify-content-beetween flex-wrap" style={{...styles.comboWeekDay,position: "absolute",top: "-29%"}}>
         <button name="menu_monday" className={this.state.menu_monday==menu_id?"mb-1 btn-info":"mb-1 btn-dark"} value={menu_id} onClick={this.addStateEvent}>MO</button>
         <button name="menu_tuesday" className={this.state.menu_tuesday==menu_id?"ml-3 mr-3 mb-1 btn-info":"ml-3 mr-3 mb-1 btn-dark"} value={menu_id} onClick={this.addStateEvent}>TU</button>
         <button name="menu_wednesday" className={this.state.menu_wednesday==menu_id?"btn-info":" btn-dark"}  value={menu_id} onClick={this.addStateEvent}>WE</button>
         <button name="menu_thursday" className={this.state.menu_thursday==menu_id?" btn-info":" btn-dark"}  value={menu_id} onClick={this.addStateEvent}>TH</button>
         <button name="menu_friday" className={this.state.menu_friday==menu_id?"ml-1 mr-1 btn-info":"ml-1 mr-1 btn-dark"} value={menu_id} onClick={this.addStateEvent}>Fr</button>
         <button name="menu_saturday" className={this.state.menu_saturday==menu_id?"ml-1 mr-1 btn-info":"ml-1 mr-1 btn-dark"} value={menu_id} onClick={this.addStateEvent}>SA</button>
         <button name="menu_sunday" className={this.state.menu_sunday==menu_id?" btn-info":"btn-dark"} value={menu_id}  onClick={this.addStateEvent}>SU</button>

       </div>}
       <button className="btn btn-danger"  name={name} id={menu_id}  onClick={this.deleteMenu} style={{    position: "absolute", width: "11px",left: "0px",height: "28px",lineHeight: "0"}}>x</button>
       <span className="btn-action text-darkyellow" id={menu_id}  data-id={menu_id} style={{    position: "absolute", width: "11px",right: "2px",height: "28px",lineHeight: "0"}}>
          {this.shortNavlink("/admin/modified-menu/"+menu_id, "text-darkyellow", "", "pencil-alt")}
          </span> 
        <div className=" menu-item-title"  style={{backgroundColor:"gray"}}>
        <h6 className="col-lg-12">{name.length>9?<marquee scrolldelay="200">{name}</marquee>:name}</h6>
        </div>
        <div className="menu-item-body" style={{overflowY: "scroll",backgroundColor:"white",width: "119%",fontSize: "11px"}}> 
        {
  
         
          this.showItemMenu(itemsMenu)
        }
          
      </div>
      </div>
        <img className="mt-1" src={require("./../../Assets/img/icon/duplicate.png")} id={menu_id} name={name} onClick={this.btnDuplicate}/>
      </div>
      );
    })
  }
  showItemList = (item) => {

    return item.map((me, idd) => {

      var { id, name, description, status } = me;
      return (<tr key={idd}>
        <th scope="row">{idd}</th>
        <td>{id}</td>
        <td>{name}</td>

        <td>{description}</td>
        <td>{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td>
          {/* <FontAwesomeIcon icon="pencil-alt" />|||

        <button id={id} onClick={this.deleteBigCategory} data-id={id}>
            <FontAwesomeIcon className="text-danger" icon="times" />
          </button> */}
        </td>
      </tr>
      );

    });
  }

  deleteBigCategory = (e) => {
    actionRedux.delBigCategory(e.target.id)
    
  }
  shortNavlink = (to, type, text, icon) => {
    return <NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> 
  }

  onclick = () => {
    this.setState({
      ok     : "dlfasldjflkasdjflkjsdklff",
      example: "lalalalal",
      Hotels : this.props.Hotels
    }, () => {
      //console.log(this.state.Hotels)
    })

  }

  
  componentWillReceiveProps(newprops) {

    var previousLang = this.props.Lang[0]["lang"];
    if (this.props != newprops) {
      this.setState({
        Language   : newprops.Lang[0]["lang"],
        BigCategory: newprops.BigCategory,
      }, () => {
       
        // if(previousLang!=this.state.Language){
        //   actionRedux.getProductCategory(this.state.Language,(result) => {
        //     this.props.fetchAllProductCategory(result);
        //   });
        // }
      });

    }
  }
  setStateHotelsPagination = (state) => {

    if (state != this.state.HotelsPagination) {

      this.setState({

        HotelsPagination: state
      })
    }


  }
  
  setStateConfirmCustom = (type, title, content, time) => {
    this.setState({
      ConfirmCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.ConfirmCustom.session + 1
      },
    })
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  saveChanges=()=>{
    var me=this;
    me.setState({
      showBtnSubmit : false
    })
    var menu_config={
    menu_config_type:this.state.menu_config_type,
    menu_repeat_default:this.state.menu_repeat_default, 
    menu_monday:this.state.menu_monday,
    menu_tuesday:this.state.menu_tuesday,
    menu_wednesday:this.state.menu_wednesday,
    menu_thursday:this.state.menu_thursday,
    menu_friday:this.state.menu_friday,
    menu_saturday:this.state.menu_saturday,
    menu_sunday:this.state.menu_sunday
  }
  actionRedux.updateMenuConfig(menu_config).then(function () {
    me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You updated menu configuration"))
    me.setState({
      showBtnSubmit : true
    })
 })
   .catch(function (error) {
    me.setState({
      showBtnSubmit : true
    })
     me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ")+ eval("me.state.name_"+me.state.Language) + ". error: " + error)
   })
  }
  yesConfirmDeleted = () => {
    this.state.ConfirmCustom.display  = false;
    this.state.ConfirmCustom.session += 1;
    // alert(this.state.delete.id)
    actionRedux.delMenuByID(this.state.delete.id, (result) => {
      if (result == true) {
        this.setStateAlertCustom("success", this.stran("Congratulation!"), this.stran("You have deleted: ") + this.state.delete.name)

      } else {
        this.setStateAlertCustom("danger", this.stran("Fail"), this.stran("there are some thing wrong cannot delete: "))
      }
      this.setState({
        delete: {
          id  : "",
          name: ""
        }
      });
    })

  }
  render() {


    var { BigCategory }  = this.state;
    var HotelsPagination = this.state.HotelsPagination;
    var stranl           = this.stran;
    return (

      <Layout>



        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2">

          <div className="card">
            <div className="card-header">
              <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                  <h4 >{stranl("Menu")}</h4>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
               
              <button className="btn btn-success float-right" style={{display:this.state.showBtnSubmit?"block":"none"}} onClick={this.saveChanges}>Save changes</button>
     
                </div>
              </div>

            </div>
            <div className="card-body">
            <div className="row mb-2">
            <div className="col-lg-4">
             <label> 
             
             <FontAwesomeIcon icon="bolt"/> MENU TYPE: </label>
            
              <select className="form-control" name="menu_config_type" onChange={this.addStateEvent}>
                <option value="repeat_default" selected={this.state.menu_config_type=="repeat_default"?"selected":""}>Repeat Default</option>
                <option value ="multiday_in_week" selected={this.state.menu_config_type=="multiday_in_week"?"selected":""}>Multi Day in Week</option>
              </select>

            </div>
            <div className="col-lg-8">
          <code>{stranl("Repeat default: just a menu that can apply everyday. ")}<br/>
          {stranl("Multiday in week: setup each day with unique menu.")}<br/>
          {this.state.menu_config_type=="multiday_in_week"?stranl("[MO: Monday, TU: Tuesday, WED: Wednesday, TH: Thursday, FR: Friday, SA: Saturday, SU, Sunday]"):""}</code>
          </div>
            </div>
            <hr/>
            <div className="row mb-2 mt-5" >
            
            <div className="col-lg-2 ml-4 mt-5 mb-5 d-flex justify-content-center align-items-center" style={styles.createNew} onClick={this.btnAddNew}>
              <FontAwesomeIcon style={styles.plusIcon}icon="plus"/>
            </div>
            {this.showListMenu(this.state.Menu)}
              </div>
              {/* <div className="table-responsive">

                <PaginationCustom todos={BigCategory} onReceive={this.setStateHotelsPagination}>
                  <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>status</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>



                      {HotelsPagination != null ? this.showItemList(HotelsPagination) : ""}




                    </tbody>
                  </table>
                </PaginationCustom>

              </div> */}
            </div>
          </div>
        </div>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session} />
        <ConfirmCustom type={this.state.ConfirmCustom.type} title={this.state.ConfirmCustom.title} content={this.state.ConfirmCustom.content} display={this.state.ConfirmCustom.display} time={this.state.ConfirmCustom.time} session={this.state.ConfirmCustom.session} yes={this.yesConfirmDeleted} />

      </Layout>
    );
  }
}

const styles={
  createNew:{
    width          : "15%",
    height         : "248px",
    backgroundColor: "#023240de",
    borderRadius   : "7px",
    boxShadow      : "3px 5px 2px darkgrey"

  },
  ItemMenu:{
  
    height         : "220px",
    backgroundColor: "white",
    borderRadius   : "7px",
    border:"1px solid darkturquoise",
    boxShadow      : "3px 5px 2px darkgrey"

  },
  plusIcon:{
    fontSize: "36px",
    color   : "ghostwhite"
  },
  comboWeekDay:{
    position: "absolute",
    top: "-29%",
    border: "1px solid",
    width: "105%",
    paddingLeft: "3px",
    borderRadius:" 6px",
    backgroundColor: "black"
  }
}



const mapStateToProps = state => {
  return {
    Lang       : state.Language,
    BigCategory: state.BigCategory
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);