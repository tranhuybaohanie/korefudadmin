import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../Ultils/StranlatetionData';
import ImageManagerment from '../ImageManagerment/Index';
import defaultImg from './../../Assets/img/default-image.png';
import BigCategory from '../../Storage/Reducers/BigCategory';
import './../../Assets/css/Admin/ProductCategory/AddNewForm.css';
import AlertCustom from './../Shared/AlertCustom';

class ProductCategoryAddNewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mouse:{
          x: 0,
          y: 0,
        },
       
      displayDetail: this.props.displayDetail,
      modifiedID   : "this.props.match.params.id",
      Language     : localStorage.getItem("lang"),
      AlertCustom  : {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      showBtnSubmit: true,
      openImageMana: false,
      setClassFixed: false,
      name         : "",
      img_url      : "https://www.goodfood.com.au/content/dam/images/h/0/f/a/q/i/image.related.wideLandscape.940x529.h0fa4n.png/1515456591895.jpg",
      // defaultImg,
      description     : "",
      meta_title      : "",
      meta_description: "",
      meta_keyword    : "",
      status          : true,
      authorName      : "",
      create_date     : "",
      modified_date   : "",
      modified_by_name: "",
      price           : 0,
      promotionPrice  : 0,
      imgMore         : "",
      limitImgMore    : 4,
      countImgMore    : 0,
      ingredient      : "",
      nutritionalValue: "",
      currency        : "",
      BigCategory     : "",
      BigCategoryName : "",
      BigCategoryID   : "",
      animatedDisplay : true,
      detailByID      : "empty",
      imgMoreShow     : ""
    }


  }




  addStateEvent = (e) => {
    const input = e.target;
    console.log(input)
    var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
    setTimeout(() => {
      this.validateInput();
    }, 100)

  }

  addStateNameKey = (name, value) => {
    this.setState({
      [name]: value
    })

  }

  BigCategoryGet = () => {
    return this.state.BigCategory.map((item, id) => {
      // console.log(item.id +"  "+this.state)
      if(this.state.BigCategoryID == item.id) {
        alert(item.name)
        this.setState({
          BigCategoryName: item.name
        })
      }
     
      return (
        
        <option value={item.id} selected={this.state.BigCategoryID == item.id ? "selected" : ""}>{item.name}</option>
      );
    });
  }

  componentWillMount() {
    this.setState({
      Language   : this.props.Lang[0]["lang"],
      BigCategory: this.props.BigCategory,
    })
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();
  }

  componentWillReceiveProps(newprops) {
    if (this.props != newprops) {
      this.setState({
        // displayDetail:newprops.displayDetail,
        Language       : newprops.Lang[0]["lang"],
        BigCategory    : newprops.BigCategory,
        animatedDisplay: newprops.displayDetail,
        detailByID     : newprops.detailByID
      }, () => {
   

      });
      

      setTimeout(()=>{
     
        this.setState({
          displayDetail: newprops.displayDetail,
        })
      },300)
        setTimeout(()=>{
          if(this.state.detailByID!=""){
        actionRedux.getProductByIDLang(this.state.Language,this.state.detailByID,(result)=>{
          // if(ProductCategory==false){
          //   this.props.history.push('/admin/product-category');
          // }})


  if(result!=false){
    this.setState({
      // id             : doc.id,
      BigCategoryName: result.bigCategoryName,
      // bigCategoryName: bigcategoryData.name,
       productCategoryName: result.productCategoryName,
       authorName         : result.authorName,
       modified_date      : result.modified_date,
       create_date        : result.create_date,
       modified_by_name   : result.modified_by_name,
       name               : result.name,
       BigCategoryID      : result.big_category_id,
       description        : result.description,
       img_url            : result.img_url,
       meta_title         : result.meta_title,
       meta_description   : result.meta_description,
       meta_keyword       : result.meta_keyword,
       price              : result.price,
       promotionPrice     : result.promotion_price,
       imgMore            : result.img_more,
       ProductCategoryID  : result.product_category_id,
       ingredient         : result.ingredient,
       nutritionalValue   : result.nutritional_value,
       currency           : result.currency,
       status             : result.status
}
   
        );
      }
    })
      }
      },100)
    }
  }


  getImgMore=()=>{
    var imgMoreArr              = this.state.imgMore.split("{bao&%/break}");
        this.state.countImgMore = 0;
      return imgMoreArr.map((item,idex)=>{
    this.state.countImgMore++;
  return(
    <img onMouseEnter={this.tooltip} onMouseLeave={this.tooltipHide} src={item} data-src={item} width="49%" height="auto" style={{ backgroundColor: 'gray',    width:"15%" }} className="img-responsive img-thumbnail mt-2" alt="Click upload to add image" onClick={this.deleteItemImgMore}/>
  );

});
}
  componentDidUpdate(){
   
  }
  componentDidMount() {

  }


  validateInput = () => {
    var me           = this;
    var result       = true;
    var arr          = Object.values(this.state.validate);
    var validate_msg = "";
    arr.map((item, id) => {
      if (item.required === true && item.length_min > this.state[item.id].length) {
        this.state.validate[item.id].msg = this.stran('cannot be blank!')
        me.setStateAlertCustom("warning", me.stran("recommendation"), me.stran("Please! satify red line behide input box."))
        // this.forceUpdate()
        result = false;
      } else {
        this.state.validate[item.id].msg = ""
      }
    })
    return result;
  }

  submitBtn = (e) => {
    const me                       = this;
          this.state.showBtnSubmit = false;
    e.preventDefault();
    if (this.validateInput()) {
      var cate = {
        name            : this.state.name,
        description     : this.state.description,
        img_url         : this.state.img_url,
        meta_title      : this.state.meta_title,
        meta_description: this.state.meta_description,
        meta_keyword    : this.state.meta_keyword,
        BigCategoryID   : this.state.BigCategoryID,
        status          : this.state.status
      }

      actionRedux.updateProductCategory(this.state.modifiedID, cate).then(function () {
        me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You updated a new product category") + me.state.name)
        me.state.showBtnSubmit = true;
      })
        .catch(function (error) {
          me.state.showBtnSubmit = true;
          me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when processing: ") + me.state.name + ". error: " + error)
        })
    } else {
      this.state.showBtnSubmit = true;
    }

  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "999999999",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  showImageMana = () => {
    this.setState({
      openImageMana: this.state.openImageMana ? false: true
    })
  }
  closeDetail =()=>{
// alert("ok")
this.setState({
  name            : "",
  BigCategoryID   : "",
  description     : "",
  img_url         : "",
  meta_title      : "",
  meta_description: "",
  meta_keyword    : "",
  create_date     : "",
  author          : "",
  status          : "",
})
   this.props.setShow();
   
  }
  tooltip=(e)=>{
 

    this.setState({
      imgMoreShow: e.target.dataset.src
    })
   }
   tooltipHide=()=>{
     
    this.setState({
      imgMoreShow: ""
    })
   }
   MouseMove=(e)=>{
    var me = this;
    window.addEventListener("mousemove", function (evt) {
      me.setState({
        mouse:{
          x: evt.clientX,
          y: evt.clientY
        }
      },()=>{console.log(me.state)})
    })
  }
  render() {

    var stranl = this.stran;
 
    // var Lang = this.props.Hotels;

    return (



      <div ref="imgmana" className={`container-fluid animated ${this.state.animatedDisplay?"fadeIn":"fadeOut"}`} style={{...styles.container,display:this.state.displayDetail?"block":"none"}} onMouseMove={this.MouseMove}>

        <div className={`row animated ${this.state.animatedDisplay?"jackInTheBox":"fadeOut"}`} style={{height:"45%"}}>
        
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-10 mt-2 ">
          <button className="btn btn-danger" style={styles.btnClose} onClick={this.closeDetail}>x</button>
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 "  style={{ backgroundImage:`url("${this.state.img_url}")`,  filter: "blur(3px)",   height: "30%",    backgroundRepeat: "repeat",
    backgroundSize:"100%"}} >
         
                        
         
        </div>
        <img src={this.state.img_url} width="100%" height="auto" style={ styles.img} className="img-responsive mt-2" alt="Image" />

            <div className="card">
              <div className="card-body">
                <h4 className="card-title">{stranl("Detail product category")} 
                <span className="btn-action text-darkyellow" style={{width:"78px",marginLeft:"10px"}}>
          {this.shortNavlink("/admin/modified-product-category/"+this.state.detailByID, "text-darkyellow", "", "pencil-alt")}
          </span> </h4>
          <h6  className={this.state.status?"text-success":"text-danger"} >{this.state.name} <FontAwesomeIcon className={this.state.status ? "ml-2 mt-2 text-success animated bounceIn" : "ml-2 mt-2 text-danger animated tada"} icon="circle" />   {this.state.status?stranl("Active"):stranl("Inactive")}</h6>
                <hr/>
                <div className="row">

                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2" style={{    height: "353px",overflowY: "scroll"}}>
                    {/* <label className="col-sm-12 form-control-label">Material Inputs</label> */}
                    <div className="col-sm-12">
                      <div className="form-group-material">
                        <label className="">{stranl("name")}* ({this.state.name.length}/50) </label>
                        <h6  className="" >{this.state.name} </h6>

                        {/* <label  className=""> Name</label> */}
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("Author")} </label>
                        <h6  className="" >{this.state.authorName} </h6>

                        {/* <label  className=""> Name</label> */}
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("Created date")} </label>
                        <h6  className="" >{this.state.create_date} </h6>

                        {/* <label  className=""> Name</label> */}
                      </div>
                     {this.state.modified_by_name!=null?
                     <Fragment>
                      <div className="form-group-material">
                        <label className="">{stranl("Modified by")} </label>
                        <h6  className="" >{this.state.modified_by_name} </h6>

                      
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("Modified date")} </label>
                        <h6  className="" >{this.state.modified_date} </h6>
   
                      </div>
                      </Fragment>
                      :""}
                      <div className="form-group-material">
                        <label className="">{stranl("price")}* ({this.state.price.length}/50) </label>
                        <h6  className="" >{this.state.price}({this.state.currency}) </h6>

                        {/* <label  className=""> Name</label> */}
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("promotion price")}* ({this.state.name.length}/50) </label>
                        <h6  className="" >{this.state.promotionPrice}({this.state.currency}) </h6>

                        {/* <label  className=""> Name</label> */}
                      </div>

                      <div className="form-group-material">
                        <label className="">{stranl("description")} ({this.state.description.length}/100)</label>
                        <p  className="" >{this.state.description} </p>
                      
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("Ingredient")} ({this.state.ingredient.length}/100)</label>
                        <p  className="" >{this.state.ingredient} </p>
                      
                      </div>
                      <div className="form-group-material">
                        <label className="">{stranl("Nutritional value")} ({this.state.nutritionalValue.length}/100)</label>
                        <p  className="" >{this.state.nutritionalValue} </p>
                      
                      </div>
                      <div className="">
                        <label className="">{stranl("big category")}*</label>
                        <h6  className="" >{this.state.BigCategoryName} </h6>

                      </div>
                      <div className="">
                        <label className="">{stranl("product category")}*</label>
                        <h6  className="" >{this.state.productCategoryName} </h6>

                      </div>
                      <div className="form-group-material mt-4">
                        <label className="">{stranl("status")}*</label>
                        <h6  className={this.state.status?"text-success":"text-danger"} >{this.state.status?stranl("Active"):stranl("Inactive")}</h6>
                        {/* <input type="checkbox" name="status" required="true" checked={this.state.status} className="ml-3" onChange={this.addStateEvent} /> */}
                        <p className="text-warning">{!this.state.status ? stranl("This category will not work until status checked") : ""}</p>
                      </div>



                    </div>

                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 mt-2">
                  <h6 className="card-title">{stranl("Detail Images")}({this.state.countImgMore}/{this.state.limitImgMore}) </h6>
                  {this.getImgMore()}
                  <img src={this.state.imgMoreShow} className={`img-thumbnail img-responsive ${this.state.imgMoreShow.length>0? "animated rollIn":"animated rollOut"}`} width="100%" style={styles.imgMoreShow}/>
                    <h5 className="card-title">{stranl("Basic SEO infomation")}<FontAwesomeIcon className={this.state.meta_title.length > 0 && this.state.meta_description.length && this.state.meta_keyword.length > 0 ? "ml-2 mt-2 text-success animated bounceIn" : "ml-2 mt-2 text-danger animated tada"} icon="circle" /> </h5>

                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        <div className="form-group-material">
                          <label className="">{stranl("focus keyword")}  ({this.state.meta_keyword.length}/50) </label>
                          <h6  className="" >{this.state.meta_keyword} </h6>
                          {/* <input type="text" name="meta_keyword" value={this.state.meta_keyword} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Meta title")}  ({this.state.meta_title.length}/70) </label>
                          <h6  className="" >{this.state.meta_title} </h6>
                          {/* <input type="text" name="meta_title" value={this.state.meta_title} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("description")}  ({this.state.meta_description.length}/156)</label>
                          <h6  className="" >{this.state.meta_description} </h6>
                          {/* <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>

                      </div>
                    </div>

                  </div>

                </div>


              </div>

            </div>






          </div>




        </div>


        <img style={{ display: this.state.showBtnSubmit ? "none" : "block" }} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%" />
        <ImageManagerment openImageMana={this.state.openImageMana} showImageMana={this.showImageMana} addStateNameKey={this.addStateNameKey}></ImageManagerment>
        {/* <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session}></AlertCustom> */}

      </div>


    );



  }

}

const styles ={
  container:{
    position       : "fixed",
    top            : "0px",
    backgroundColor: "#bb500891",
    height         : "100%",
    zIndex         : "8",
    
  },img:{
   
  width          : "auto",
  zIndex         : "10",
  position       : "absolute",
  left           : "43%",
  top            : "1%",
  borderRadius   : "10px",
  backgroundColor: "gray",
  height         : "124px"
  },
  btnClose:{
    position    : "absolute",
    zIndex      : "10",
    right       : "2%",
    top         : "3%",
    boxShadow   : "0px 0px 11px darkred",
    borderRadius: "24%",
    border      : "1px solid brown"
  },
  imgMoreShow:{
    position : "absolute",
    bottom   : "12%",
    right    : "132%",
    maxWidth : "100%",
    maxHeight: "100%"
  }
}

const mapStateToProps = state => {
  return {
    Hotels     : state.Hotel,
    Lang       : state.Language,
    BigCategory: state.BigCategory
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    },
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductCategoryAddNewForm);
