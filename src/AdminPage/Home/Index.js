import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from '../../Ultils/StranlatetionData';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Language: localStorage.getItem("lang"),
            UserStaff: [],
            UserCustomer: [],
            Orders: [],
        }
    }

    componentDidMount() {

        actionRedux.getUserCustomer((result) => {

            this.setState({ UserCustomer: result })

        });
        actionRedux.getUserStaff((result) => {

            this.setState({ UserStaff: result })

        });
        actionRedux.getOrder((result) => {

            this.setState({ Orders: result })

        });

    }
    componentWillReceiveProps(newprops) {

        var previousLang = this.props.Lang[0]["lang"];
        if (this.props != newprops) {
            this.setState({
                Language: newprops.Lang[0]["lang"],

            }, () => {

                if (previousLang != this.state.Language) {
                    actionRedux.getProduct(this.state.Language, (result) => {

                        this.setState({ ProductCategory: result })
                        // this.props.fetchAllProductCategory(result);
                    });
                }
            });

        }

    }

    successOrder = (Orders) => {
        return Orders.filter(x => x.delivered_state == true)
    }
    cancelOrder = (Orders) => {
        return Orders.filter(x => x.cancel_state == true)
    }
    AcceptedOrder = (Orders) => {
        return Orders.filter(x => x.received_state == true && x.finished_state == false)
    }
    DeliveringOrder = (Orders) => {
        return Orders.filter(x => x.finished_state == true && x.delivered_state == false)
    }
    render() {
        var today = new Date();
        var sevenStaff = this.state.UserStaff.filter(x => (30 - Math.abs(Math.round(((today.getTime() - (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getTime()) / 1000 / 86400) - 0.5))) <= 30)

        var sevenCustomer = this.state.UserCustomer.filter(x => (30 - Math.abs(Math.round(((today.getTime() - (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getTime()) / 1000 / 86400) - 0.5))) <= 30)
        return (


            <Layout history={this.props.history}>

                <section className="dashboard-counts section-padding">
                    <div className="container-fluid">
                        <div className="row">
                            {/* Count item widget*/}
                            <div className="col-xl-3 col-md-4 col-6">
                                <div className="wrapper count-title d-flex">
                                    <div className="icon"><i className="icon-user" /></div>
                                    <div className="name"><strong className="text-uppercase">Total Clients</strong><span>All user customers</span>
                                        <div className="count-number">{this.state.UserCustomer.length}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-4 col-6">
                                <div className="wrapper count-title d-flex">
                                    <div className="icon"><i className="icon-user" /></div>
                                    <div className="name"><strong className="text-uppercase">New Clients</strong><span>Last 30 days</span>
                                        <div className="count-number">{sevenCustomer.length}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-4 col-6">
                                <div className="wrapper count-title d-flex">
                                    <div className="icon"><i className="icon-user" /></div>
                                    <div className="name"><strong className="text-uppercase">Total Staff</strong><span>All user staff</span>
                                        <div className="count-number">{this.state.UserStaff.length}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-4 col-6">
                                <div className="wrapper count-title d-flex">
                                    <div className="icon"><i className="icon-user" /></div>
                                    <div className="name"><strong className="text-uppercase">New staff</strong><span>Last 30 days</span>
                                        <div className="count-number">{sevenStaff.length}</div>
                                    </div>
                                </div>
                            </div>
                   
                        {/* Count item widget*/}
                        <div className="col-xl-3 col-md-4 col-6">
                            <div className="wrapper count-title d-flex">
                                <div className="icon"><i className="icon-padnote" /></div>
                                <div className="name"><strong className="text-uppercase">Work Orders</strong><span></span>
                                    <div className="count-number">{this.state.Orders.length}</div>
                                </div>
                            </div>
                        </div>
                        {/* Count item widget*/}
                        <div className="col-xl-3 col-md-4 col-6">
                            <div className="wrapper count-title d-flex">
                                <div className="icon"><i className="icon-check" /></div>
                                <div className="name"><strong className="text-uppercase">Successful order</strong><span></span>
                                    <div className="count-number">{this.successOrder(this.state.Orders).length}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-md-4 col-6">
                            <div className="wrapper count-title d-flex">
                                <div className="icon"><i className="icon-check" /></div>
                                <div className="name"><strong className="text-uppercase text-danger">Cancel order</strong><span></span>
                                    <div className="count-number">{this.cancelOrder(this.state.Orders).length}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-md-4 col-6">
                            <div className="wrapper count-title d-flex">
                                <div className="icon"><i className="icon-check" /></div>
                                <div className="name"><strong className="text-uppercase text-warning">Accepted order</strong><span></span>
                                    <div className="count-number">{this.AcceptedOrder(this.state.Orders).length}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-md-4 col-6">
                            <div className="wrapper count-title d-flex">
                                <div className="icon"><i className="icon-check" /></div>
                                <div className="name"><strong className="text-uppercase text-warning">Delivering order</strong><span></span>
                                    <div className="count-number">{this.DeliveringOrder(this.state.Orders).length}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section >

            </Layout >

        );
    }
}

const mapStateToProps = state => {
    return {
        Lang: state.Language,
        // ProductCategory: state.ProductCategory
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllProductCategory: (result) => {

            dispatch(actionRedux.actFetchProductCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);