import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from '../../Ultils/StranlatetionData';
import { HorizontalBar,Line } from 'react-chartjs-2';
class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Language: localStorage.getItem("lang"),
            UserStaff: [],
            UserCustomer: [],
            Orders: [],
        }
    }

    componentDidMount() {

        actionRedux.getUserCustomer((result) => {

            this.setState({ UserCustomer: result })

        });
        actionRedux.getUserStaff((result) => {

            this.setState({ UserStaff: result })

        });
        actionRedux.getOrder((result) => {

            this.setState({ Orders: result })

        });

    }
    componentWillReceiveProps(newprops) {

        var previousLang = this.props.Lang[0]["lang"];
        if (this.props != newprops) {
            this.setState({
                Language: newprops.Lang[0]["lang"],

            }, () => {

                if (previousLang != this.state.Language) {
                    actionRedux.getProduct(this.state.Language, (result) => {

                        this.setState({ ProductCategory: result })
                        // this.props.fetchAllProductCategory(result);
                    });
                }
            });

        }

    }

    month = (Orders,month) => {
        var today = new Date();
        var Year = today.getFullYear();
    
 

        return Orders.filter(x =>  (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getMonth()==month&& (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getFullYear()==Year)
   
    }
    totalMonth = (Orders,month) => {
     
    var thisOrder =this.month(Orders,month);
    var total =0;
   if(thisOrder.length>0){
    thisOrder.map((item,id)=>{
       if( typeof item.totalMoneyAcceptance != "undefined")
        total=total+  item.totalMoneyAcceptance
    })}
    return total

}
    day = (Orders,day) => {
        var today = new Date();
        var Year = today.getFullYear();
        var month = today.getMonth()+1;
        
        return Orders.filter(x =>  x.delivered_state==true&& (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getMonth()==month&& (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getFullYear()==Year&& (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getDate()==day)
   
    }
    totalDay = (Orders,day) => {
     
    var thisOrder =this.day(Orders,day);
    var total =0;
   if(thisOrder.length>0){
    thisOrder.map((item,id)=>{
       if( typeof item.totalMoneyAcceptance != "undefined")
        total=+  item.totalMoneyAcceptance
    })}
    return total
    }
    cancelOrder = (Orders) => {
        return Orders.filter(x => x.cancel_state == true)
    }
    AcceptedOrder = (Orders) => {
        return Orders.filter(x => x.received_state == true && x.finished_state == false)
    }
    DeliveringOrder = (Orders) => {
        return Orders.filter(x => x.finished_state == true && x.delivered_state == false)
    }
    render() {
        var today = new Date();
        var Year = today.getFullYear();
        var totalMonth=this.totalMonth
        var totalDay=this.totalDay
        var {Orders}= this.state;
       var monthLabel=['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','Decenber']
        // var sevenStaff = this.state.UserStaff.filter(x => (30 - Math.abs(Math.round(((today.getTime() - (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getTime()) / 1000 / 86400) - 0.5))) <= 30)

        // var sevenCustomer = this.state.UserCustomer.filter(x => (30 - Math.abs(Math.round(((today.getTime() - (new Date(x.create_date.split(",")[1].split("/")[2], x.create_date.split(",")[1].split("/")[1], x.create_date.split(",")[1].split("/")[0], x.create_date.split(",")[0].split(":")[0], x.create_date.split(",")[0].split(":")[1], x.create_date.split(",")[0].split(":")[2])).getTime()) / 1000 / 86400) - 0.5))) <= 30)
        var data = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','Decenber'],
            datasets: [
                {
                    label: 'Total money of successful orders(VND).',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: [totalMonth(Orders,1),totalMonth(Orders,2),totalMonth(Orders,3),totalMonth(Orders,4),totalMonth(Orders,5),totalMonth(Orders,6),totalMonth(Orders,7),totalMonth(Orders,8),totalMonth(Orders,9),totalMonth(Orders,10),totalMonth(Orders,11),totalMonth(Orders,12)]
                }
            ]
        };
      
        return (


            <Layout history={this.props.history}>
                <div>
                    <h2 className="text-center">Orders In {today.getFullYear()}</h2>
                    <HorizontalBar data={data} />
                </div>
             
            </Layout >

        );
    }
}

const mapStateToProps = state => {
    return {
        Lang: state.Language,
        // ProductCategory: state.ProductCategory
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllProductCategory: (result) => {

            dispatch(actionRedux.actFetchProductCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);