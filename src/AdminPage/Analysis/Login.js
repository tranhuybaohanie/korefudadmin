import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Layout from '../Shared/Layout';
import * as actionRedux from '../../Storage/Actions/Index';
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


class Home extends Component {

	constructor(props) {
		super(props);

			
		this.state={
			username  : "",
			password  : "",
			rememberme: "",
			msg       : "",
			loading   : false,
			validate  : {
				username:{
					id        : "username",
					length_max: 100,
					require   : true,
					msg       : ""
				},
				password:{
					id        : "password",
					length_max: 100,
					require   : true,
					msg       : ""
				},
				rememberme:{
					id        : "rememberme",
					length_max: 100,
					require   : false,
					msg       : ""
				},
			}
		}

	}


	addStateEvent = (e) => {
		const input = e.target;
		console.log(input)
		var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
		if (length_max == "infinity" || length_max >= input.value.length) {
		  this.setState({
			[input.name]: input.type === 'checkbox' ? input.checked: input.value
		  })
		}
		
	
	  }
	  btnLogin=(e)=>{
		  this.setState({
			  loading: true
		  })
		e.preventDefault();
		actionRedux.checkLogin(this.state.username,this.state.password,(data)=>{
			
			 if(data.status==false){
				this.setState({msg:"this account is blocked!",loading:false})
			}else if(data){
			
				// localStorage.setItem("LoginID",data);
				// var today = new Date();
				// if(this.state.rememberme){
					
				// 	localStorage.setItem("expired",today.addDays(7));
				// }
				// else{
				// 	localStorage.removeItem("expired");
					
				// }
				// if(this.props.history.location.pathname!="/admin/login")
				// this.props.history.goBack();
				// else
				// var backurl = this.props.match.url.replace("/admin/login/","").replaceAll("__","/");
				// console.log(backurl)
				// if(backurl!="/admin/login"){
				// this.props.history.push(backurl); 
				// }else{
					this.props.history.push("/admin/"); 
				// }
			} else if(data==false){
				this.setState({msg:"Please check your usename password!",loading:false})
			}
		})
		// console.log(this.state)
	  }
	componentWillMount() {
		// this.props.history.push('/#/admin');
	}
	render() {

		return (


			<div className="container-fluid" style={{ height: "100%" }}>
				<div className="row" style={{ height: "100%" }}>

			
					<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center" style={{ backgroundImage: `url(${require("./../../Assets/img/login-background.jpg")})`, backgroundRepeat: "none", backgroundSize: "100%" }}>

						<div className=" d-flex justify-content-center align-items-center" style={styles.form_login} >
							
							<form className="text-white">
							<h3 className="text-center text-white">Login</h3>
							<p className="text-danger">{this.state.msg.length>0?this.state.msg:""}</p>
								<div className="form-group">
									<label >UserName </label>
									<input type="email" className="form-control"  name="username" placeholder="Enter email" onChange={this.addStateEvent} />
										<small id="emailHelp" className="form-text text-muted">We'll never share your info with anyone else.</small>
								</div>
								<div className="form-group">
									<label >Password</label>
									<input type="password" name="password" className="form-control" id="exampleInputPassword1" placeholder="Password"  onChange={this.addStateEvent} />
								</div>
								<div className="form-check">
									<input type="checkbox" className="form-check-input" name="rememberme"   onChange={this.addStateEvent}  />
												<label className="form-check-label" >Save next.</label>
								</div>
								<button type="submit"  className="btn btn-primary" style={styles.btnlogin}  onClick={this.btnLogin} >Get me in</button>
								<img src={require("./../../Assets/img/l.gif")} style={{...styles.loading,display:this.state.loading?"block":"none"} }></img>
							</form>
						</div>



					</div>

				</div>

			</div>

		);

	}

}
const styles = {
	loading:{
		position    : "absolute",
		mixBlendMode: "multiply",
		top         : "0px",
		right       : "38%",
	},
	form_login: {
		height         : "72%",
		width          : "50%",
		backgroundColor: "rgba(28, 59, 77, 0.96)"
	},
	btnlogin:{
		width: "100%"
	}
}
export default Home;
