import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../Ultils/StranlatetionData';
import ImageManagerment from '../ImageManagerment/Index';
import defaultImg from './../../Assets/img/default-image.png';
import BigCategory from '../../Storage/Reducers/BigCategory';
import './../../Assets/css/Admin/ProductCategory/AddNewForm.css';
import AlertCustom from './../Shared/AlertCustom';

class ProductCategoryAddNewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mouse:{
          x: 0,
          y: 0,
        },
       
      displayDetail: this.props.displayDetail,
      Language     : localStorage.getItem("lang"),
      AlertCustom  : {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      showBtnSubmit: true,
      openImageMana: false,
      setClassFixed: false,

      status          : true,
      
      create_date     : "",
  
      price           : 0,

      address:"",
      phone:"",
      usermail:"",
      totalItem:0,
      totalMoney:0,
      Cart:[],
      CartKey:[],
        CartComponentPrice:[],
        CartComponent:[],
        received_state:false,
        status:false,
       create_date:"",
       cancel_state:false,
       content_state:false,
       delivered_state:false,
       finished_state:false,
      animatedDisplay : true,
      detailByID      : "empty",
      Acceptance:[],
      sectionSlider:0,
      reject_content:"",
      validate:{
        reject_content: {
        id        : "reject_content",
        length_min: 0,
        length_max: 100,
        required  : false,
        msg       : ""
      },}
    
    }


  }




  addStateEvent = (e) => {
    const input = e.target;
    console.log(input)
    var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
    setTimeout(() => {
      this.validateInput();
    }, 100)

  }

  addStateNameKey = (name, value) => {
    this.setState({
      [name]: value
    })

  }

  componentWillMount() {
    this.setState({
      Language   : this.props.Lang[0]["lang"],
  
    })
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();
  }
  componentDidMount(){
   
  }
  componentWillReceiveProps(newprops) {
    if(newprops.detailByID){
     
      actionRedux.getOrderById(newprops.detailByID,result=>{
        var {Acceptance,  Cart,  CartComponentPrice,  address, phone,fullname,received_state,status,totalItem,totalMoney,usermail,create_date,cancel_state,content_state,delivered_state,finished_state}=result;
  
           
        Cart.map((item,id)=>{
         
          actionRedux.getProductByIDLang(this.state.Language,item.id,(result)=>{
          
            if(result!=false){
             
              
              if(id==0){
                  this.setState({
                    CartComponent:[],
                  },()=>{
                    var CartComponent= this.state.CartComponent;
                      var checkDup=false;
                      CartComponent.map((itemcart,id)=>{checkDup= itemcart.id==result.id?true:false})
                      if(!checkDup){
                  CartComponent.push(result);
                    this.setState({
                      CartComponent
                });
              }})
                
              }else{
                var CartComponent= this.state.CartComponent;
                var checkDup=false;
                CartComponent.map((itemcart,id)=>{checkDup= itemcart.id==result.id?true:false})
                if(!checkDup){
            CartComponent.push(result);
              this.setState({
                CartComponent
          });
        }
              }
           
                }
              })
        })
    
        this.setState({
          Acceptance,
          Cart,  CartComponentPrice,  address, phone,fullname,received_state,status,totalItem,totalMoney,usermail,create_date,cancel_state,content_state,delivered_state,finished_state
        })
      })
    
    }
    if (this.props != newprops) {
    
      this.setState({
        // displayDetail:newprops.displayDetail,
        Language       : newprops.Lang[0]["lang"],
        animatedDisplay: newprops.displayDetail,
        detailByID     : newprops.detailByID
      }, () => {
   

      });
      

      setTimeout(()=>{
     
        this.setState({
          displayDetail: newprops.displayDetail,
        })
      },300)
    

      }
 
    }
  




  validateInput = () => {
    var me           = this;
    var result       = true;
    var arr          = Object.values(this.state.validate);
    var validate_msg = "";
    arr.map((item, id) => {
      if (item.required === true && item.length_min > this.state[item.id].length) {
        this.state.validate[item.id].msg = this.stran('cannot be blank!')
        me.setStateAlertCustom("warning", me.stran("recommendation"), me.stran("Please! satify red line behide input box."))
        // this.forceUpdate()
        result = false;
      } else {
        this.state.validate[item.id].msg = ""
      }
    })
    return result;
  }
  btnReject=()=>{
    var me           = this;
    actionRedux.RejectOrder(this.state.usermail,this.state.Cart,this.state.CartComponent,this.state.CartComponentPrice,this.state.detailByID,this.state.reject_content,(result)=>{
      if(result){
        me.setStateAlertCustom("success", me.stran("Congratulation"), me.stran("Reject successfuly order: ") +this.state.detailByID)
       
      }else{
        me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("Cannot Reject: ") +this.state.detailByID)
       
      }
       
      })
  }
  btnSave=()=>{
    var me           = this;
    actionRedux.AcceptanceOrder(this.state.usermail,this.state.Cart,this.state.CartComponent,this.state.CartComponentPrice,this.state.detailByID,this.state.Acceptance,this.state.totalItem,this.state.totalMoney,(result)=>{
    if(result){
    
      me.setStateAlertCustom("success", me.stran("Congratulation"), me.stran("Confirm successfuly order: ") +this.state.detailByID)
     
    }else{
      me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("Cannot confirm: ") +this.state.detailByID)
     
    }
     
    })
  }
  showOrderList=(Cart,CartComponent,CartComponentPrice)=>{

    if(Cart&&CartComponent.length>0&&this.state.Acceptance.length>0){
    return CartComponent.map((item,idd)=>{
      var { id, name,productCategoryName,authorName,currency, price,promotion_price, description, status, bigCategoryName, author, create_date } = item;
      const Acceptance=this.state.Acceptance;
      const quantity=Cart[Cart.findIndex(x => x.id === id)]?Cart[Cart.findIndex(x => x.id === id)]["quantity"]:0;
      const quantityAceptace=Acceptance[Acceptance.findIndex(x => x.id === id)]?Acceptance[Acceptance.findIndex(x => x.id === id)]["quantity"]:0;
     const unitPrice= CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]?CartComponentPrice[CartComponentPrice.findIndex(x => x.id === id)]["price"]:0
      if(idd<=Cart.length){
        return (<tr key={id}>
          <th scope="row">{idd}</th>
          <td>{name}</td>
          <td>{unitPrice}</td>
          <td>{quantity}</td>
          <td>{unitPrice*quantityAceptace}</td>
          <td><input type="number" min="0" max={quantity} onChange={(e)=>{
            if(e.target.value<=quantity&&e.target.value>=0){

                Acceptance[Acceptance.findIndex(x => x.id === id)]["quantity"]=e.target.value
                var totalItem=0;
                var totalMoney=0;

                Acceptance.map((iac,idac)=>{
                  var iacQuantity=parseInt(iac.quantity)
                  totalItem+=iacQuantity;
               totalMoney+=iacQuantity*parseInt(CartComponentPrice[CartComponentPrice.findIndex(x=>x.id==iac.id)]["price"] );})
                this.setState({Acceptance:Acceptance,
                totalItem,totalMoney
                },()=>{
            })
            }
           
            }} value={quantityAceptace}/></td>
          </tr>
          )
        }
    })}
  }
  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "999999999",
        session: this.state.AlertCustom.session + 1
      },
    })
  }

  
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  showImageMana = () => {
    this.setState({
      openImageMana: this.state.openImageMana ? false: true
    })
  }
  closeDetail =()=>{
// alert("ok")
this.setState({
  Cart:[],
  CartComponentPrice:[],
  CartComponent:[],
})
   this.props.setShow();
   
  }
  tooltip=(e)=>{
 

    this.setState({
      imgMoreShow: e.target.dataset.src
    })
   }
   tooltipHide=()=>{
     
    this.setState({
      imgMoreShow: ""
    })
   }

  render() {

    var stranl = this.stran;
 
    // var Lang = this.props.Hotels;

    return (



      <div ref="imgmana" className={`container-fluid animated ${this.state.animatedDisplay?"fadeIn":"fadeOut"}`} style={{...styles.container,display:this.state.displayDetail?"block":"none"}}>

        <div className={`row animated ${this.state.animatedDisplay?"jackInTheBox":"fadeOut"}`} style={{height:"45%"}}>
        
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-10 mt-2 ">
          <button className="btn btn-danger" style={styles.btnClose} onClick={this.closeDetail}>x</button>
   
      
            <div className="card">
              <div className="card-body" style={{height: "650px"}}>
               <div className="d-flex flex-row">
                <h4 className="card-title">{stranl("Detail Order")} 
                <span className="btn-action text-darkyellow" style={{width:"78px",marginLeft:"10px"}}>
           {this.state.totalItem>0?  <button className="btn btn-success" onClick={this.btnSave}>{stranl("Save")}</button> :null}
             </span>
             </h4>
             <h4 className="card-title">
                <span className="btn-action text-darkyellow" style={{width:"78px",marginLeft:"10px"}}>
             <button className="btn btn-warning" onClick={this.btnReject}>{stranl("Reject")}</button> 
             </span>
             </h4>
             <div>
               <textarea name="reject_content" value={this.state.reject_content} placeholder={stranl("Reject content")} onChange={this.addStateEvent}/>
             </div>
             </div>
          <h6  className={this.state.status?"text-success":"text-danger"} >{this.state.detailByID} <FontAwesomeIcon className={this.state.status ? "ml-2 mt-2 text-success animated bounceIn" : "ml-2 mt-2 text-danger animated tada"} icon="circle" />   {this.state.status?stranl("Active"):stranl("Inactive")}</h6>
                <hr/>
                <div className="row">

                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2" style={{    height: "353px",overflowY: "scroll"}}>
                    {/* <label className="col-sm-12 form-control-label">Material Inputs</label> */}
                    <div className="col-sm-12">
                    <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>

                        <th>{stranl("Name")}</th>
                        <th>{stranl("Price")}</th>
                        <th>{stranl("Quantity")}</th>
                        <th >{stranl("Total")}</th>
                        <th >{stranl("Acceptance Quantity")}</th>
                       

                      </tr>
                    </thead>
                    <tbody>
                     {this.showOrderList(this.state.Cart,this.state.CartComponent,this.state.CartComponentPrice)}
                  </tbody>
                  </table>
                    </div>

                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 mt-2">
                
                 
                    <h5 className="card-title">{stranl("Ship infomation")}
                    <FontAwesomeIcon className={"ml-2 mt-2 text-success animated tada"} icon="circle" /> </h5>

                    <div className="row"  style={{height: "84%",overflowY:"scroll"}}>

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        <div className="form-group-material">
                          <label className="">{stranl("Full name")}  </label>
                          <h6  className="" >{this.state.fullname} </h6>
                          {/* <input type="text" name="meta_keyword" value={this.state.meta_keyword} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Phone")} </label>
                          <h6  className="" >{this.state.phone} </h6>
                          {/* <input type="text" name="meta_title" value={this.state.meta_title} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Address")}</label>
                          <h6  className="" >{this.state.address} </h6>
                          {/* <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Email")} </label>
                          <h6  className="" >{this.state.usermail} </h6>
                          {/* <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Total Item")}</label>
                          <h6  className="" >{this.state.totalItem} </h6>
                          {/* <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Total money")} </label>
                          <h6  className="" >{this.state.totalMoney} </h6>
                          {/* <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} /> */}
                          {/* <label  className=""> Name</label> */}
                        </div>

                      </div>
                    </div>

                  </div>

                </div>


              </div>

            </div>






          </div>




        </div>


        <img style={{ display: this.state.showBtnSubmit ? "none" : "block" }} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%" />
        <ImageManagerment openImageMana={this.state.openImageMana} showImageMana={this.showImageMana} addStateNameKey={this.addStateNameKey}></ImageManagerment>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session}></AlertCustom>

      </div>


    );



  }

}

const styles ={
  container:{
    position       : "fixed",
    top            : "0px",
    backgroundColor: "#bb500891",
    height         : "100%",
    zIndex         : "8",
    
  },img:{
   
  width          : "auto",
  zIndex         : "10",
  position       : "absolute",
  left           : "43%",
  top            : "1%",
  borderRadius   : "10px",
  backgroundColor: "gray",
  height         : "124px"
  },
  btnClose:{
    position    : "absolute",
    zIndex      : "10",
    right       : "2%",
    top         : "3%",
    boxShadow   : "0px 0px 11px darkred",
    borderRadius: "24%",
    border      : "1px solid brown"
  },
  imgMoreShow:{
    position : "absolute",
    bottom   : "12%",
    right    : "132%",
    maxWidth : "100%",
    maxHeight: "100%"
  }
}

const mapStateToProps = state => {
  return {
    Hotels     : state.Hotel,
    Lang       : state.Language,
    BigCategory: state.BigCategory
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    },
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductCategoryAddNewForm);
