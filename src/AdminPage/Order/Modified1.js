import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../Ultils/StranlatetionData';
import ImageManagerment from '../ImageManagerment/Index';
import defaultImg from './../../Assets/img/default-image.png';
import BigCategory from '../../Storage/Reducers/BigCategory';
import './../../Assets/css/Admin/ProductCategory/AddNewForm.css';
import AlertCustom from './../Shared/AlertCustom';
class ProductCategoryAddNewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ModifiedID : this.props.match.params.id,
      Language   : localStorage.getItem("lang"),
      AlertCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      showBtnSubmit    : true,
      openImageMana    : false,
      setClassFixed    : false,
      name             : "",
      img_url          : defaultImg,
      description      : "",
      meta_title       : "",
      meta_description : "",
      meta_keyword     : "",
      status           : true,
      BigCategory      : [],
      BigCategoryID    : "",
      ProductCategory  : [],
      ProductCategoryID: "",
      price            : 0,
      currency         : "VND",
      promotionPrice   : 0,
      ingredient       : "",
      nutritionalValue : "",
      imgMore          : "",
      validate         : {
                         name: {
                            id        : "name",
                            length_min: 1,
                            length_max: 50,
                            required  : true,
                            msg       : ""
                          },
                          ingredient: {
                            id        : "ingredient",
                            length_min: 0,
                            length_max: 200,
                            required  : false,
                            msg       : ""
                          },
                          imgMore: {
                            id        : "imgMore",
                            length_min: 0,
                            count     : 0,
                            limit_img : 4,
                            length_max: 10000,
                            required  : false,
                            msg       : ""
                          },
                          currency: {
                            id        : "currency",
                            length_min: 0,
                            length_max: 200,
                            required  : false,
                            msg       : ""
                          },
                          nutritionalValue: {
                            id        : "nutritionalValue",
                            length_min: 0,
                            length_max: 200,
                            required  : false,
                            msg       : ""
                          },
                          price: {
                            id        : "price",
                            length_min: 1,
                            length_max: 50,
                            required  : true,
                            msg       : ""
                          },
                          promotionPrice: {
                            id        : "promotionPrice",
                            length_min: 0,
                            length_max: 50,
                            required  : false,
                            msg       : ""
                          },
       BigCategoryID: {
          id        : "BigCategoryID",
          length_min: 1,
          length_max: 100,
          required  : true,
          msg       : ""
        },
        ProductCategoryID: {
          id        : "ProductCategoryID",
          length_min: 1,
          length_max: 100,
          required  : true,
          msg       : ""
        },
        description: {
          id        : "description",
          length_min: 0,
          length_max: 100,
          msg       : ""
        },
        status: {
          id        : "status",
          length_min: 1,
          length_max: 50,
          required  : true,
          msg       : ""
        },
        meta_title: {
          id        : "meta_title",
          length_min: 0,
          length_max: 70,
          msg       : ""

        },
        meta_description: {
          id        : "meta_description",
          length_min: 0,
          length_max: 156,
          msg       : ""
        },
        meta_keyword: {
          id        : "meta_keyword",
          length_min: 0,
          length_max: 50,
          msg       : ""
        }

      }
    }

  }




  addStateEvent = (e) => {
    
    const input = e.target;
  if(input.name=="BigCategoryID"){
    this.OnSelectBigcategory(input.value)
  }
    var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
    setTimeout(() => {
      this.validateInput();
    }, 100)

  }

  addStateNameKey = (name, value) => {
    this.setState({
      [name]: value
    })

  }
  addsetImg = (name, value) => {

    var imgUrlString = "";
    var breakKey     = this.state.validate.imgMore.count>0?"{bao&%/break}":"";
    if(this.state.imgSetFor=="imgMore"){
      imgUrlString = this.state.imgMore+breakKey+value;
      this.state.validate.imgMore.count++;
    }else{
      imgUrlString = value;
    }

    this.setState({
      
      [this.state.imgSetFor]: imgUrlString
    })
  }

  BigCategoryGet = () => {
   
    return this.state.BigCategory.map((item, id) => {
      return (
        <option value={item.id} selected={this.state.BigCategoryID==item.id?"selected":""}  >{item.name}</option>
      );
    });
  }
  OnSelectBigcategory =(BigCategoryID)=>{
    actionRedux.getProductCategoryByBigCategoryID(BigCategoryID,(result) => {
      this.setState({ProductCategory:result})
    });
  }
  ProductCategoryGet = () => {
   
    return this.state.ProductCategory.map((item, id) => {
      return (
        <option value={item.id} selected={this.state.ProductCategoryID==item.id?"selected":""}>{item.name}</option>
      );
    });
  }

  componentWillMount() {
    this.setState({
      Language   : this.props.Lang[0]["lang"],
      BigCategory: this.props.BigCategory,
    })
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();
  }

  componentWillReceiveProps(newprops) {
    if (this.props != newprops) {
      this.setState({
        Language   : newprops.Lang[0]["lang"],
        BigCategory: newprops.BigCategory
      }, () => {
      });
    }
  }

  componentDidMount() {
    actionRedux.getBigCategory((result) => {
      this.props.fetchAllBigCategory(result);
    });
    actionRedux.getProductByID(this.state.ModifiedID,(result) => {
      if(result==false){
        this.props.history.push('/admin/product-category');
      }

      this.setState({
      // id             : doc.id,
      // bigCategoryName: bigcategoryData.name,
      // productCategoryName: productCategoryData.name,
      // authorName: userData.name,
      name             : result.name,
      BigCategoryID    : result.big_category_id,
      description      : result.description,
      img_url          : result.img_url,
      meta_title       : result.meta_title,
      meta_description : result.meta_description,
      meta_keyword     : result.meta_keyword,
      price            : result.price,
      promotionPrice   : result.promotion_price,
      imgMore          : result.img_more,
      ProductCategoryID: result.product_category_id,
      ingredient       : result.ingredient,
      nutritionalValue : result.nutritional_value,
      currency         : result.currency,
      status           : result.status
},()=>{ this.OnSelectBigcategory(this.state.BigCategoryID)});
    });

    setInterval(() => {
      //     //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
      var clientWidth = this.refs.setOfSubmitBtn != null ? this.refs.setOfSubmitBtn.clientWidth : 0;
      if (window.pageYOffset > clientWidth) {
        this.setState({
          setClassFixed: true
        })
      }
      else {
        this.setState({
          setClassFixed: false
        })
      }
    }, 10);
  }

  deleteItemImgMore=(e)=>{
  
    var finalImgmore = "";
    var breakKey     = "{bao&%/break}";
    var imgMoreArr   = this.state.imgMore.split("{bao&%/break}");
    var count        = 0;
     imgMoreArr.map((item,idex)=>{
      if(e.target.dataset.src!=item){
      var breakKeyCustom=count>0?breakKey:"";
      count++;
      finalImgmore = finalImgmore+breakKeyCustom+item;
      }
     })
     console.log(finalImgmore)
    this.setState({
      imgMore: finalImgmore
    })
    this.state.validate.imgMore.count = count;
  }
  getImgMore=()=>{
    var imgMoreArr = this.state.imgMore.split("{bao&%/break}");
      return imgMoreArr.map((item,idex)=>{

  return(
    <img src={item} data-src={item} width="49%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive img-thumbnail mt-2" alt="Click upload to add image" onClick={this.deleteItemImgMore}/>
  );

});
}


  validateInput = () => {
   var me           = this;
   var result       = true;
   var arr          = Object.values(this.state.validate);
   var validate_msg = "";
    arr.map((item, id) => {
      if (item.required === true && item.length_min > this.state[item.id].length) {
        this.state.validate[item.id].msg = this.stran('cannot be blank!')
        me.setStateAlertCustom("warning", me.stran("recommendation"), me.stran("Please! satify red line behide input box."))
        // this.forceUpdate()
        result = false;
      } else {
        this.state.validate[item.id].msg = ""
      }
    })
    return result;
  }

  submitBtn = (e) => {
    const me                       = this;
          this.state.showBtnSubmit = false;
    e.preventDefault();
    if (this.validateInput()) {
      var cate = {
        name             : this.state.name,
        price            : this.state.price,
        promotion_price  : this.state.promotionPrice,
        description      : this.state.description,
        img_url          : this.state.img_url,
        img_more         : this.state.imgMore,
        meta_title       : this.state.meta_title,
        meta_description : this.state.meta_description,
        meta_keyword     : this.state.meta_keyword,
        BigCategoryID    : this.state.BigCategoryID,
        ProductCategoryID: this.state.ProductCategoryID,
        ingredient       : this.state.ingredient,
        nutritional_value: this.state.nutritionalValue,
        currency         : this.state.currency,
        status           : this.state.status

      }
      // console.log(cate)
      actionRedux.updateProduct(this.state.ModifiedID,cate)
      .then(function () {
         me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You updated a product ") + me.state.name)
         me.state.showBtnSubmit = true;
      })
        .catch(function (error) {
          me.state.showBtnSubmit = true;
          me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ")+ me.state.name + ". error: " + error)
        })
    }else{
      this.state.showBtnSubmit = true;
    }
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "999999999",
        session: this.state.AlertCustom.session+1
      },
    })
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  showImageMana = (e) => {
    this.setState({
      imgSetFor    : e.target.name,
      openImageMana: this.state.openImageMana ? false: true
    })
  }


  render() {


    var stranl = this.stran;

    // var Lang = this.props.Hotels;

    return (
      <Layout history={this.props.history}>

        <form>
          <div className="container ">

            <div className="row">

              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-10 mt-2 animated rollIn">


                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">{stranl("add a new awesome product")}</h4>
                    {/* ingredient dish */}
                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        {/* <label className="col-sm-12 form-control-label">Material Inputs</label> */}
                        <div className="col-sm-12">
                          <div className="form-group-material">
                            <label className="">{stranl("name")}* ({this.state.name.length}/50) </label>
                            <input type="text" name="name" required="" value={this.state.name} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.name.msg.length > 0 ? this.state.validate.name.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("price")}* ({this.state.price.length}/50) </label>
                            <input type="number" name="price" required="" value={this.state.price} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.price.msg.length > 0 ? this.state.validate.price.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("promotion price")}({this.state.promotionPrice.length}/50) </label>
                            <input type="number" name="promotionPrice" required="" value={this.state.promotionPrice} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.promotionPrice.msg.length > 0 ? this.state.validate.promotionPrice.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="">
                            <label className="">{stranl("Currency")}*</label>
                            <select name="Currency" className="form-control" onChange={this.addStateEvent} >
                              <option selected value="VND" >VNĐ</option>
                              
                            </select>
                            
                          </div>
                          <div className="form-group-material mt-2">
                            <label className="">{stranl("description")} ({this.state.description.length}/100)</label>
                            <textarea rows="5" type="text" name="description" required="" value={this.state.description} className="form-control" onChange={this.addStateEvent} />

                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Ingredient")} ({this.state.ingredient.length}/200)</label>
                            <textarea rows="5" type="text" name="ingredient" required="" value={this.state.ingredient} className="form-control" onChange={this.addStateEvent} />

                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Nutritional value")} ({this.state.nutritionalValue.length}/200)</label>
                            <textarea  rows="5" name="nutritionalValue" required="" value={this.state.nutritionalValue} className="form-control" onChange={this.addStateEvent} />

                          </div>
                          <div className="">
                            <label className="">{stranl("big category")}*</label>
                            <select name="BigCategoryID" className="form-control" onChange={this.addStateEvent} >
                              <option selected disabled hidden>Choose here</option>
                              {this.BigCategoryGet()}
                            </select>
                            <p className="text-danger">{this.state.validate.BigCategoryID.msg.length > 0 ? this.state.validate.BigCategoryID.msg : ""}</p>
                          </div>
                          <div className="">
                            <label className="">{stranl("product category")}*</label>
                            <select name="ProductCategoryID" className="form-control" onChange={this.addStateEvent} >
                              <option selected disabled hidden>Choose here</option>
                              {this.ProductCategoryGet()}
                            </select>
                            <p className="text-danger">{this.state.validate.ProductCategoryID.msg.length > 0 ? this.state.validate.ProductCategoryID.msg : ""}</p>
                          </div>
                          <div className="form-group-material mt-4">
                            <label className="">{stranl("status")}*</label>
                            <input type="checkbox" name="status" required="true" checked={this.state.status} className="ml-3" onChange={this.addStateEvent} />
                            <p className="text-warning">{!this.state.status? stranl("This category will not work until status checked") : ""}</p>
                          </div>



                        </div>

                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 mt-2">
                        <h6>{stranl("Profile Image")}</h6>
                        <input name="img_url" type="button" className="btn btn-default" value={stranl("Upload")}  onClick={this.showImageMana} />
                        <div className="decorate-img animated rotateInDownRight"></div>
                        <img src={this.state.img_url} width="100%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive mt-2" alt="Image" />
                        <hr/>
                        <h6>{stranl("Detail Images")}({this.state.validate.imgMore.count}/{this.state.validate.imgMore.limit_img})</h6>
                        <p>{stranl("Note: click image to remove it.")}</p>
                       {this.state.validate.imgMore.count<this.state.validate.imgMore.limit_img? <input  name="imgMore"  type="button" className="btn btn-default" value={stranl("Upload")} onClick={this.showImageMana} />:""}
                        {/* <div className="decorate-img animated rotateInDownRight"></div> */}
                        <br/>

                         
                         {this.getImgMore()}
                      
                      </div>

                    </div>


                  </div>

                </div>


                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{stranl("Basic SEO infomation")}<FontAwesomeIcon className={this.state.meta_title.length > 0 && this.state.meta_description.length && this.state.meta_keyword.length > 0 ? "ml-2 mt-2 text-success animated bounceIn" : "ml-2 mt-2 text-danger animated tada"} icon="circle" /> </h5>

                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        <div className="form-group-material">
                          <label className="">{stranl("focus keyword")}  ({this.state.meta_keyword.length}/50) </label>
                          <input type="text" name="meta_keyword" required="" value={this.state.meta_keyword} className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Meta title")}  ({this.state.meta_title.length}/70) </label>
                          <input type="text" name="meta_title" required="" value={this.state.meta_title} className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("description")}  ({this.state.meta_description.length}/156)</label>
                          <input type="text" name="meta_description" value={this.state.meta_description} required="" className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>

                      </div>
                    </div>
                  </div>
                </div>



              </div>


              <div className=" col-xs-12 col-sm-12 col-md-12 col-lg-2">
                <div className="line mt-2" />
                <div className={` container set-submit   text-center ${this.state.setClassFixed ? "fixed-set-submit" : "mt-2"}`} >

                  <button style={{display:this.state.showBtnSubmit?"block":"none"}} type="submit" className="btn btn-success" ref="setOfSubmitBtn" onClick={this.submitBtn}>Submit </button>
                  {this.props.history != null ? <div className="mt-2"><button className="btn btn-default" onClick={(e)=> {e.preventDefault();this.props.history.goBack();}}>Go Back</button></div> : ""}
                </div>
                <div className="line mt-2" />
              </div>


            </div>



          </div>
        </form>
        <img style={{display:this.state.showBtnSubmit?"none":"block"}} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%"/>
        <ImageManagerment openImageMana={this.state.openImageMana} showImageMana={this.showImageMana} addStateNameKey={this.addsetImg}></ImageManagerment>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session}/>
      </Layout >
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels     : state.Hotel,
    Lang       : state.Language,
    BigCategory: state.BigCategory
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    },
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductCategoryAddNewForm);
