import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../Ultils/StranlatetionData';
import ImageManagerment from '../ImageManagerment/Index';
import defaultImg from './../../Assets/img/default-image.png';
import BigCategory from '../../Storage/Reducers/BigCategory';
import './../../Assets/css/Admin/ProductCategory/AddNewForm.css';
import AlertCustom from './../Shared/AlertCustom';
class ProductCategoryAddNewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {

      Language: localStorage.getItem("lang"),
      AlertCustom: {
        type: "default",
        title: "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time: "40000",
        session: 0
      },
      showBtnSubmit: true,
      openImageMana: false,
      setClassFixed: false,
      name: "",
      username: "",
      img_url: defaultImg,
      img_url_review: defaultImg,
      password: "",
      email: "",
      status: true,
      position: "",
      Users: [],
      validate: {
        name: {
          id: "name",
          length_min: 1,
          length_max: 50,
          required: true,
          msg: ""
        },
        email: {
          id: "email",
          length_min: 1,
          length_max: 50,
          required: true,
          msg: ""
        },
        username: {
          id: "username",
          length_min: 1,
          length_max: 50,
          required: true,
          msg: ""
        },
        password: {
          id: "password",
          length_min: 8,
          length_max: 50,
          required: true,
          msg: ""
        },
        position: {
          id: "position",
          length_min: 1,
          length_max: 50,
          required: true,
          msg: ""
        },
        status: {
          id: "status",
          length_min: 1,
          length_max: 50,
          required: true,
          msg: ""
        },


      }
    }

  }




  addStateEvent = (e) => {
        const input = e.target;
    if (input.name == "BigCategoryID") {
      this.OnSelectBigcategory(input.value)
    }




    var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked : input.value
      })
    }
    setTimeout(() => {
      this.validateInput();
      this.checkMailExits();
      this.checkUsernameExits();
    }, 100)

  }
  checkMailExits = () => {
    if (this.state.Users.filter(x => x.email == this.state.email).length > 0) {
      this.state.validate["email"].msg = this.stran("This email was created, choose another..");
      return true;
    } else {
      if(!this.state.email.includes("@")){
        this.state.validate["email"].msg = this.stran("This is not a type of email, choose another..");
        return true;
      }
     
      return false;
    }
  }
  checkUsernameExits = () => {
    if (this.state.Users.filter(x => x.username == this.state.username).length > 0) {
      this.state.validate["username"].msg = this.stran("This username was created, choose another..");
      return true;
    } else {
      return false;
    }
  }
  addStateNameKey = (name, value) => {
    this.setState({
      [name]: value
    })

  }
  addsetImg = (name, value) => {
    this.setState({

      img_url: value
    })
  }

  BigCategoryGet = () => {
    return this.state.BigCategory.map((item, id) => {
      return (
        <option value={item.id}>{item.name}</option>
      );
    });
  }
  OnSelectBigcategory = (BigCategoryID) => {
    actionRedux.getProductCategoryByBigCategoryID(this.state.Language, BigCategoryID, (result) => {
      this.setState({ ProductCategory: result })
    });
  }
  ProductCategoryGet = () => {

    return this.state.ProductCategory.map((item, id) => {
      return (
        <option value={item.id}>{item.name}</option>
      );
    });
  }

  componentWillMount() {
    this.setState({
      Language: this.props.Lang[0]["lang"],
      BigCategory: this.props.BigCategory,
    })
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();
  }

  componentWillReceiveProps(newprops) {
    if (this.props != newprops) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
        BigCategory: newprops.BigCategory
      }, () => {
      });
    }
  }

  componentDidMount() {
    actionRedux.getUserStaff((result) => {

      this.setState({ Users: result })
      // this.props.fetchAllProductCategory(result);
    });


    setInterval(() => {
      //     //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
      var clientWidth = this.refs.setOfSubmitBtn != null ? this.refs.setOfSubmitBtn.clientWidth : 0;
      if (window.pageYOffset > clientWidth) {
        this.setState({
          setClassFixed: true
        })
      }
      else {
        this.setState({
          setClassFixed: false
        })
      }
    }, 10);
  }

  deleteItemImgMore = (e) => {

    var finalImgmore = "";
    var breakKey = "{bao&%/break}";
    var imgMoreArr = this.state.imgMore.split("{bao&%/break}");
    var count = 0;
    imgMoreArr.map((item, idex) => {
      if (e.target.dataset.src != item) {
        var breakKeyCustom = count > 0 ? breakKey : "";
        count++;
        finalImgmore = finalImgmore + breakKeyCustom + item;
      }
    })
    console.log(finalImgmore)
    this.setState({
      imgMore: finalImgmore
    })
    this.state.validate.imgMore.count = count;
  }
  getImgMore = () => {
    var imgMoreArr = this.state.imgMore.split("{bao&%/break}");
    return imgMoreArr.map((item, idex) => {

      return (
        <img src={item} data-src={item} width="49%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive img-thumbnail mt-2" alt="Click upload to add image" onClick={this.deleteItemImgMore} />
      );

    });
  }


  validateInput = () => {
    var me = this;
    var result = true;
    var arr = Object.values(this.state.validate);
    var validate_msg = "";
    arr.map((item, id) => {
      if (item.required === true && item.length_min > this.state[item.id].length) {
        if(item.id=="password"){
        this.state.validate[item.id].msg = this.stran('Password must be more than 8 charaters!')
        }else{
          this.state.validate[item.id].msg = this.stran('cannot be blank!')
        }
        me.setStateAlertCustom("warning", me.stran("recommendation"), me.stran("Please! satify red line behide input box."))
        // this.forceUpdate()
        result = false;
      } else {
        this.state.validate[item.id].msg = ""
      }
    })
    return result;
  }

  submitBtn = (e) => {
    const me = this;
    this.state.showBtnSubmit = false;
    e.preventDefault();
    if (this.validateInput() && !this.checkMailExits() && !this.checkUsernameExits()) {
      var {
        name,
        username,
        password,
        email,
        position,
        img_url,
        status

      }=this.state;

      actionRedux.adUserStaff(name,username, email, password,img_url,position,status, result => {
       if(result==true){
        me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You added a new user: ") + me.state.username)
        me.state.showBtnSubmit = true;
       }else{
      
          me.state.showBtnSubmit = true;
          me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ") + me.state.username+ ". error: " + result)
       } })
    } else {
      this.state.showBtnSubmit = true;
    }
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type: type,
        title: title,
        content: content,
        display: true,
        time: time | "999999999",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  showImageMana = (e) => {
    this.setState({
      imgSetFor: e.target.name,
      openImageMana: this.state.openImageMana ? false : true
    })
  }


  render() {


    var stranl = this.stran;

    // var Lang = this.props.Hotels;

    return (
      <Layout history={this.props.history}>

        <form>
          <div className="container ">

            <div className="row">

              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-10 mt-2 animated rollIn">


                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">{stranl("add a new awesome user")}</h4>
                    {/* ingredient dish */}
                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        {/* <label className="col-sm-12 form-control-label">Material Inputs</label> */}
                        <div className="col-sm-12">
                          <div className="form-group-material">
                            <label className="">{stranl("Email")} ({this.state.email.length}/50) </label>
                            <input type="text" name="email" required="" value={this.state.email} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.email.msg.length > 0 ? this.state.validate.email.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("name")} ({this.state.name.length}/50) </label>
                            <input type="text" name="name" required="" value={this.state.name} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.name.msg.length > 0 ? this.state.validate.name.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("username")} ({this.state.username.length}/50) </label>
                            <input type="text" name="username" required="" value={this.state.username} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.username.msg.length > 0 ? this.state.validate.username.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("Position")}  </label>
                            <select name="position" className="form-control" onChange={this.addStateEvent} >
                              <option selected disabled hidden>Choose here</option>
                              <option value="admin" >admin</option>
                              <option value="super" >super</option>
                              <option value="order" >order</option>
                              <option value="content" >content</option>
                            </select>
                            <p className="text-danger">{this.state.validate.position.msg.length > 0 ? this.state.validate.position.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("password")}* ({this.state.password.length}/50) </label>
                            <input type="password" name="password" required="" value={this.state.password} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.password.msg.length > 0 ? this.state.validate.password.msg : ""}</p>
                          </div>


                          <div className="form-group-material mt-4">
                            <label className="">{stranl("status")}*</label>
                            <input type="checkbox" name="status" required="true" checked={this.state.status} className="ml-3" onChange={this.addStateEvent} />
                            <p className="text-warning">{!this.state.status ? stranl("This will not work until status checked") : ""}</p>
                          </div>



                        </div>

                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 mt-2">
                        <h6>{stranl("Profile Image")}</h6>
                        <input name="img_url" type="button" className="btn btn-default" value={stranl("Upload")}  onClick={this.showImageMana} />
                        <div className="decorate-img animated rotateInDownRight"></div>
                        <img src={this.state.img_url} width="100%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive mt-2" alt="Image" />
                        {/* <input type="file" name="img_url" accept="image/*" onChange={(e) => {
                          var reader = new FileReader();
                          reader.readAsDataURL(e.target.files[0]);
                          var me = this;
                          reader.onload = function () {
                            var fileContent = reader.result;
                            me.addStateNameKey("img_url_review", fileContent);
                          }

                          this.addStateNameKey("img_url", e.target.files[0])
                        }} />
                        <img src={this.state.img_url_review} width="100%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive mt-2" alt="Image" />
                        <hr /> */}

                        <br />


                      </div>

                    </div>


                  </div>

                </div>






              </div>


              <div className=" col-xs-12 col-sm-12 col-md-12 col-lg-2">
                <div className="line mt-2" />
                <div className={` container set-submit   text-center ${this.state.setClassFixed ? "fixed-set-submit" : "mt-2"}`} >

                  <button style={{ display: this.state.showBtnSubmit ? "block" : "none" }} type="submit" className="btn btn-success" ref="setOfSubmitBtn" onClick={this.submitBtn}>Submit </button>
                  {this.props.history != null ? <div className="mt-2"><button className="btn btn-default" onClick={(e) => { e.preventDefault(); this.props.history.goBack(); }}>Go Back</button></div> : ""}
                </div>
                <div className="line mt-2" />
              </div>


            </div>



          </div>
        </form>
        <img style={{ display: this.state.showBtnSubmit ? "none" : "block" }} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%" />
        <ImageManagerment openImageMana={this.state.openImageMana} showImageMana={this.showImageMana} addStateNameKey={this.addsetImg}></ImageManagerment>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session} />
      </Layout >
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels: state.Hotel,
    Lang: state.Language,
    BigCategory: state.BigCategory
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    },
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductCategoryAddNewForm);
