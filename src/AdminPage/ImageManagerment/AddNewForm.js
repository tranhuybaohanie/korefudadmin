import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      setClassFixed: false,
      name: "",
      description: "",
      status: true
    }
    //  this.myRef = React.createRef();
  }
  //componentDrops.fetchAllHotels();

  componentWillMount() {
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();

  }
  componentWillUpdate() {

  }


  addState = (e) => {
    const input = e.target;
    this.setState({
      [input.name]: input.type === 'checkbox' ? input.checked : input.value
    })
    setTimeout(() => {
      // console.log(this.state )
    }, 1000);


  }
  submitBtn = (e) => {
    e.preventDefault();

    var bigcate = {
      name: this.state.name,
      description: this.state.description,
      status: this.state.status
    }
    actionRedux.adBigCategory(bigcate)

  }

  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }

  render() {
    setInterval(() => {
      //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
      var clientWidth = this.refs.setOfSubmitBtn != null ? this.refs.setOfSubmitBtn.clientWidth : 0;
      if (window.pageYOffset > clientWidth) {
        this.setState({
          setClassFixed: true
        })

      }
      else {
        this.setState({
          setClassFixed: false
        })
      }
    }, 1000);
    var Hotels = this.props.Hotels;

    return (
      <Layout>

        <form>
          <div className="container">

            <div className="row">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-2 mt-2"></div>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-8 mt-2">

                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">Add a new awesome big category</h4>

                    <label className="col-sm-12 form-control-label">Material Inputs</label>
                    <divs className="col-sm-12">
                      <div className="form-group-material">
                        <input type="text" name="name" required="" className="input-material" onChange={this.addState} />
                        <label className="label-material "> Name</label>
                      </div>

                      <div className="form-group-material">
                        <input type="email" name="description" required="" className="input-material" onChange={this.addState} />
                        <label className="label-material ">Description</label>
                      </div>
                   
                      <div className="form-group-material">
                        <input type="checkbox" name="status" required="" checked={this.state.status} className="input-material" onChange={this.addState} />
                        <label className="label-material active">Status</label>
                      </div>

                    </divs>

                  </div>
                </div>

              </div>




              <div className=" col-xs-12 col-sm-12 col-md-12 col-lg-2">
                <div className="line mt-2" />
                <div className={` container set-submit   text-center ${this.state.setClassFixed ? "fixed-set-submit" : "mt-2"}`} >

                  <button type="submit" className="btn btn-success" ref="setOfSubmitBtn" onClick={this.submitBtn}>Submit </button>
                  {this.props.history != null ? <div className="mt-2"><button className="btn btn-default" onClick={this.props.history.goBack}>Go Back</button></div> : ""}
                </div>
                <div className="line mt-2" />
              </div>


            </div>



          </div>
        </form>
      </Layout >
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels: state.Hotel
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);