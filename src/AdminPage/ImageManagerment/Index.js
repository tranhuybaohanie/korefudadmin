import React, { Component, Fragment, progress } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import { storageRef } from './../../Ultils/firebaseConfig'
import { db } from './../../Ultils/firebaseConfig'
import '../../Assets/css/Admin/ImageManagerment/style.css'
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from './../../Ultils/StranlatetionData';
import SendFile from './../../Ultils/SendFile'
import { config } from '@fortawesome/fontawesome-svg-core';
import * as Config from './.../../../../Constants/Config';
let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
   
    this.state = {
      Language     : "en",
      ConfirmCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      AlertCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      tooltipcustom:{
        display: false,
        content: "content",
        "title": "title"
      },
      mouse:{
        x: 0,
        y: 0
      },
      clientWidth: 0,
      delete     : {
        id: "", name: ""
      },
      imgUpload     : null,
      uploadProgress: 0,
      ok            : "oyennnn",
      // todos: ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','T','v','u','w','x','y','z'],
      // currentPage: 1,
      // todosPerPage: 3,
      isReadyUpload   : false,
      BigCategory     : this.props.BigCategory,
      imgUploadName   : "",
      imgFile         : "",
      currentPage     : 1,
      todosPerPage    : 3,
      HotelsPagination: null,
      img_list_from_db: [],
      infoImageDB     : {
        name        : "",
        img_url     : "",
        img_size    : "",
        create_date : "",
        content_type: "",
        status      : true}
    };



  }
  componentWillReceiveProps(newprops) {

  
    if (this.state.Language != newprops.Lang&& newprops.Lang!=null) {
     
      this.setState({
        Language: newprops.Lang,
      }, () => {

      });

    }
  }
  componentDidMount() {
    actionRedux.getImange((result) => {
    result
    this.setState({
      img_list_from_db: result
    })
    //   this.props.fetchAllBigCategory(result);
    // });
    // storageRef.ref().child('images/').getMetadata().then((url) => {
    // // .getDownloadURL().then((url) => {
    //   console.log("url")  
    // console.log(url)
    // }).catch((error) => {
    //   // Handle any errors

    })
    setInterval(()=>{
      if(this.refs.imgmana!=null)
      if(this.state.clientWidth!=this.refs.imgmana.offsetLeft)
      this.setState({
        clientWidth: this.refs.imgmana.offsetLeft+50
      })
    },2000)
 
      // // it is a regular DOM node
      // this.containerLine.offsetTop
      // // or with jquery
      // console.log($(this.containerLine).offset())
  


  }
  // componentWillMount() {
  //   this.props.history.push('/#/admin');
  // }
  delImgByid=(e)=>{
    if (e.target.id) {
      var name = e.target.name;
      var id   = e.target.id;
      this.setState({
        delete: {
          id,
          name
        }
      }, () => {
        this.setStateConfirmCustom("warning", this.stran("confirm"), this.stran("Do you realy want to delete: ") + this.state.delete.name)
      })
    }
  }

  showItemList = (item) => {

    return item.map((me, idd) => {

      var { id, name,img_url, status,img_size,create_date,content_type } = me;
      var styleme                                                        = {
        position    : "absolute",
        right       : "5px",
        top         : "3px",
        width       : "31px",
        height      : "27px",
        lineHeight  : "0",
        paddingRight: "15px",
        borderRadius: "18px",
      }
      return (

        <div className="col-lg-2 img-thumbnail d-flex p-2 flex-column d-flex justify-content-center" style={{height: "200px"}} >
       <button id={id} name={name} className="btn btn-danger" style={styleme} onClick={this.delImgByid}>x</button>
        <img name={name}  data-size={img_size/1000+"kb"} data-create_date={create_date} data-content_type={content_type} className=" img-thumbnail img-responsive  align-self-center" src={img_url}  style={{maxWidth: "100%",maxHeight: "90%"}}  onClick={this.takeUrlToParent} onMouseEnter={this.tooltip} onMouseLeave={this.tooltipHide}></img>
        
        <h6 className="align-self-center text-center">{name}</h6>
      
        </div>  
        // <tr key={idd}>
        //   <th scope="row">{idd}</th>
        //   <td>{id}</td>
        //   <td>{name}</td>

        //   <td>{description}</td>
        //   <td>{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        //   <td>
        //     <FontAwesomeIcon icon="pencil-alt" />|||

        //   <button id={id} onClick={this.deleteBigCategory} data-id={id}>
        //       <FontAwesomeIcon className="text-danger" icon="times" />
        //     </button>
        //   </td>
        // </tr>
      );

    });
  }
  tooltip=(e)=>{
 

   this.setState({
    tooltipcustom:{
      display: true,
      content: e.target.dataset.size+", "+e.target.dataset.create_date,
      "title": e.target.name
    },
   })
  }
  tooltipHide=()=>{
    
   this.setState({
    tooltipcustom:{
      display: false,
      content: "",
      "title": ""
    },
   })
  }
  takeUrlToParent =(e)=>{
    var input = e.target;
    this.props.addStateNameKey("img_url",input.src);

    this.props.showImageMana(e)
  }
 
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }


  setStateHotelsPagination = (state) => {
    
    if (state != this.state.HotelsPagination) {

      this.setState({
        HotelsPagination: state
      })
    }
  }

  addStateEvent = (e) => {
    const input = e.target;
    this.setState({
      [input.name]: input.type === 'checkbox' ? input.checked: input.value
    })
   
  }
  setStateCommon = (name, value) => {
    this.setState({
      [name]: value
    })
    setTimeout(() => {
     // console.log(this.state, name, value);
    }, 1000);
  }

  adImgToDb = (id)=>{
    var me = this;
   
    var infoImageDB = this.state.infoImageDB;
    db.collection("image-library").doc(id).update(
      {
          name        : infoImageDB.name,
          img_url     : infoImageDB.img_url,
          img_size    : infoImageDB.img_size,
          create_date : infoImageDB.create_date,
          content_type: infoImageDB.content_type,
          status      : true
      }
  ).then(()=>{
 
    me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You updated a new image") )
  })

  // .then(function() {
  //     alert("Document successfully written!");
  // })
  // .catch(function(error) {
  //    alert("Error writing document: ", error);
  // });
  }
  // importImage = (e) => {
  //   const me = this;
  //   db.collection("image-library").add({
  //     name        : "",
  //     img_url     : "",
  //     img_size    : "",
  //     create_date : "",
  //     content_type: "",
  //     status      : true
  // })
  // .then(function(docRef) {
    
  //   e.preventDefault();
   
  //   me.setStateAlertCustom("warning", me.stran("warning"), me.stran("Please, wait for a moment. It is saving info to server!"))
  //   if(me.state.imgUploadName.trim()==""){
  //     alert("You should input name for Image");
  //     return;
  //   }
  
  //   me.setState({
        
  //     isReadyUpload: false,
    
  //   })
  //   var img = me.state.imgFile;
  
  //   var {name}     = img;
  //   var uploadTask = storageRef.ref(`images/${docRef.id}`).put(img);
  //   var setInfo    = (()=>{
  //   uploadTask.on('state_changed', function (snapshot) {
  //     var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  //     if(me.state.uploadProgress!=process){
  //       me.setStateCommon("uploadProgress", progress)
  //     }
  //     console.log('Upload is ' + progress + '% done');
    
  //   });
   
  //   uploadTask.then(()=>{
  //     // alert("done")
  //      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
         
  //       me.setState({
  //       infoImageDB:{
  //         img_url     : downloadURL,
  //         name        : me.state.imgUploadName,
  //         img_size    : uploadTask.snapshot.metadata.size,
  //         create_date : new Date().toLocaleString(),
  //         content_type: uploadTask.snapshot.metadata.contentType,
  //         author      : ""
  //       }

  //       },()=>{
  //         me.adImgToDb(docRef.id)
  //       })
       
      
     
  //         console.log('File available at', downloadURL);
         
         
        
       
  //       });
  //   })
      
     
  
   

  // });

  // setInfo()
  //   // actionRedux.adImage(infoImageDB);
    
  //   // setTimeout(()=>{
  //   //   console.log(infoImageDB)
  //   // },1000)
  //     // console.log("Document written with ID: ", docRef.id);
  // })
  // .catch(function(error) {
  //     console.error("Error adding document: ", error);
  // });
    
  // }
  importImage = (e) => {
   
    const me = this;
    me.setStateCommon("uploadProgress", "0")
    db.collection("image-library").add({
      name        : "",
      img_url     : "",
      img_size    : "",
      create_date : "",
      content_type: "",
      status      : true
  })
  .then(function(docRef) {
    
    e.preventDefault();
   
    me.setStateAlertCustom("warning", me.stran("warning"), me.stran("Please, wait for a moment. It is saving info to server!"))
    if(me.state.imgUploadName.trim()==""){
      alert("You should input name for Image");
      return;
    }
  
    me.setState({
        
      isReadyUpload: false,
    
    })
    var img = me.state.imgFile;
  
    // var uploadTask = storageRef.ref(`images/${docRef.id}`).put(img);
    // var setInfo    = (()=>{
    // uploadTask.on('state_changed', function (snapshot) {
    //   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    //   if(me.state.uploadProgress!=process){
    //     me.setStateCommon("uploadProgress", progress)
    //   }
    //   console.log('Upload is ' + progress + '% done');
    
    // });
   
    // uploadTask.then(()=>{
    //   // alert("done")
    //    uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
      SendFile(me.state.imgUploadName,img).then((file_upload)=>{
        me.setStateCommon("uploadProgress", "100")
      var data=file_upload.data
        me.setState({
        infoImageDB:{
          img_url     : Config.domainnodeuploadfile+"/"+me.state.imgUploadName+data.extension_file,
          name        : me.state.imgUploadName,
           img_size    :data.size,
          create_date : new Date().toLocaleString(),
           content_type:data.extension_file,
          author   : localStorage.getItem("uid")
        }

        },()=>{
          me.adImgToDb(docRef.id)
        })
          // console.log('File available at', downloadURL);

})

  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
  });
    
  }
  stran = (key) => {
   
    return stranl(this.state.Language, key);
  }
 
  setStateImg=(e)=>{
   
   
      this.setState({
        imgFile       : e.target.files[0],
        isReadyUpload : true,
        uploadProgress: 5
      })
   
   
  }
  
  setStateConfirmCustom = (type, title, content, time) => {
    this.setState({
      ConfirmCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.ConfirmCustom.session + 1
      },
    })
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "100000",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  MouseMove=(e)=>{
    var me = this;
    window.addEventListener("mousemove", function (evt) {
      me.setState({
        mouse:{
          x: evt.clientX,
          y: evt.clientY
        }
      })
    })
  }
  
  yesConfirmDeleted = () => {
    this.state.ConfirmCustom.display  = false;
    this.state.ConfirmCustom.session += 1;
    // alert(this.state.delete.id)
    actionRedux.delImgageById(this.state.delete.id, (result) => {
      if (result == true) {
        this.setStateAlertCustom("success", this.stran("Congratulation!"), this.stran("You have deleted: ") + this.state.delete.name)

      } else {
        this.setStateAlertCustom("danger", this.stran("Fail"), this.stran("there are some thing wrong cannot delete: "))
      }
      this.setState({
        delete: {
          id  : "",
          name: ""
        }
      });
    })

  }

  render() {
    var { img_list_from_db } = this.state;
    var HotelsPagination     = this.state.HotelsPagination;
    var stranl               = this.stran;
    return (
      <div  ref="imgmana"  style={{display:`${this.props.openImageMana?'block':'none'}`}} className="containner-image-mana  col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2">
        <div className= {this.props.openImageMana?'bounceIn card animated':'bounceOut card animated'} onMouseMove={this.MouseMove}>
          <div className="card-header">
            <div className="row">
              <div className="col-xs-5 col-sm-5 col-md-5 col-lg-3">
                <h4 >{stranl("Image mangerment")}  </h4>
              </div>
              <div className="col-xs-5 col-sm-5 col-md-6 col-lg-8 ">
                Import Image   <input placeholder="image name.." type="text" name="imgUploadName" className="input-name-upload" value={this.state.imgUploadName} onChange={this.addStateEvent}/>
                <input type="file" onChange={this.setStateImg} className="btn btn-default input-file" />
                <input type="submit"  style={{display:`${this.state.isReadyUpload?'block':'none'}`}} onClick={this.importImage.bind(this)} value="Upload" className="btn btn-success btn-upload-img animated bounceIn" />
               
               
              </div>
              <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
               <input type="button" className=" btn-close" value="x" onClick={this.props.showImageMana}/>
              </div> 
            

          </div>
          <div className="card-body">
            <div className="table-responsive">

              <PaginationCustom todos={img_list_from_db} onReceive={this.setStateHotelsPagination}>
              <div className="container">
              <div className="row">

                {HotelsPagination != null ? this.showItemList(HotelsPagination) : ""}

            </div>
            </div>

              </PaginationCustom>

            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <progress className="prodress-upload" value={this.state.uploadProgress} max="100" >
                </progress>{this.state.uploadProgress}%
               
            </div>
      </div>
      {/* tooltipcustom */}
      <div className="tooltipcustom" style={{...styles.tooltip,top:this.state.mouse.y,left:this.state.mouse.x-this.state.clientWidth,display:this.state.tooltipcustom.display?"block":"none"}}>
      <div style={styles.tooltiparrow}></div>
      <div style={styles.tooltiptitle}>

      <h6 className="text-center" style={styles.tooltiptitle}>{this.state.tooltipcustom.title}</h6>
      </div>
      <div style={styles.tooltipbody}>
        <p>{this.state.tooltipcustom.content}</p>
      </div>
      </div>
      <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session} />
        <ConfirmCustom type={this.state.ConfirmCustom.type} title={this.state.ConfirmCustom.title} content={this.state.ConfirmCustom.content} display={this.state.ConfirmCustom.display} time={this.state.ConfirmCustom.time} session={this.state.ConfirmCustom.session} yes={this.yesConfirmDeleted} />
      </div>

    );
  }
}

const styles={

  tooltip:{
    width    : "100px",
    minHeight: "100px",
    position : "absolute",
  
    border         : "1px solid",
    backgroundColor: "white",
    borderRadius   : "9px",
    boxShadow      : "0px 0px 10px",
  },

  tooltiparrow:{
    width          : "20px",
    height         : "11px",
    backgroundColor: "black",
    position       : "absolute",
    top            : "-10px",
    left           : "40px",
    clipPath       : "polygon(50% 0%,100% 100%, 0% 100%)",
  },
  tooltiptitle:{
    color          : "white",
    backgroundColor: "black",
    width          : "100%",
    minHeight      : "20px",
  },
  tooltipcontent:{
    fontSize: "15px",
    color   : "gray"
  },

}

const mapStateToProps = state => {
  return {
    BigCategory: state.BigCategory,
   
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);