import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { browserHistory } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../Ultils/StranlatetionData';
import ImageManagerment from '../ImageManagerment/Index';
import defaultImg from './../../Assets/img/default-image.png';
import BigCategory from '../../Storage/Reducers/BigCategory';
import './../../Assets/css/Admin/ProductCategory/AddNewForm.css';
import AlertCustom from './../Shared/AlertCustom';
class ProductCategoryAddNewForm extends Component {

  constructor(props) {
    super(props);
    this.state = {

      Language   : localStorage.getItem("lang"),
      AlertCustom: {
        type   : "default",
        title  : "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time   : "40000",
        session: 0
      },
      showBtnSubmit   : true,
      openImageMana   : false,
      setClassFixed   : false,
      name_en         : "",
      name_vi         : "",
      img_url         : defaultImg,
      description_en  : "",
      description_vi  : "",
      meta_title      : "",
      meta_description: "",
      meta_keyword    : "",
      status          : true,
      BigCategory     : "",
      BigCategoryID   : "",
      validate        : {
        name_en: {
                      id        : "name_en",
                      length_min: 1,
                      length_max: 50,
                      required  : true,
                      msg       : ""
                    }, 
                    name_vi: {
                      id        : "name_vi",
                      length_min: 1,
                      length_max: 50,
                      required  : true,
                      msg       : ""
                    }, 
                    BigCategoryID: {
          id        : "BigCategoryID",
          length_min: 1,
          length_max: 100,
          required  : true,
          msg       : ""
        },
        description_en: {
          id        : "description_en",
          length_min: 0,
          length_max: 100,
          msg       : ""
        },
        description_vi: {
          id        : "description_vi",
          length_min: 0,
          length_max: 100,
          msg       : ""
        },
        status: {
          id        : "status",
          length_min: 1,
          length_max: 50,
          required  : true,
          msg       : ""
        },
        meta_title: {
          id        : "meta_title",
          length_min: 0,
          length_max: 70,
          msg       : ""

        },
        meta_description: {
          id        : "meta_description",
          length_min: 0,
          length_max: 156,
          msg       : ""
        },
        meta_keyword: {
          id        : "meta_keyword",
          length_min: 0,
          length_max: 50,
          msg       : ""
        }

      }
    }

  }




  addStateEvent = (e) => {
    const input = e.target;
    console.log(input)
    var length_max = this.state.validate[input.name].length_max ? this.state.validate[input.name].length_max : "infinity";
    if (length_max == "infinity" || length_max >= input.value.length) {
      this.setState({
        [input.name]: input.type === 'checkbox' ? input.checked: input.value
      })
    }
    setTimeout(() => {
      this.validateInput();
    }, 100)

  }

  addStateNameKey = (name, value) => {
    this.setState({
      [name]: value
    })

  }

  BigCategoryGet = () => {
    return this.state.BigCategory.map((item, id) => {
      return (
        <option value={item.id}>{item.name}</option>
      );
    });
  }

  componentWillMount() {
    this.setState({
      Language   : this.props.Lang[0]["lang"],
      BigCategory: this.props.BigCategory,
    })
    // this.props.history.push('/#/admin');
    //  this.myRef.focus();
  }

  componentWillReceiveProps(newprops) {
    if (this.props != newprops) {
      this.setState({
        Language   : newprops.Lang[0]["lang"],
        BigCategory: newprops.BigCategory
      }, () => {
      });
    }
  }

  componentDidMount() {
    actionRedux.getBigCategory((result) => {
      this.props.fetchAllBigCategory(result);
    });
    setInterval(() => {
      //     //console.log( window.pageYOffset,this.refs.setOfSubmitBtn.clientWidth)
      var clientWidth = this.refs.setOfSubmitBtn != null ? this.refs.setOfSubmitBtn.clientWidth : 0;
      if (window.pageYOffset > clientWidth) {
        this.setState({
          setClassFixed: true
        })
      }
      else {
        this.setState({
          setClassFixed: false
        })
      }
    }, 10);
  }

  validateInput = () => {
   var me           = this;
   var result       = true;
   var arr          = Object.values(this.state.validate);
   var validate_msg = "";
    arr.map((item, id) => {
      if (item.required === true && item.length_min > this.state[item.id].length) {
        this.state.validate[item.id].msg = this.stran('cannot be blank!')
        me.setStateAlertCustom("warning", me.stran("recommendation"), me.stran("Please! satify red line behide input box."))
        // this.forceUpdate()
        result = false;
      } else {
        this.state.validate[item.id].msg = ""
      }
    })
    return result;
  }

  submitBtn = (e) => {
    const me                       = this;
          this.state.showBtnSubmit = false;
    e.preventDefault();
    if (this.validateInput()) {
      var cate = {
        name_en         : this.state.name_en,
        description_en  : this.state.description_en,
        name_vi         : this.state.name_vi,
        description_vi  : this.state.description_vi,
        img_url         : this.state.img_url,
        meta_title      : this.state.meta_title,
        meta_description: this.state.meta_description,
        meta_keyword    : this.state.meta_keyword,
        BigCategoryID   : this.state.BigCategoryID,
        status          : this.state.status
      }
      actionRedux.adProductCategory(cate).then(function () {
         me.setStateAlertCustom("success", me.stran("Congratulation!"), me.stran("You added a new product category: ") +  eval("me.state.name_"+me.state.Language))
         me.state.showBtnSubmit = true;
      })
        .catch(function (error) {
          me.state.showBtnSubmit = true;
          me.setStateAlertCustom("danger", me.stran("Fail"), me.stran("there are some thing wrong when adding: ")+  eval("me.state.name_"+me.state.Language) + ". error: " + error)
        })
    }else{
      this.state.showBtnSubmit = true;
    }
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type   : type,
        title  : title,
        content: content,
        display: true,
        time   : time | "999999999",
        session: this.state.AlertCustom.session+1
      },
    })
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  showImageMana = () => {
    this.setState({
      openImageMana: this.state.openImageMana ? false: true
    })
  }

  render() {


    var stranl = this.stran;

    // var Lang = this.props.Hotels;

    return (
      <Layout history={this.props.history}>

        <form>
          <div className="container ">

            <div className="row">

              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-10 mt-2 animated rollIn">


                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title">{stranl("add a new awesome product category")}</h4>

                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        {/* <label className="col-sm-12 form-control-label">Material Inputs</label> */}
                        <div className="col-sm-12">
                          <div className="form-group-material">
                            <label className="">{stranl("name")}_en* ({this.state.name_en.length}/50) </label>
                            <input type="text" name="name_en" required="" value={this.state.name_en} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.name_en.msg.length > 0 ? this.state.validate.name_en.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("name")}_vi* ({this.state.name_vi.length}/50) </label>
                            <input type="text" name="name_vi" required="" value={this.state.name_vi} className="input-material form-control" onChange={this.addStateEvent} />
                            <p className="text-danger">{this.state.validate.name_vi.msg.length > 0 ? this.state.validate.name_vi.msg : ""}</p>
                            {/* <label  className=""> Name</label> */}
                          </div>

                          <div className="form-group-material">
                            <label className="">{stranl("description")}_en ({this.state.description_en.length}/100)</label>
                            <input type="text" name="description_en" required="" value={this.state.description_en} className="input-material" onChange={this.addStateEvent} />

                          </div>
                          <div className="form-group-material">
                            <label className="">{stranl("description")}_vi ({this.state.description_vi.length}/100)</label>
                            <input type="text" name="description_vi" required="" value={this.state.description_vi} className="input-material" onChange={this.addStateEvent} />

                          </div>
                          <div className="">
                            <label className="">{stranl("big category")}*</label>
                            <select name="BigCategoryID" className="form-control" onChange={this.addStateEvent} >
                              <option selected disabled hidden>Choose here</option>
                              {this.BigCategoryGet()}
                            </select>
                            <p className="text-danger">{this.state.validate.BigCategoryID.msg.length > 0 ? this.state.validate.BigCategoryID.msg : ""}</p>
                          </div>
                          <div className="form-group-material mt-4">
                            <label className="">{stranl("status")}*</label>
                            <input type="checkbox" name="status" required="true" checked={this.state.status} className="ml-3" onChange={this.addStateEvent} />
                            <p className="text-warning">{!this.state.status? stranl("This category will not work until status checked") : ""}</p>
                          </div>



                        </div>

                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 mt-2">
                        <input type="button" className="btn btn-default" value="Upload" onClick={this.showImageMana} />
                        <div className="decorate-img animated rotateInDownRight"></div>
                        <img src={this.state.img_url} width="100%" height="auto" style={{ backgroundColor: 'gray' }} className="img-responsive mt-2" alt="Image" />


                      </div>

                    </div>


                  </div>

                </div>


                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{stranl("Basic SEO infomation")}<FontAwesomeIcon className={this.state.meta_title.length > 0 && this.state.meta_description.length && this.state.meta_keyword.length > 0 ? "ml-2 mt-2 text-success animated bounceIn" : "ml-2 mt-2 text-danger animated tada"} icon="circle" /> </h5>

                    <div className="row">

                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-7 mt-2">
                        <div className="form-group-material">
                          <label className="">{stranl("focus keyword")}  ({this.state.meta_keyword.length}/50) </label>
                          <input type="text" name="meta_keyword" required="" className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("Meta title")}  ({this.state.meta_title.length}/70) </label>
                          <input type="text" name="meta_title" required="" className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>
                        <div className="form-group-material">
                          <label className="">{stranl("description")}  ({this.state.meta_description.length}/156)</label>
                          <input type="text" name="meta_description" required="" className="input-material" onChange={this.addStateEvent} />
                          {/* <label  className=""> Name</label> */}
                        </div>

                      </div>
                    </div>
                  </div>
                </div>



              </div>


              <div className=" col-xs-12 col-sm-12 col-md-12 col-lg-2">
                <div className="line mt-2" />
                <div className={` container set-submit   text-center ${this.state.setClassFixed ? "fixed-set-submit" : "mt-2"}`} >

                  <button style={{display:this.state.showBtnSubmit?"block":"none"}} type="submit" className="btn btn-success" ref="setOfSubmitBtn" onClick={this.submitBtn}>Submit </button>
                  {this.props.history != null ? <div className="mt-2"><button className="btn btn-default" onClick={(e)=> {e.preventDefault();this.props.history.goBack();}}>Go Back</button></div> : ""}
                </div>
                <div className="line mt-2" />
              </div>


            </div>



          </div>
        </form>
        <img style={{display:this.state.showBtnSubmit?"none":"block"}} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%"/>
        <ImageManagerment openImageMana={this.state.openImageMana} showImageMana={this.showImageMana} addStateNameKey={this.addStateNameKey}></ImageManagerment>
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session}/>
      </Layout >
    );



  }

}


const mapStateToProps = state => {
  return {
    Hotels     : state.Hotel,
    Lang       : state.Language,
    BigCategory: state.BigCategory
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    setToDB: () => {
      dispatch(actFetchHotelsRequest());
    },
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductCategoryAddNewForm);
