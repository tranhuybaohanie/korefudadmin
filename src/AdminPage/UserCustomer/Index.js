import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';
import AlertCustom from './../Shared/AlertCustom';
import ConfirmCustom from './../Shared/ConfirmCustom';
import { stranl } from '../../Ultils/StranlatetionData';
import Detail from './Detail'

// import "babel-polyfill";
let prev = 0;
let next = 0;
let last = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      Language: localStorage.getItem("lang"),
      ConfirmCustom: {
        type: "default",
        title: "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time: "40000",
        session: 0
      },
      AlertCustom: {
        type: "default",
        title: "InfoMation",
        content: "dddd dddddddd ddddddddđ dddddddd ddddddđ",
        display: false,
        time: "40000",
        session: 0
      },
      delete: {
        name: "undified",
        id: "11111111"
      },
      BigCategory: [],
      pageNumber: 1,
      loading: true,
      displayDetail: false,
      detailByID: "",
      // todos: ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','T','v','u','w','x','y','z'],
      // currentPage: 1,
      // todosPerPage: 3,
      Users: [],
      ProductCategoryReal: [],
      BigCategoryIDSearching: "",
      currentPage: 1,
      todosPerPage: 3,
      HotelsPagination: null,
      searchText: "",
      searchOption: "name"
    };

    // this.showItemList = this.showItemList.bind(this);

  }



  addStateEvent = (e) => {
    const input = e.target;


    this.setState({
      [input.name]: input.type === 'checkbox' ? input.checked : input.value
    },
      () => {

      })

  }

  addStateEventCB = (e, cb) => {
    const input = e.target;
    this.setState({
      [input.name]: input.type === 'checkbox' ? input.checked : input.value
    },
      () => {
        cb();
      })

  }

  addStateNameKey = (name, value) => {
    if (this.state[name] != value) {
      this.setState({
        [name]: value
      })
    }

  }
  big_category = () => {
    actionRedux.getBigCategoryById(big_category_id, (data) => { big_category_name = data.name })
    this.setState({ big_category })
  }
  componentDidMount() {

    actionRedux.getUserCustomer((result) => {

      this.setState({ Users: result })
     
    }); 
  }
  active = (e) => {
    if (e.target.id) {
      var name = e.target.name;
      var id = e.target.id;
      this.setState({
        delete: {
          id,
          name
        }
      }, () => {
        this.setStateConfirmCustom("warning", this.stran("confirm"), this.stran("Do you realy want to change status: ") + this.state.delete.name)
      })
    }
  }
  // componentWillMount() {
  //   // this.props.history.push('/#/admin');

  // }

  showItemList = (item) => {
    return item.map((me, idd) => {
      var { id, username,email,address,status, create_date} = me;

      return (<tr key={idd}>
        <th scope="row">{(idd + 1) + (10 * (this.state.pageNumber - 1))}</th>
        <td >{id}</td>
        <td>{email}</td>
        <td>{address.phone}</td>
        <td>{create_date}</td>
        <td className="text-center">{status==true ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td className="text-center">
          {/* <button className="btn-action text-darkcyan" id={id} data-id={id} onClick={this.showDetail}>
            <img src={require("./../../Assets/img/icon/eye.png")} style={{ paddingBottom: "3px" }} id={id} name={name} onClick={this.showDetail} width="15px" className="img-responsive" />
          </button>
          <span className="btn-action text-darkyellow" id={id} data-id={id}>
            {this.shortNavlink("/admin/modified-product/" + id, "text-darkyellow", "", "pencil-alt")}
          </span> */}

          {/* <button className={status==true ?"btn-danger":" btn-info"} name={username} id={id} onClick={this.active} data-id={id}>
           {status==true ?"INACTIVE":"ACTIVE"}
          </button> */}
        </td>
      </tr>
      );

    });

  }
  showDetail = (e) => {

    this.setState({
      displayDetail: true,
      detailByID: e.target.id
    });
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }

  shortNavlink = (to, type, text, icon) => {
    return <li style={{ listStyleType: "none" }}><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }

  onclick = () => {
    this.setState({
      ok: "dlfasldjflkasdjflkjsdklff",
      example: "lalalalal",
      Hotels: this.props.Hotels
    }, () => {
      //console.log(this.state.Hotels)
    })

  }
  componentWillReceiveProps(newprops) {

    var previousLang = this.props.Lang[0]["lang"];
    if (this.props != newprops) {
      this.setState({
        Language: newprops.Lang[0]["lang"],

      }, () => {

        if (previousLang != this.state.Language) {
          actionRedux.getUserCustomer((result) => {

            this.setState({ Users: result })
           
          }); 
        }
      });

    }

  }
  BigCategoryGet = () => {

    return this.state.BigCategory.map((item, id) => {
      return (
        <option value={item.id} >{item.name}</option>
      );
    });
  }
  OnSelectBigcategory = (e) => {

    actionRedux.getProductCategoryByBigCategoryID(this.state.Language, e.target.value, (result) => {
      this.setState({ ProductCategoryReal: result })
    });
  }
  ProductCategoryGet = () => {

    return this.state.ProductCategoryReal.map((item, id) => {
      return (
        <Fragment>

          <option value={item.id} >{item.name}</option>
          <option selected>Choose here</option>
        </Fragment>
      );
    });
  }

  setStateHotelsPagination = (state, pageNumber) => {

    if (state != this.state.HotelsPagination) {
      setTimeout(() => {
        this.setState({
          loading: false
        })
      }, 1000)

      this.setState({

        pageNumber,
        HotelsPagination: state
      })
    }


  }

  setStateConfirmCustom = (type, title, content, time) => {
    this.setState({
      ConfirmCustom: {
        type: type,
        title: title,
        content: content,
        display: true,
        time: time | "100000",
        session: this.state.ConfirmCustom.session + 1
      },
    })
  }

  setStateAlertCustom = (type, title, content, time) => {
    this.setState({
      AlertCustom: {
        type: type,
        title: title,
        content: content,
        display: true,
        time: time | "100000",
        session: this.state.AlertCustom.session + 1
      },
    })
  }
  search = (e) => {
    // console.log(this.state.searchOption+"ok")
    this.addStateEventCB(e, () => {
      if (this.state.searchText != "") {
        actionRedux.searchProduct(this.state.Language, this.state.searchOption, this.state.searchText, (result) => {
          if (result != false) {
            this.setState({ Users: result })
          }

        });
      } else {
        actionRedux.getUserCustomer((result) => {

          this.setState({ Users: result })
         
        }); 
      }
    })

  }
  yesConfirmDeleted = () => {
    this.state.ConfirmCustom.display = false;
    this.state.ConfirmCustom.session += 1;
    // alert(this.state.delete.id)
    actionRedux.updateUserstaffStatus(this.state.delete.id, (result) => {
      if (result == true) {
        this.setStateAlertCustom("success", this.stran("Congratulation!"), this.stran("You have changed status: ") + this.state.delete.name)

      } else {
        this.setStateAlertCustom("danger", this.stran("Fail"), this.stran("there are some thing wrong cannot change status"))
      }
      this.setState({
        delete: {
          id: "",
          name: ""
        }
      });
    })

  }
  render() {


    var { Users } = this.state;
    var HotelsPagination = this.state.HotelsPagination;
    var stranl = this.stran;
    return (

      <Layout history={this.props.history}>
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2 ">

          <div className="card">
            <div className="card-header">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"><h4 >{stranl("User customer")}  </h4></div>
              <div className="row">

                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 animated bounceIn mt-1">
                  {/* {this.shortNavlink("/admin/user-staff-add-new", "btn btn-success ", stranl("add a new awesome user"), "plus")} */}
                </div>
                <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">

                  {/* <div className="input-group d-flex justify-content-end">
                    {this.state.searchOption == "product_category_id" ?
                      <div className="d-flex flex-row justify-content-between" style={{ position: "absolute", top: "0%", right: "35%", width: "651px" }}>
                        <label className="">{stranl("big category")}</label>


                      </div>
                      : this.state.searchOption == "status" ? <span> <button className="btn btn-success border-right-0 border" name="searchText" value="true" type="button" onClick={this.search}>
                        Active
                </button>
                        <button className="btn btn-danger border-left-0 border" name="searchText" value="false" type="button" onClick={this.search}>
                          InActive
                </button> </span>
                        : <input type="text" name="searchText" required="" value={this.state.searchText} className="rounded-left p-1 control-form border-right-0 border" onChange={this.search} />
                    }
                    <span className="input-group-append">

                      <select className="form-control" name="searchOption" onChange={this.addStateEvent}>
                        <option value="name">username</option>
                        <option value="status">Status</option>
                        <option value="author">Author</option>
                        <option value="create_date">Date</option>
                        


                      </select>
                      <button className="btn btn-outline-secondary border-left-0 border" name="searchText" value="" type="button" onClick={(e) => { this.search(e); }}>
                        X
                </button>

                    </span>
                  </div> */}
                </div>
              </div>

            </div>
            <div className="card-body">
              <div className="table-responsive">

                <PaginationCustom todos={Users} onReceive={this.setStateHotelsPagination}>
                  <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>

                        <th>{stranl("ID")}</th>
                        <th>{stranl("Name")}</th>
                        <th>{stranl("Phone")}</th>
                        <th >{stranl("Create date")}</th>
                        <th className="text-center">{stranl("Status")}</th>
                        {/* <th className="text-center">{stranl("Action")}</th> */}

                      </tr>
                    </thead>
                    <tbody>



                      {HotelsPagination != null ? this.showItemList(HotelsPagination) : ""}




                    </tbody>
                  </table>
                </PaginationCustom>

              </div>
            </div>
          </div>
        </div>
        <img style={{ display: this.state.loading ? "block" : "none" }} className="loading-icon" src={require('./../../Assets/img/octo-loader.gif')} width="20%" />
        <AlertCustom type={this.state.AlertCustom.type} title={this.state.AlertCustom.title} content={this.state.AlertCustom.content} display={this.state.AlertCustom.display} time={this.state.AlertCustom.time} session={this.state.AlertCustom.session} />
        <ConfirmCustom type={this.state.ConfirmCustom.type} title={this.state.ConfirmCustom.title} content={this.state.ConfirmCustom.content} display={this.state.ConfirmCustom.display} time={this.state.ConfirmCustom.time} session={this.state.ConfirmCustom.session} yes={this.yesConfirmDeleted} />
        <Detail detailByID={this.state.detailByID} displayDetail={this.state.displayDetail} setShow={() => { this.setState({ displayDetail: false }) }}></Detail>
      </Layout>
    );
  }
}



const mapStateToProps = state => {
  return {
    Lang: state.Language,
    // ProductCategory: state.ProductCategory
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllProductCategory: (result) => {

      dispatch(actionRedux.actFetchProductCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);