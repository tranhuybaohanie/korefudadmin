import React, { Component,Fragment } from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

// import './App.css';
import SideBarHeader from '../Shared/SidebarHeader';
import Header from './Header';
import Footer from './Footer';
import * as actionRedux from '../../Storage/Actions/Index';
import './../../Assets/tempdashboard/css/fontastic.css';
// import 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700';
// import './../../Assets/tempdashboard/vendor/jquery/jquery.min.js';
// import './../../Assets/tempdashboard/vendor/popper.js/umd/popper.min.js';
// import './../../Assets/tempdashboard/vendor/bootstrap/js/bootstrap.min.js';
import './../../Assets/tempdashboard/css/grasp_mobile_progress_circle-1.0.0.min.css';
import './../../Assets/tempdashboard/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css';
import './../../Assets/tempdashboard/css/style.default.css';
import './../../Assets/tempdashboard/css/custom.css';
import './../../Assets/tempdashboard/js/grasp_mobile_progress_circle-1.0.0.min.js';
import './../../Assets/tempdashboard/vendor/jquery.cookie/jquery.cookie.js';
// import './../../Assets/tempdashboard/vendor/chart.js/Chart.min.js';
import './../../Assets/tempdashboard/vendor/jquery-validation/jquery.validate.min.js';
import './../../Assets/tempdashboard/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js';
// import './../../Assets/tempdashboard/js/charts-home.js';
import './../../Assets/tempdashboard/js/front.js';

import '../../Assets/css/Admin/Shared/Layout.css';
import { firebaseApp } from '../../Ultils/firebaseConfig';
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
// window.onbeforeunload = function() {
//     var rememberme = localStorage.getItem("expired");
//     var today      = new Date();
//     if(rememberme==null)
//     localStorage.removeItem("LoginID");
//     return '';
//   };

class Home extends Component {

    constructor(props) {
        super(props);
        this.state={
            redirectLogin: true
        }
     
    }

    componentWillMount(){
        var me = this;
        firebaseApp.auth().onAuthStateChanged(function(user) {
            if (user) {
          actionRedux.getUserAdminByID(user.uid,result=>{

      if(result.status){
    }else{
        me.props.history.push(`/admin/login`); 
      }
            })
            localStorage.setItem("uid",user.uid);
              me.setState({redirectLogin:false})
              
              var isAnonymous = user.isAnonymous;
              var uid         = user.uid;
    
            } else {
               
                me.props.history.push(`/admin/login`); 
             
            }
           
          });
   
    }
    render() {
if(!this.state.redirectLogin){
        return (
            <Fragment>
            
                <div className="">
                   

                  <SideBarHeader></SideBarHeader>
                    <div className="page">
                        {/* navbar*/}
                        <Header history={this.props.history}></Header>
                      
                        {this.props.children}
  
                        <footer className="main-footer">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <p>Korefud © 2018-2019</p>
                                    </div>
                                    <div className="col-sm-6 text-right">
                                        <p>Design by <a href="https://www.facebook.com/TranHuyBaoHS" className="external">Tran Huy Bao</a></p>
                                        {/* Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)*/}
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>


            </Fragment>
        );
    }else{return(
    <div className="container-fluid" style={{    height: "100%", backgroundColor: "white"}}>
    <div className="row" style={{    height: "100%", backgroundColor: "white"}}>


    </div>
<img src={require("./../../Assets/img/loadsquare.gif")} style={{ position:"absolute",left:"48%",top:"40%"  }} width="5%"></img>
    </div>
    )}
    }
}

export default Home;
