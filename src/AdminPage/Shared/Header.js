import React, { Component, Fragment } from 'react';
import * as actionRedux from '../../Storage/Actions/Index';
import { connect } from 'react-redux';
import { firebaseApp } from '../../Ultils/firebaseConfig';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Header extends Component {
    constructor(props){
        super(props);
        this.state={
                lang: localStorage.getItem("lang"),
          
        }
        this.setLang(this.state.lang)
       
    }

    setLang=(lang)=>{
        var obLang = [{lang:lang}]
        if(this.props.Language!=this.state.lang){
        this.props.updateLang(obLang)
        localStorage.setItem("lang",lang);
        }
       
    }
    componentWillReceiveProps(newprops) {

        if (this.props != newprops) {
          this.setState({
            lang: newprops.Lang[0]["lang"]
          }, () => {
    
          });
    
        }
      }
      logOut=()=>{
          var me = this.props;
        firebaseApp.auth().signOut().then(function() {
            me.history.push(`/admin/login/${me.history.location.pathname.replaceAll("/","__")} `); 
          }).catch(function(error) {
           alert("Error:"+error)
          });
     
      }
    stran = (key) => {

        return stranl(this.state.Language,key);
    }
    render(){
        var {lang} = this.state;
        return(
            <header className="header">
                            <nav className="navbar">
                                <div className="container-fluid">
                                    <div className="navbar-holder d-flex align-items-center justify-content-between">
                                        <div className="navbar-header"><a id="toggle-btn" href="#" className="menu-btn"><i className="icon-bars"> </i></a><a href="index.html" className="navbar-brand">
                                            <div className="brand-text d-none d-md-inline-block"><span><img src={require("./../../Assets/img/fav-iconnonback.png")} width= "20px" className="img-responsive animated rubberBand" />OREFUD</span>
                                            
                                            <strong className="text-primary">Dashboard</strong></div></a></div>
                                        <ul className="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                                            {/* Notifications dropdown*/}
                                            <li className="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="nav-link"><FontAwesomeIcon icon="bell"/><span className="badge badge-warning">12</span></a>
                                                <ul aria-labelledby="notifications" className="dropdown-menu">
                                                    <li><a rel="nofollow" href="#" className="dropdown-item">
                                                        <div className="notification d-flex justify-content-between">
                                                            <div className="notification-content"><FontAwesomeIcon icon="envelope"/>You have 6 new messages </div>
                                                            <div className="notification-time"><small>4 minutes ago</small></div>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item">
                                                        <div className="notification d-flex justify-content-between">
                                                            <div className="notification-content"><FontAwesomeIcon icon="envelope"/>You have 2 followers</div>
                                                            <div className="notification-time"><small>4 minutes ago</small></div>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item">
                                                        <div className="notification d-flex justify-content-between">
                                                            <div className="notification-content"><i className="fa fa-upload" />Server Rebooted</div>
                                                            <div className="notification-time"><small>4 minutes ago</small></div>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item">
                                                        <div className="notification d-flex justify-content-between">
                                                            <div className="notification-content"><i className="fa fa-twitter" />You have 2 followers</div>
                                                            <div className="notification-time"><small>10 minutes ago</small></div>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item all-notifications text-center"> <strong> <FontAwesomeIcon icon="bell"/>view all notifications                                          </strong></a></li>
                                                </ul>
                                            </li>
                                            {/* Messages dropdown*/}
                                            <li className="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="nav-link"><FontAwesomeIcon icon="envelope"/><span className="badge badge-info">10</span></a>
                                                <ul aria-labelledby="notifications" className="dropdown-menu">
                                                    <li><a rel="nofollow" href="#" className="dropdown-item d-flex">
                                                        <div className="msg-profile"> <img src={require("./../../Assets/tempdashboard/img/avatar-1.jpg")} alt="..." className="img-fluid rounded-circle" /></div>
                                                        <div className="msg-body">
                                                            <h3 className="h5">Jason Doe</h3><span>sent you a direct message</span><small>3 days ago at 7: 58 pm - 10.06.2014</small>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item d-flex">
                                                        <div className="msg-profile"> <img src={require("./../../Assets/tempdashboard/img/avatar-2.jpg")} alt="..." className="img-fluid rounded-circle" /></div>
                                                        <div className="msg-body">
                                                            <h3 className="h5">Frank Williams</h3><span>sent you a direct message</span><small>3 days ago at 7: 58 pm - 10.06.2014</small>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item d-flex">
                                                        <div className="msg-profile"> <img src={require("./../../Assets/tempdashboard/img/avatar-3.jpg")} alt="..." className="img-fluid rounded-circle" /></div>
                                                        <div className="msg-body">
                                                            <h3 className="h5">Ashley Wood</h3><span>sent you a direct message</span><small>3 days ago at 7: 58 pm - 10.06.2014</small>
                                                        </div></a></li>
                                                    <li><a rel="nofollow" href="#" className="dropdown-item all-notifications text-center"> <strong> <FontAwesomeIcon icon="envelope"/>Read all messages  </strong></a></li>
                                                </ul>
                                            </li>
                                            {/* Languages dropdown    */}
                                            <li className="nav-item dropdown"><a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="nav-link language ">
                                            <img src={lang=="vi"?require("./../../Assets/img/flag/vietnam.png"):require("./../../Assets/img/flag/us.png")} width='20px' alt="English" />
                                            <span className="d-none d-sm-inline-block">{lang=='vi'?'Vietnamese':'English'}</span></a>
                                                <ul aria-labelledby="languages" className="dropdown-menu">
                                                    <li><button rel="nofollow" onClick={()=>{this.setLang("vi")}} className="dropdown-item"> <img src={require("./../../Assets/img/flag/vietnam.png")} width='20px' alt="Vietnam" className="mr-2" /><span>Vietnamese</span></button></li>
                                                    <li><button rel="nofollow" onClick={()=>{this.setLang("en")}} className="dropdown-item"> <img src={require("./../../Assets/img/flag/us.png")} width='20px' alt="English" className="mr-2" /><span>English </span></button></li>
                                                </ul>
                                            </li>
                                            {/* Log out*/}
                                            <li className="nav-item"><button style={{    backgroundColor: "#544c4c",  border: "1px solid forestgreen",color: "white"}} onClick={this.logOut} className="btn"> <span className="d-none d-sm-inline-block"><FontAwesomeIcon icon="sign-out-alt"></FontAwesomeIcon> </span><i className="fa fa-sign-out" /></button></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </header>
        );
    }
}



const mapStateToProps = state => {
    return {
      Hotels: state.Hotel,
      Lang  : state.Language
    }
  }
  const mapDispatchToProps = (dispatch, props) => {
    return {
    //   setToDB: () => {
    //     dispatch(actFetchHotelsRequest());
    //   }
      updateLang: (Lang)=>{
        
          dispatch(actionRedux.updateLanguage(Lang))
      }
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(Header);
