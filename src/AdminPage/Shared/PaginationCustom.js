import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import { actFetchHotelsRequest } from '../../Storage/Actions/Index';
import {AdminPagination} from '../../Constants/Config'
import {
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap";

let prev = 0;
let next = 0;
let last = 0;
let first = 0;
class PaginationCustom extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todos: this.props.todos,
      currentPage: AdminPagination.currentPage,
      todosPerPage: AdminPagination.totalPage,
      oldTodosPagination:null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);

  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleLastClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: last
    });
  }
  handleFirstClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: 1
    });
  }




  componentWillMount() {

    this.setState({
      todos: this.props.todos
    }, () => {
     
    });

  }
  componentWillReceiveProps(newprops) {
 
    if (this.props != newprops) {
      this.setState({
        todos: newprops.todos
      }, () => {
        // console.log("me" + this.state.todos);
      });
    
    }
  }

  render() {


    let { todos, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    let indexOfLastTodo = currentPage * todosPerPage; //2*5
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;//10-5
    let currentTodos =todos!=null? todos.slice(indexOfFirstTodo, indexOfLastTodo):[];
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    var todosLength = todos!=null?todos.length:0;
    last = Math.ceil(todosLength / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;
    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }

    if(JSON.stringify(this.state.oldTodosPagination) !== JSON.stringify(currentTodos) ){
      // console.log("rin",this.state.oldTodosPagination,currentTodos);
      this.setState({ oldTodosPagination:currentTodos});
      this.props.onReceive(currentTodos,currentPage);
    }
  
    
    return (

      <Fragment>

       
        {this.props.children}
        <ul id="page-numbers">
          <nav>
            <Pagination>
              <PaginationItem>
                {prev === 0 ? <PaginationLink disabled>First</PaginationLink> :
                  <PaginationLink onClick={this.handleFirstClick} id={prev} href={prev}>First</PaginationLink>
                }
              </PaginationItem>
              <PaginationItem>
                {prev === 0 ? <PaginationLink disabled>Prev</PaginationLink> :
                  <PaginationLink onClick={this.handleClick} id={prev} href={prev}>Prev</PaginationLink>
                }
              </PaginationItem>
              {
                pageNumbers.map((number, i) =>
                  <Pagination key={i}>
                    <PaginationItem active={pageNumbers[currentPage - 1] === (number) ? true : false} >
                      <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>
                        {number}
                      </PaginationLink>
                    </PaginationItem>
                  </Pagination>
                )}

              <PaginationItem>
                {
                  currentPage === last ? <PaginationLink disabled>Next</PaginationLink> :
                    <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</PaginationLink>
                }
              </PaginationItem>

              <PaginationItem>
                {
                  currentPage === last ? <PaginationLink disabled>Last</PaginationLink> :
                   <PaginationLink onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</PaginationLink>
                }
              </PaginationItem>
            </Pagination>
          </nav>
        </ul>
      </Fragment>);
  }
}


export default PaginationCustom;