

import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { stranl } from '../../Ultils/StranlatetionData'
import { encode } from 'he';
import { db } from '../../Ultils/firebaseConfig';
import * as actionRedux from '../../Storage/Actions/Index';
import { firebaseApp } from '../../Ultils/firebaseConfig';
import { connect } from 'react-redux';
import Imgdefault from './../../Assets/img/img_profied_default.jpg'

class SidebarHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            UserName: "",
            url_profile: Imgdefault,
            Language: this.props.Lang[0]["lang"],
            position:""
        }

    }
    componentDidMount() {
        actionRedux.getUserAdminByID(localStorage.getItem("uid"), (userData) => {
            this.setState({ UserName: userData.name, url_profile: userData.url_profile })

        })
        var me=this;
        firebaseApp.auth().onAuthStateChanged(function (user) {
            if (user) {
              actionRedux.getUserAdminByID(user.uid, result => {
                me.setState({ position: result.position })
              })
            }
          })

    }
    componentWillReceiveProps(newprops) {

        if (this.props != newprops) {
            this.setState({
                Language: newprops.Lang[0]["lang"]
            }, () => {

            });

        }
    }
    stran = (key) => {

        return stranl(this.state.Language, key);
    }

    shortNavlink = (to, icon, text,positionAr) => {
        if(positionAr.includes(this.state.position))
        return <li><NavLink activeClassName="active text-danger" exact className="nav-link" to={to}> <FontAwesomeIcon icon={icon} /><p className="pl-2 d-inline">{this.stran(text)}</p>         </NavLink> </li>
    }

    render() {
        var stran = this.stran;
        var position =this.state.position;
        var admin ="admin";
        var content ="content";
        var order ="order";
        var superman ="super";
    

        return (

            <nav className="side-navbar mCustomScrollbar _mCS_1">
                {/* <div id="mCSB_1" className="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style={{maxHeight: "none"}} tabindex="0"> */}
                <div id="mCSB_1_container" className="mCSB_container" style={{ position: "relative", top: "0", left: "0" }} dir="ltr">


                    <div className="side-navbar-wrapper">
                        {/* Sidebar Header    */}
                        <div className="sidenav-header d-flex align-items-center justify-content-center">
                            {/* User Info*/}
                            <div className="sidenav-header-inner text-center">
                                <img style={{ boxShadow: "0px 0px 0px 3px firebrick" }} src={this.state.url_profile} alt="person" className="img-fluid rounded-circle" />
                                <h6 className="h5">{this.state.UserName}<br /></h6>
                                <i> {this.state.position+stran(" dashboard")} </i>
                                <span>Be working effective!</span>
                            </div>
                            {/* Small Brand information, appears on minimized sidebar*/}
                            <div className="sidenav-header-logo"><a href="index.html" className="brand-small text-center"> <strong>B</strong><strong className="text-primary">D</strong></a></div>
                        </div>
                        {/* Sidebar Navigation Menus*/}
                        <div className="main-menu">
                            <h5 className="sidenav-heading">{stran("main")}</h5>
                            <ul id="side-main-menu" className="side-menu list-unstyled">
                                {this.shortNavlink("/admin/", "home", "home",[admin,superman,content,order])}
                                {this.shortNavlink("/admin/big-category", "building", "big category",[admin,superman,content])}
                                {this.shortNavlink("/admin/product-category", "book", "product category",[admin,superman,content])}
                                {this.shortNavlink("/admin/product", "cubes", "product",[admin,superman,content])}
                                {this.shortNavlink("/admin/menu", "list-alt", "Menu",[admin,superman,content])}
                                {this.shortNavlink("/admin/order", "archive", "Order",[admin,superman,order])}
                                {/* {this.shortNavlink("/admin/blogs", "newspaper", "blogs")} */}
                                {/* <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i className="icon-interface-windows" />Example dropdown </a>
                                    <ul id="exampledropdownDropdown" className="collapse list-unstyled ">
                                        <li><a href="">Page</a></li>
                                    <li><a href="">Page</a></li>
                                    <li><a href="">Page</a></li>
                                    </ul>
                                </li>

                                <li> <a href=""> <i className="icon-mail" />Mails
<div className="badge badge-warning">6 New</div></a></li> */}
                            </ul>
                        </div>
                        <div className="admin-menu">
                           {this.state.position=="admin"? <h5 className="sidenav-heading">User common</h5>:""}
                            <ul id="side-admin-menu" className="side-menu list-unstyled">
                            {this.shortNavlink("/admin/user-staff", "user", "User Staff",[admin,superman])}
                            {this.shortNavlink("/admin/user-customer", "user", "User Customer",[admin,superman])}

                            </ul>
                        </div>
                        <div className="admin-menu">
                            <h5 className="sidenav-heading">Analysis</h5>
                            <ul id="side-admin-menu" className="side-menu list-unstyled">
                            {this.shortNavlink("/admin/order-analysis-dayly", "band-aid", "Order analysis dayly",[admin,superman])}                      
                            {this.shortNavlink("/admin/order-analysis-year", "band-aid", "Order analysis yearly",[admin,superman])}                      
                            </ul>
                           
                              </div>
                         
                    </div>


                </div>
                {/* </div> */}
            </nav>);
    }
}


const mapStateToProps = state => {
    return {
        Hotels: state.Hotel,
        Lang: state.Language
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        //   setToDB: () => {
        //     dispatch(actFetchHotelsRequest());
        //   }
        updateLang: (Lang) => {

            dispatch(actionRedux.updateLanguage(Lang))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SidebarHeader);