import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
// import './../../Assets/css/Admin/Shared/AlertCustom.css';



class AlertCustom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            type: this.props.type,
            title: this.props.title,
            content: this.props.content,
            display: this.props.display,
            time: this.props.time,
            session:this.props.session

        }
    }

    closeForm = () => {
        this.state.display = false;
        this.forceUpdate();
    }
    componentWillReceiveProps(newProps) {

        if (this.props.session != newProps.session) {
            if (this.state.display == true) {
                this.closeForm()
            }
            setTimeout(() => {
                this.setState({
                    type: newProps.type,
                    title: newProps.title,
                    content: newProps.content,
                    display: newProps.display,
                    time: newProps.time
                })
            }, 100)


        }
    }
    getColorForm = () => {
        switch (this.state.type) {
            case "success": return "rgba(199, 234, 199, 0.71)"; break;
            case "warning": return "#fcf8e3"; break;
            case "danger": return "#f2dede"; break;
            default: return "#d9edf7"; break;
        }
    }
    getColorFormBorder = () => {
        switch (this.state.type) {
            case "success": return "#d6e9c6"; break;
            case "warning": return "#faebcc"; break;
            case "danger": return "#ebccd1"; break;
            default: return "#bce8f1"; break;
        }
    }
    getColorFormText = () => {
        switch (this.state.type) {
            case "success": return "#3c763d"; break;
            case "warning": return "#8a6d3b"; break;
            case "danger": return "#a94442"; break;
            default: return "#31708f"; break;
        }

    }

    render() {
        const styles = {
            alert_custom_form: {
                width: "35%",
                position: "fixed",
                zIndex: "10",
                right: "32%",
                minHeight:"200px",
                top:"22%",
                 boxShadow: `0px 0px 30px ${this.getColorFormBorder()}`,
                backgroundColor: this.getColorForm(),
                border: "2px solid",
                borderColor: this.getColorFormBorder(),
                display: this.state.display ? "block" : "none"
            },
            alert_custom_title: {
                width: "100%",
                height: "10px",
                color: this.getColorFormText(),
            },
            alert_custom_content: {
                width: "100%",
                margin: '3%',
                color: this.getColorFormText(),
                minHeight: "94px",
            },
            alert_custom_body:{
                // minHeight: "94px"
            },
            closeBtn: {
                position: "absolute",
                right: "1%",
                top: "-3%",
                backgroundColor: "rgb(243, 71, 97)",
                borderRadius: "30%",
                width: "8%",
                border: "none",
                color: this.getColorFormText(),
            },
            alert_custom_cumbo_btn:{

            },
            btnYes:{
                marginRight:"20%"
            }
        }

        // setTimeout(() => {
        //     this.setState({
        //         display: false
        //     })
        // }, this.state.time)

        return (
            <Fragment>
                <div className={`alert_custom_form  animated ${this.state.display ? " zoomInUp" : "zoomOut"}`} style={styles.alert_custom_form}>
                    <div className="alert_custom_title" style={styles.alert_custom_title}>
                        <h6 className="text-center">{this.state.title}</h6>
                        <button  style={styles.closeBtn} onClick={this.closeForm}>x</button>
                    </div>
                    <div className="alert_custom_body" style={styles.alert_custom_content}>
                        <p className="">{this.state.content}</p>

                    </div>
                    <div className="alert_custom_cumbo_btn text-center" style={styles.alert_custom_cumbo_btn}>
                    <button className="btn  btn-success" style={styles.btnYes} onClick={this.props.yes}>Yes</button>
                    <button className="btn btn-danger" onClick={this.closeForm}>Cancel</button>

                    </div>
                    
                </div>


            </Fragment>
        );


    }
}





export default AlertCustom;
