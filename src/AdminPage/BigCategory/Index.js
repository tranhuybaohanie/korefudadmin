import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '../Shared/Layout';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';
import PaginationCustom from '../Shared/PaginationCustom';

let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ok: "oyennnn",
      // todos: ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','T','v','u','w','x','y','z'],
      // currentPage: 1,
      // todosPerPage: 3,
      BigCategory: this.props.BigCategory,

      currentPage     : 1,
      todosPerPage    : 3,
      HotelsPagination: null
    };



  }

  componentDidMount() {
    actionRedux.getBigCategory((result) => {
     
      this.props.fetchAllBigCategory(result);
    });



  }
  // componentWillMount() {
  //   // this.props.history.push('/#/admin');

  // }


  showItemList = (item) => {

    return item.map((me, idd) => {

      var { id, name, description, status } = me;
      return (<tr key={idd}>
        <th scope="row">{idd}</th>
        <td>{id}</td>
        <td>{name}</td>

        <td>{description}</td>
        <td>{status ? <i className="text-success"><FontAwesomeIcon icon="circle" /></i> : <i className="text-danger"><FontAwesomeIcon icon="circle" /></i>}</td>
        <td>
          {/* <FontAwesomeIcon icon="pencil-alt" />|||

        <button id={id} onClick={this.deleteBigCategory} data-id={id}>
            <FontAwesomeIcon className="text-danger" icon="times" />
          </button> */}
        </td>
      </tr>
      );

    });
  }

  deleteBigCategory = (e) => {
    actionRedux.delBigCategory(e.target.id)
    
  }
  shortNavlink = (to, type, text, icon) => {
    return <li><NavLink exact className={type} to={to}> <FontAwesomeIcon icon={icon} />{text}       </NavLink> </li>
  }

  onclick = () => {
    this.setState({
      ok     : "dlfasldjflkasdjflkjsdklff",
      example: "lalalalal",
      Hotels : this.props.Hotels
    }, () => {
      //console.log(this.state.Hotels)
    })

  }
  componentWillReceiveProps(newprops) {


    if (this.props != newprops) {
      this.setState({
        BigCategory: newprops.BigCategory,
      }, () => {

      });

    }
  }

  setStateHotelsPagination = (state) => {

    if (state != this.state.HotelsPagination) {

      this.setState({

        HotelsPagination: state
      })
    }


  }
  render() {


    var { BigCategory }  = this.state;
    var HotelsPagination = this.state.HotelsPagination;

    return (

      <Layout>



        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2">

          <div className="card">
            <div className="card-header">
              <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                  <h4 >Big category  </h4>
                </div>
                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">

                  {this.shortNavlink("/admin/add-new-big-category", "btn btn-success float-right", "Add new big category", "plus")}
                </div>
              </div>

            </div>
            <div className="card-body">
              <div className="table-responsive">

                <PaginationCustom todos={BigCategory} onReceive={this.setStateHotelsPagination}>
                  <table className="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>status</th>
                        <th>Action</th>

                      </tr>
                    </thead>
                    <tbody>



                      {HotelsPagination != null ? this.showItemList(HotelsPagination) : ""}




                    </tbody>
                  </table>
                </PaginationCustom>

              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}



const mapStateToProps = state => {
  return {
    BigCategory: state.BigCategory
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);